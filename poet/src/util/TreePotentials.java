package util;

import potentialModelsGP.tree.AdditionNode;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.DistanceNode;
import potentialModelsGP.tree.DivisionNode;
import potentialModelsGP.tree.MultiplicationNode;
import potentialModelsGP.tree.Node;
import potentialModelsGP.tree.PowerNode;
import potentialModelsGP.tree.SubtractionNode;
import potentialModelsGP.tree.TreePotential;
import potentialModelsGP.tree.neighbors.SiteSumNode;

public class TreePotentials {
//  Sutton Chen Embedding term from EAM
  public static TreePotential generateSCEmbedding(double cutoffDistance, String species, boolean exact){
	  /**
	   * Source: suttonchen.lib from GULP, originally using an outer cutoff distance of 12 Angstroms
			eam_density power 6
			Ni core   729.6928296
			Cu core   527.6214232
			Rh core  1530.5233630
			Ag core   630.5428597
			Ir core  2157.0662340
			Al core  1303.9271480
			eam_density power 7
			Pd core  2759.359115
			Pb core  4745.406658
			eam_density power 8
			Pt core  25964.75440
			Au core  14878.01402
	   */

//	  	Constant C (unitless)
	  	double cvalue = Double.NaN;
	  	double powervalue = Double.NaN;
	  	if(species.equals("Cu")){
	  		if(exact) {
		  		cvalue = 527.6214232;
		  		powervalue = 6;
	  		}
	  		else {
		  		cvalue = 527.6214232*0.999;
		  		powervalue = 6*-1.00001;
	  		}
	  	}
	  	else if(species.equals("Au")){
	  		if(exact){
	  			cvalue = 14878.01402;
	  		}
	  		else{
	  			cvalue = 14878.01402*0.95;
	  		}
	  		powervalue = 8;
	  	}
	  	else if(species.equals("Pt")){
	  		if(exact){
	  			cvalue = 25964.75440;
	  		}
	  		else{
	  			cvalue = 25964.75440*0.95;
	  		}
	  		powervalue = 8;
	  	}
	  	else if(species.equals("Al")){
	  		powervalue = 6;
	  		if(exact){
	  			cvalue =1303.9271480;
	  		}
	  		else{
	  			cvalue = 1303.9271480*0.99;
	  		}
	  	}
	  	else if(species.equals("Ag")){
	  		powervalue = 6;
	  		if(exact){
	  			cvalue = 630.5428597;
	  		}
	  		else{
	  			cvalue = 630.5428597*0.99;
	  		}
	  	}
	  	else if(species.equals("Ni")){
	  		powervalue = 6;
	  		if(exact){
//	  			double cLAMMPS = 3.000475003706081;
	  			cvalue = 729.6928296;//Math.pow(cLAMMPS, powervalue);
	  		}
	  		else{
	  			double cGULLP = 729.6928296;
	  			cvalue = cGULLP*0.99;
	  		}
	  	}
	  	else if(species.equals("Pd")){
	  		powervalue = 7;
	  		if(exact){
//	  			double cLAMMPS = 3.000475003706081;
	  			cvalue = 2759.359115;//Math.pow(cLAMMPS, powervalue);
	  		}
	  		else{
	  			double cGULLP = 2759.359115;
	  			cvalue = cGULLP*0.99;
	  		}
	  	}
		Node r = new DistanceNode();
		Node oneHalf = new ConstantNode(0.5);
		Node pw = new ConstantNode(-powervalue);

		Node sqrtC = new ConstantNode(Math.sqrt(cvalue));
		
		Node powerRho = new PowerNode(r,pw);

		Node rho = new SiteSumNode(powerRho,true);

		Node sqrtRho = new PowerNode(rho,oneHalf);
		Node multip = new MultiplicationNode(sqrtC,sqrtRho);
		TreePotential tree = new TreePotential(multip,cutoffDistance); 
		return tree;
  }

	  public static TreePotential generateSCRepulsive(double cutoffDistance, String species, boolean exact){
		  /**
		   * Source: suttonchen.lib from GULP, originally using an outer cutoff distance of 12 Angstroms
			lennard 9 6
			Ni core Ni core 1303.09841 0.0 0.0 5.5
			Cu core Cu core 1289.04851 0.0 0.0 12.0
			lennard 12 6
			Rh core Rh core 44758.45463 0.0 0.0 12.0
			Pd core Pd core 50172.71320 0.0 0.0 12.0
			Ag core Ag core 55689.05010 0.0 0.0 12.0
			lennard 14 6
			Ir core Ir core 371200.1922 0.0 0.0 12.0
			lennard 10 6
			Pt core Pt core 16992.17930 0.0 0.0 12.0
			Au core Au core 16352.11869 0.0 0.0 12.0
			Pb core Pb core 49250.84611 0.0 0.0 12.0
			lennard 7 6
			Al core Al core 592.4195621 0.0 0.0 12.0

		   */
//		  	Constant A lj (Units of eV*Angstrom). Later we divide by 2 to avoid double counting
		  	double aValue = Double.NaN;
		  	double powerValue = Double.NaN;
		  	if(species.equals("Cu")){
		  		if(exact) {
			  		aValue = 1289.04851;
			  		powerValue = 9;
		  		}
		  		else {
			  		aValue = 1289.04851*1.001;
			  		powerValue = 9*1.000001;
		  		}
		  	}
		  	else if(species.equals("Au")){
		  		powerValue = 10;
		  		if(exact){
		  			aValue = 16352.11869;
		  		}
		  		else{
		  			aValue = 16352.11869*1.001;
		  		}
		  	}
		  	else if(species.equals("Al")){
		  		powerValue = 7;
		  		if(exact){
		  			aValue = 592.4195621;
		  		}
		  		else{
		  			aValue = 592.4195621*1.1;
		  		}
		  	}
		  	else if(species.equals("Ag")){
		  		powerValue = 12;
		  		if(exact){
		  			aValue = 55689.05010;
		  		}
		  		else{
		  			aValue = 55689.05010*1.1;
		  		}
		  	}
		  	else if(species.equals("Ni")){
		  		powerValue = 9;
		  		if(exact){
//		  			double cLAMMPS = 2.218751067911386;
		  			aValue = 1303.09841;//Math.pow(cLAMMPS, powerValue);
		  		}
		  		else{
		  			double cGULP = 1303.09841;
//		  			double cLAMMPS = Math.pow(cGULP, 1.0/powerValue);
		  			aValue = cGULP*1.11;
		  		}
		  	}
		  	else if(species.equals("Pt")){
		  		powerValue = 10;
		  		if(exact){
		  			aValue = 16992.17930;
		  		}
		  		else{
		  			aValue = 16992.17930*1.1;
		  		}
		  	}
		  	else if(species.equals("Pd")){
		  		powerValue = 12;
		  		if(exact){
		  			aValue = 50172.71320;
		  		}
		  		else{
		  			aValue = 50172.71320*1.1;
		  		}
		  	}
			Node alj = new ConstantNode(aValue/2.0);//multiply by 1/2 to avoid double-counting
			Node powNode = new ConstantNode(-powerValue);
			Node rlj = new DistanceNode();
			Node powerlj = new PowerNode(rlj,powNode);
			Node multiplj = new MultiplicationNode(alj,powerlj);//alj,powerlj
			Node lj = new SiteSumNode(multiplj,true);
			
			TreePotential tree = new TreePotential(lj,cutoffDistance);
			return tree;
	  }
	/**
	 * Citation: Foiles et al, Phys Rev B, 33, 7983 (1986)
	 * @return
	 */
	public static TreePotential generate_Cu_u3_eam() {
		
		double alpha = 1.7227;
		MultiplicationNode mult5 = new MultiplicationNode(new ConstantNode(-alpha), new DistanceNode());
		PowerNode term2 = new PowerNode(new ConstantNode(Math.E), mult5);
		double nu = 2.0;
		PowerNode power1 = new PowerNode(new DistanceNode(), new ConstantNode(nu));
		double beta = 0.1609;
		MultiplicationNode mult4 = new MultiplicationNode(new ConstantNode(beta), power1);
		AdditionNode term1 = new AdditionNode(new ConstantNode(1.0), mult4);
		MultiplicationNode mult3 = new MultiplicationNode(term1, term2);
		double z0 = 11;
		MultiplicationNode mult2 = new MultiplicationNode(new ConstantNode(z0), mult3);
		MultiplicationNode z = new MultiplicationNode(new ConstantNode(z0), mult2);
		PowerNode mult1 = new PowerNode(z,new ConstantNode(2.0));
		DivisionNode phi = new DivisionNode(mult1, new DistanceNode());
		SiteSumNode repulsive = new SiteSumNode(new MultiplicationNode(new ConstantNode(1/2.0), phi),true);
		
		
//		SiteSumNode rho = null;
		PowerNode embedding = null;
		
		AdditionNode add = new AdditionNode(repulsive, embedding);
		return new TreePotential(add, 4.9499999999999886);
	}
	/**
	 * Citation: Mishin et al. (2001). DOI: 10.1103/PhysRevB.63.224106
	 * @param r0
	 * @param alpha
	 * @return
	 */
	public static Node morseMishin(double r0, double alpha) {
		Node subtraction2 = new SubtractionNode(new DistanceNode(), new ConstantNode(r0));
		Node term2 = new MultiplicationNode(new ConstantNode(-alpha),subtraction2);
		Node exp2 = new PowerNode(new ConstantNode(Math.E), term2);
		Node subtraction1 = new SubtractionNode(new DistanceNode(), new ConstantNode(r0));
		Node term1 = new MultiplicationNode(new ConstantNode(-2*alpha),subtraction1);
		Node exp1 = new PowerNode(new ConstantNode(Math.E), term1);
		return new SubtractionNode(exp1,new MultiplicationNode(new ConstantNode(2.0),exp2));
	}
	///**
	// * Citation: Mishin et al. (2001). DOI: 10.1103/PhysRevB.63.224106
	// * @param rc
	// * @param h
	// * @return
	// */
	//public static Node smoothingMishin(double rc, double h) {
//		Node x = new DivisionNode(new SubtractionNode(new DistanceNode(),new ConstantNode(rc)),new ConstantNode(h));
//		Node power1 = new PowerNode(x,new ConstantNode(4.0));
//		Node power2 = new PowerNode(x,new ConstantNode(4.0));
//		return new DivisionNode(power1,new AdditionNode(new ConstantNode(1.0),power2));
	//}

	public static TreePotential generateLJ(double cutoffDistance, double power, double sigma, double epsilon){
		double factor = 4.0*epsilon/2.0; //divide by 2 to avoid double counting
		factor *= Math.pow(sigma, power);
		Node factorNode = new ConstantNode(factor);
		Node powerValueNode = new ConstantNode(power);
		Node rlj = new DistanceNode();
		Node powerNode = new PowerNode(rlj,powerValueNode);
		Node division = new DivisionNode(factorNode,powerNode);
		Node ss = new SiteSumNode(division,true);
		return new TreePotential(ss,cutoffDistance);
	}
	public static TreePotential generateLJ(double cutoffDistance, String species){
		double kB = 8.6173303E-5; //Boltzmann constant eV/K
		double sigma = Double.NaN; //Angstroms
		double epsilon = Double.NaN; // eV
		double powerRepulsive = Double.NaN;
		double powerAttractive = Double.NaN;
		if(species.equals("He")) {
			sigma = 2.551; 
			epsilon = 10.22*kB;
			powerRepulsive = 12.0;
			powerAttractive = 6.0;
		}
		else if(species.equals("Ar")) {
			sigma = 3.35;
			epsilon = 143.2*kB;
			powerRepulsive = 12.0;
			powerAttractive = 6.0;
		}
		Node lj = new SubtractionNode(generateLJ( cutoffDistance, powerRepulsive, sigma, epsilon).getTopNode(), 
				generateLJ(cutoffDistance, powerAttractive, sigma, epsilon).getTopNode());
		return new TreePotential(lj,cutoffDistance);

	}
//	public static TreePotential generateLJRepulsive(double cutoffDistance, String species){
//		double kB = 8.6173303E-5; //Boltzmann constant eV/K
//		double sigma = Double.NaN; //Angstroms
//		double epsilon = Double.NaN; // eV
//		double powerRepulsive = Double.NaN;
//		if(species.equals("He")) {
//			sigma = 2.551; 
//			epsilon = 10.22*kB;
//			powerRepulsive = 12.0;
//		}
//		Node lj = generateLJ(true, cutoffDistance, powerRepulsive, sigma, epsilon).getTopNode();
//		return new TreePotential(lj,cutoffDistance);
//	}
//	public static TreePotential generateLJAttractive(double cutoffDistance, String species){
//		double kB = 8.6173303E-5; //Boltzmann constant eV/K
//		double sigma = Double.NaN; //Angstroms
//		double epsilon = Double.NaN; // eV
//		double powerAttractive = Double.NaN;
//		if(species.equals("He")) {
//			sigma = 2.551; 
//			epsilon = 10.22*kB;
//			powerAttractive = 6.0;
//		}
//		Node lj = generateLJ(false, cutoffDistance, powerAttractive, sigma, epsilon).getTopNode();
//		return new TreePotential(lj,cutoffDistance);
//	}
	//  Sutton Chen EAM
	public static TreePotential generateSC(double cutoffDistance, String species, boolean exact){
		Node eam = new SubtractionNode(generateSCRepulsive(cutoffDistance,species, exact).getTopNode(),
				generateSCEmbedding(cutoffDistance, species, exact).getTopNode());
		TreePotential tree = new TreePotential(eam,cutoffDistance);
		return tree;
	}
	/**
	 * Term used in GP1, GP2 and GP3: r^(a-b*r)
	 * @param params params[0] = a, params[1] = b
	 * @return r^(a-b*r)
	 */
	private static Node generateRpowABR(double[] params) {
		Node d0 = new DistanceNode();
		Node d1 = new DistanceNode();
		
		Node param0 = new ConstantNode(params[0]);
		Node param1 = new ConstantNode(params[1]);
		
		Node mult0 = new MultiplicationNode(param1,d1);
		Node add0 = new SubtractionNode(param0,mult0);
		Node p0 = new PowerNode(d0,add0);
		return p0;
	}
	
	/**
	 * Term used in GP2 and GP3: a*Sum(r^(b-c*r))
	 * @param params params[0] = a, params[1] = b, params[2] = c
	 * @return a*Sum(r^(b-c*r))
	 */
	private static Node generatePairwiseGP(double[] params) {
		Node param0 = new ConstantNode(params[0]);
		Node p0 = generateRpowABR(new double[] {params[1],params[2]});
		Node ss0 = new SiteSumNode(p0,true);
		Node pairwise0 = new MultiplicationNode(param0,ss0);
		return pairwise0;
	}
	public static TreePotential generateGP3(double cutoffDistance, double[] params) {
		if(params == null) {
			//Use the default parameters for GP3 from the paper
			params = new double[7];
			params[0] = 7.51;
			params[1] = 3.98;
			params[2] = 3.93;
			params[3] = 28.01;
			params[4] = 0.03;
			params[5] = 11.73;
			params[6] = 2.93;
		}
		
		Node pairwise0 = generatePairwiseGP(new double[] {params[0],params[1],params[2]});
		
		Node pairwise1 = generatePairwiseGP(new double[] {params[4],params[5],params[6]});
		Node add0 = new SubtractionNode(new ConstantNode(params[3]),pairwise1);
		Node manybody = new DivisionNode(add0,new SiteSumNode(new ConstantNode(1.0),true));
		
		
		Node topNode = new AdditionNode(pairwise0, manybody);
		TreePotential tree = new TreePotential(topNode,cutoffDistance);
		return tree;
	}
	public static TreePotential generateGP1(double cutoffDistance, double[] params) {
		if(params == null) {
			//Use the default parameters for GP1 from the paper
			params = new double[5];
			params[0] = 10.21;
			params[1] = 5.47;
			params[2] = 0.21;
			params[3] = 0.97;
			params[4] = 0.33;
		}		
		
		Node rPowABR = generateRpowABR(new double[] {params[0],params[1]});
		Node gp1Term1 = new PowerNode(new ConstantNode(params[2]),new DistanceNode());
		Node sub1 = new SubtractionNode(rPowABR,gp1Term1);
		Node ss1 = new SiteSumNode(sub1,true);
		
		Node gp1Term2 = new PowerNode(new ConstantNode(params[4]),new DistanceNode());
		Node pow1 = new PowerNode(new SiteSumNode(gp1Term2,true),new ConstantNode(-1.0));
		Node mult1 = new MultiplicationNode(new ConstantNode(params[3]),pow1);
		Node topNode = new AdditionNode(ss1,mult1);
		TreePotential tree = new TreePotential(topNode,cutoffDistance);
		return tree;
	}
	public static TreePotential generateGP2(double cutoffDistance, double[] params) {
		if(params == null) {
			//Use the default parameters for GP1 from the paper
			params = new double[8];
			params[0] = 7.33;
			params[1] = 3.98;
			params[2] = 3.94;
			params[3] = 27.32;
			params[4] = 11.13;
			params[5] = 0.03;
			params[6] = 11.74;
			params[7] = 2.93;
		}		
		
		Node pairwise0 = generatePairwiseGP(new double[] {params[0],params[1],params[2]});
		
		Node rPowABR = generateRpowABR(new double[] {params[6],params[7]});
		Node mult1 = new MultiplicationNode(new ConstantNode(params[5]),rPowABR);
		Node add1 = new AdditionNode(new ConstantNode(params[4]),mult1);
		Node pairwise1 = new SiteSumNode(add1,true);

		Node add0 = new SubtractionNode(new ConstantNode(params[3]),pairwise1);
		Node embedding0 = new PowerNode(new SiteSumNode(new ConstantNode(1.0),true),new ConstantNode(-1.0));
		Node manybody = new MultiplicationNode(add0,embedding0);
		
		
		Node topNode = new AdditionNode(pairwise0, manybody);
		TreePotential tree = new TreePotential(topNode,cutoffDistance);
		return tree;
	}

}
