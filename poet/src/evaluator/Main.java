package evaluator;
/*
 * Created on Aug 16, 2016
 *
 */

import util.Report;
import util.Statistics;
import util.Util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import org.apache.commons.io.FilenameUtils;
import util.TreePotentials;

import gAEngine.GAEngine;
import gAEngine.IIndividual;
import matsci.engine.minimizer.CGMinimizer;
import matsci.engine.minimizer.GRLinearMinimizer;
import matsci.io.app.log.Status;
import matsci.io.structure.LAMMPSDataFile;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;
import matsci.util.MSMath;
import potentialModelsGP.tree.DistanceNode;
import potentialModelsGP.tree.Node;
import potentialModelsGP.tree.TreePotential;
import potentialModelsGP.tree.neighbors.SiteSumNode;

public class Main {
	
	private static Random GENERATOR = new Random();

    private static String m_Data_Directory;
    private static String m_InputFile_Directory;
    
    private static String[] m_DataNames;
//    private static int m_NumPerDataName;
  private static int m_NumStructures = 0;
//    private static int[] m_NumsPerDataName;
    private static HashMap<String, Integer> m_NumsStructuresPerDataName = new HashMap<String, Integer>();

    private static String[] m_Properties;
    private static double[] m_WeightsProperties;
    private static String m_0K_reference;

	private static PotentialPopulation m_InitialPopulation;
	private static double m_RatioCrossover;
	private static int m_MaxOperations = 10000;
	private static String m_PopulationType;//or BinsHFC
	private static String m_SubPopulationType;
	private static String m_SelectionMethod;
	    
    public static double m_CutoffDistance;
    
    private static DataSet m_DataSetTraining;
    private static DataSet m_DataSetValidation;
    private static int[] m_NetsIndicesTraining;
    private static int[] m_NetsIndicesValidation;
	private static StructureNet[] m_Nets;
    private static StructureNet[] m_NetsTraining;
    private static StructureNet[] m_NetsValidation;
    
    private static int[] m_TrainingSizes;
    private static int m_TrainingSize;
    private static double[] m_FractionsTraining;
    private static ArrayList<Integer> m_indices_training_must_include;
    private static int m_MiniBatchSize = 10;
    

    private static HashMap<String, StructureNet[]> m_PhasesNetsMap = new HashMap<String, StructureNet[]>();
    	  
    public static void main(String[] args) throws Exception { 
//    	Full path of directory that contains input file
    	if(args.length >1 && args[0].equals("printReport")) {
    		String pathOfArchivedRun = args[1];
    		String errorMetric = args[2];
    		String option = args[3];
    		printReport(pathOfArchivedRun, errorMetric, option);
    	}
		m_InputFile_Directory = args[0];	
		Status.setOutfile(m_InputFile_Directory+"/log.java.run");
		Status.useOutForErr();
    	
		readInputFile();
		
		printProcessorInfo();
		
		importData();
		
		m_DataSetTraining.updateDynamicIndices(new ArrayList<PotentialSubPopulation>(), true);
		m_DataSetTraining.updateDynamicIndices(new ArrayList<PotentialSubPopulation>(), false);
		
		initializeCGMinimizer(false);
			
		String verifyModel = "Cu";
		if(m_Data_Directory.contains("LJ")) {
			verifyModel = "Ar";
		}
		
		veify(verifyModel,"(((T[((0.024680033979199996)*(((((3.35)/(r)))^(12.0))))]))+((T[((-1.0)*(((0.024680033979199996)*(((((3.35)/(r)))^(6.0))))))])))");	
		
		if(args.length > 1 && (args[1].equals("optimize_Model_CMA_ES_restarting") | args[1].equals("optimize_Model_Conjugate_Gradient")) ) {
			optimize_Model_CMA_ES_or_ConjugateGradient(args);
		}
		
	  	Status.basic(m_PopulationType+". "+m_SelectionMethod+". NumValidPerMethodPerDepth "+PotentialPopulation.m_numValidPerMethodPerDepth+". Phases "+Arrays.toString(m_DataNames)+". NumStructures "+m_NumStructures+". Crossover "+m_RatioCrossover+". FractionsTraining "+Arrays.toString(m_FractionsTraining));
	  	
		createInitialArchivesPopulations();
	  	createInitialPopulation();
	  		  	
		runGAEnginePareto();
}
    
private static void printProcessorInfo() {
	Process p = null;
	try{//if(!System.getProperty("os.name").contains("Windows")) {
		//Print processor information in the corresponding directory
		ProcessBuilder builder = new ProcessBuilder("lscpu");
		builder.redirectErrorStream(true);
		p = builder.start();
	}catch(IOException e) {
		Status.basic("Warning: could not get processor information "+e.toString());
		Status.basic("Warning: could not get processor information "+e.getMessage());
	}
	if(p!=null) {
	    try(BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()))){
		    String line;
		    while ((line = r.readLine())!=null) {
		        Status.basic(line);
		    }
	    }catch (Exception e) {
			Status.basic("Warning: could not get processor information "+e.toString());
			Status.basic("Warning: could not get processor information "+e.getMessage());
		}

	}
}

/**
 * 
 * @param args: 
 * 			args[2]=model string, 
 * 			args[3]="MiniBatch" or "All" (training data to use), 
 * 			args[4]=initial population size for CMA-ES
 * 			args[5]=increment in population size
 * 			args[6]=number of restarts 
 */
private static void optimize_Model_CMA_ES_or_ConjugateGradient(String[] args) {

	String model = args[2];
	String subsetName = args[3];
	
	PotentialEvaluator pe = new PotentialEvaluator(TreePotential.buildTree(model, m_CutoffDistance), m_DataSetTraining);
		
	String subsetName0 = pe.getDataSet().getSubSet();
	pe.getDataSet().setSubset(subsetName);
	pe.clearValues(false);
	Status.basic("subsetName "+subsetName);
	//Print training errors of the initial model and its expression
	Status.basic("Expression Initial "+ pe.getPotential().getExpression(false));	
	pe.setMinimizeOnlyTheseNodes_all();
	Status.basic("Parameters Initial "+Arrays.toString(pe.getUnboundedParameters(null)));
	Status.basic(pe.getDataSet().getTitle()+" Fitness Initial "+pe.getFitness(null, false));

	String[] properties = pe.getDataSet().getTargetProperties();
	for (String string : properties) {
		double[] predicted = pe.getPredictedProperty(string);
		double[] target = pe.getDataSet().getTargetProperty(string);
		if(string.equals("Energy")) {
			predicted = Statistics.arrayAdd(predicted, -Statistics.getMinValue(predicted));
			target = Statistics.arrayAdd(target, -Statistics.getMinValue(target));
		}
		Status.basic(pe.getDataSet().getTitle()+" "+string+" initial "+Statistics.getMAE(predicted, target));
	}
	
	pe.getDataSet().setSubset(subsetName0);
	pe.clearValues(false);
	if(args[1].equals("optimize_Model_CMA_ES_restarting")) {
		String popSizeInitial = args[4];
		String popSizeIncrement = args[5];
		String numberOfRestarts = args[6];
		pe = pe.optimize_CMA_ES_restarting(new String[] {subsetName, popSizeInitial,popSizeIncrement,numberOfRestarts});
	}
	else if(args[1].equals("optimize_Model_Conjugate_Gradient")) {
		Status.basic("Optimizing with Conjugate Gradient");
//		initializeCGMinimizer(true);
		Status.basic(pe.getExpression());
		pe.getDataSet().setSubset(subsetName);
		pe.clearValues(false);
		
		pe = pe.optimizeConstants(new String[] {"NLCG","100","100"});
//		initializeCGMinimizer(false);
		pe.getDataSet().setSubset(subsetName0);
		pe.clearValues(false);
	}
	
	pe.getDataSet().setSubset(subsetName);
	pe.clearValues(false);
	//Print training errors of the final model and its expression
	Status.basic("Expression Final "+ pe.getPotential().getExpression(false));	
	pe.setMinimizeOnlyTheseNodes_all();
	Status.basic("Parameters Final "+Arrays.toString(pe.getUnboundedParameters(null)));
	Status.basic(pe.getDataSet().getTitle()+" Fitness Final "+pe.getFitness(null, false));

	for (String string : properties) {
		double[] predicted = pe.getPredictedProperty(string);
		double[] target = pe.getDataSet().getTargetProperty(string);
		if(string.equals("Energy")) {
			predicted = Statistics.arrayAdd(predicted, -Statistics.getMinValue(predicted));
			target = Statistics.arrayAdd(target, -Statistics.getMinValue(target));
		}
		Status.basic(pe.getDataSet().getTitle()+" "+string+" Final "+Statistics.getMAE(predicted, target));
	}
	
	
	//Print validation errors of the final model and its expression
	pe = new PotentialEvaluator(pe.getPotential(), m_DataSetValidation);
	for (String string : properties) {
		double[] predicted = pe.getPredictedProperty(string);
		double[] target = pe.getDataSet().getTargetProperty(string);
		if(string.equals("Energy")) {
			predicted = Statistics.arrayAdd(predicted, -Statistics.getMinValue(predicted));
			target = Statistics.arrayAdd(target, -Statistics.getMinValue(target));
		}
		Status.basic(pe.getDataSet().getTitle()+" "+string+" Final "+Statistics.getMAE(predicted, target));
	}
	pe.getDataSet().setSubset(subsetName0);
	pe.clearValues(false);
	System.exit(0);
}

private static void sandbox(String[] args) throws NumberFormatException, IOException, NotFiniteValueException {
	String modelStr = "((((2.1797358241531355)*((T[((r)^(((8.272009588955926)-(((5.371688561318197)*(r))))))]))))+(((((36.525731908741946)-(((0.0019043397213661358)*((T[((r)^(((18.748418839919033)-(((4.436394099934352)*(r))))))]))))))/((T[1.0])))))";
	TreePotential tp = TreePotential.buildTree(modelStr, 5.0);
//	AppliedTreePotential atp = new AppliedTreePotential(tp, m_DataSetTraining.getMinEnergyNet("FCC"));
	PotentialEvaluator pe = new PotentialEvaluator(tp, m_DataSetTraining);
	m_DataSetTraining.setSubset("MiniBatch");
	System.out.println(pe.computePredictedProperty("C11_FCC"));
	System.out.println(pe.computePredictedProperty("C12_FCC"));
	System.out.println(pe.computePredictedProperty("C44_FCC"));

	System.exit(0);

	
	double maxEnergy = -36.17;
	Util.printIterationsOUTCARs(10.0,"path/GA_jobs/","GA_relaxed/",maxEnergy);
	System.exit(0);
	
	String parentPath = args[0]+"/";
	for (int i = Integer.parseInt(args[1]); i < Integer.parseInt(args[2]); i++) {
		String poscarName = parentPath+i+".vasp";
//		if(!(new File(poscarName)).exists()) {
//			continue;
//		}
		POSCAR poscar = new POSCAR(poscarName);
		LAMMPSDataFile lmp = new LAMMPSDataFile(poscar);
		lmp.writeFile(parentPath+i+".lmpstruct");
	}

}
private static void computePOET() throws IOException, NotFiniteValueException {
	SiteSumNode.setR(3.0, 5.0);
	String treeString = "((4.377056425955665)*(((((0.5036356639590737)^((T[((0.7679265845060006)^(((1.938533318201748)^(r))))]))))+(((303.92325174644816)*((F[((0.031303589294136666)^(r))])))))))";
	TreePotential tree = TreePotential.buildTree(treeString, SiteSumNode.getRout());
	POSCAR poscar = new POSCAR(System.getProperty("user.dir") + "/POSCAR");
	StructureNet net = new StructureNet(new Structure(poscar),SiteSumNode.getRout());
	AppliedTreePotential atp = new AppliedTreePotential(tree, net);
	atp.relax(false);
	PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(System.getProperty("user.dir") +"/OUTCAR")));
	writer.println("  energy  without entropy=      "+atp.getEnergy()+"  energy(sigma->0) =     "+atp.getEnergy());
	writer.println(" General timing and accounting informations for this job:");
	POSCAR poscarCONTCAR = new POSCAR(atp.getStructureNet().getStructure());
	poscarCONTCAR.writeFile(System.getProperty("user.dir") + "/CONTCAR");
	writer.close();
}
//private static void learningCurves(String[] args) throws IOException {
//	String rootDirectory = args[0];
//	String globalPopulationName = args[1];
//	String errorMetric = args[2];
//	String snapshotName = args[3];
//	String runNum = args[4];
//	String option = args[5];
//
//	printReport(rootDirectory, globalPopulationName, errorMetric, snapshotName, runNum, option);
//}

private static void printReport(String pathOfArchivedRun, String errorMetric, String option) throws IOException {
	//TODO re-implement iteration vs best model
	
	Status.setOutfile(pathOfArchivedRun+"/results/log.java.run");
	Status.useOutForErr();
//	String runDir = rootDirectory+"/runs/"+runNum;
	m_InputFile_Directory = pathOfArchivedRun+"/1";
//	String resultsDir = rootDirectory+"/results/"+runNum+"/"+globalPopulationName;
//	String absoluteDirectory = resultsDir+"/"+snapshotName;
//	String populationFile = absoluteDirectory+"/snapshot";
	
	readInputFile();
	//Need to reset this so that POET doesn't use the PotentialPopulation.m_GlobalDirectory read from inputPM.txt
	PotentialPopulation.m_GlobalDirectory = pathOfArchivedRun;
	importData();
	
//	if(m_Data_Directory.contains("DFT")) {
//		boolean b = false;
//		for (int i = 0; i < m_Properties.length; i++) {
//			if(m_Properties[i].contains("Stress")) {
//				b = true;
//				break;
//			}
//		}
//		//add stress for validation
//		if(!b) {
//			importKnownStresses();
//			m_DataSetTraining.setWeight("Stress", 0.0);
//		}
//	}

//	String[] properties = new String[]{"Energy", "Force", "Stress"}; // TODO how to use data of a different name than the one used for training for validation
	for (String string : m_Properties) {
		if(!(string.equals("Energy")||string.equals("Force")||string.equals("Stress"))) {// TODO check this
			continue;
		}
		m_DataSetValidation.setWeight(string, m_DataSetTraining.getWeight(string));
	}
	
	if(option.equals("predictedVsTarget")) {
		printReportPredictedVsTarget(pathOfArchivedRun+"/results", errorMetric);
	}
//	else if(option.equals("iterations")) {
//		String iterationsFilePath = runDir+"/"+globalPopulationName+"_iterations";
//		printReportErrorsVsIteration(iterationsFilePath, resultsDir, errorMetric);
//	}
	System.exit(0);
}

private static void printReportErrorsVsIteration(String iterationsFilePath, String resultsDir, String errorMetric) throws IOException {
	//Error vs iteration
	//Create a population file with the fittest models from each iteration
	//Read from the iterations file
	BufferedReader iterationsReader = new BufferedReader(new FileReader(iterationsFilePath));
	
	ArrayList<String> models = new ArrayList<String>();
	ArrayList<String> iterationTime = new ArrayList<String>();
	String previousLine = null;
	String line = null;
	while((line = iterationsReader.readLine()) != null){
		String[] lineArray = line.split(" ");
		if(lineArray[0].equals("IterationNum")) {
			iterationTime.add(lineArray[1]+" "+lineArray[3]);
			if(previousLine != null && !previousLine.contains("IterationNum")) {
				models.add(previousLine);
			}
		}
		previousLine = line;
	}
	models.add(previousLine);
	iterationsReader.close();
	
	String populationFilePath = resultsDir+"/iterations.population.data";
	File populationFile = new File(populationFilePath);
	populationFile.createNewFile();
	
	PrintWriter populationFileWriter = new PrintWriter(new BufferedWriter(new FileWriter(populationFilePath)));
	for (String model : models) {
		populationFileWriter.println(model);
	}
	populationFileWriter.close();
	
	String fileNameAbsolutePathTraining = resultsDir+"/iterations."+m_DataSetTraining.getTitle()+".data";
	Report reportTraining = new Report(m_DataSetTraining, populationFilePath);
	reportTraining.printErrorsVsIteration(fileNameAbsolutePathTraining, errorMetric,iterationTime);
	
	if(m_DataSetValidation!=null) {
		String fileNameAbsolutePathValidation = resultsDir+"/iterations."+m_DataSetValidation.getTitle()+".data";
		Report reportValidation= new Report(m_DataSetValidation, populationFilePath);
		reportValidation.printErrorsVsIteration(fileNameAbsolutePathValidation, errorMetric,iterationTime);
	}
}

private static void printReportPredictedVsTarget(String absoluteDirectory, String errorMetric) {
	String populationFile = absoluteDirectory+"/snapshot";
	char[] dataDirectoryPath = m_Data_Directory.toCharArray();
	
	String dataDirectoryNameInverse = "";
	for (int i = 0; i < dataDirectoryPath.length; i++) {
		char c = dataDirectoryPath[dataDirectoryPath.length-i-1];
		if(c == '\\' || c =='/'){
			break;
		}
		dataDirectoryNameInverse += c;
	}
	char[] characters = dataDirectoryNameInverse.toCharArray();
	String dataDirectoryName = "";
	for (int i = 0; i < characters.length; i++) {
		dataDirectoryName += characters[characters.length-i-1];
	}
	
	m_DataSetTraining.setDataDirectoryName(dataDirectoryName);
	m_DataSetTraining.setTitle("Training");
	Report reportTraining = new Report(m_DataSetTraining, populationFile);
	reportTraining.printErrors(absoluteDirectory, errorMetric);

	if(m_DataSetValidation!=null) {
		m_DataSetValidation.setDataDirectoryName(dataDirectoryName);
		m_DataSetValidation.setTitle("Validation");
		Report reportValidation = new Report(m_DataSetValidation, populationFile);
		reportValidation.printErrors(absoluteDirectory, errorMetric);
	}
}

/**
 * Create all the files needed for a job
 */
private static void createInitialArchivesPopulations() {
	if(!PotentialPopulation.lock(PotentialPopulation.m_GlobalDirectory+"/global.lock")) {
		Status.basic("Couldn't lock global directory to create local population files!");
		new Exception().printStackTrace();
	}
	
	String localPath = PotentialPopulation.m_GlobalDirectory+"/"+1;
    File local = new File(localPath+"/local");
    File globalTrainingFrontierMSE = new File(PotentialPopulation.m_GlobalDirectory+"/globalTrainingFrontierMSE");
    File globalTrainingFrontierMSEiterations = new File(PotentialPopulation.m_GlobalDirectory+"/globalTrainingFrontierMSE_iterations");
    File globalTrainingFrontierLC = new File(PotentialPopulation.m_GlobalDirectory+"/globalTrainingFrontierLC");
    File globalTrainingFrontierLCiterations = new File(PotentialPopulation.m_GlobalDirectory+"/globalTrainingFrontierLC_iterations");
        
	//Need this to use dynamic indices
    String subSet = m_DataSetTraining.getSubSet();
    m_DataSetTraining.setSubset("Local");
//	ArrayList<PotentialEvaluator> individuals = new ArrayList<PotentialEvaluator>();
//	for (int i = 0; i < 10; i++) {
//		PotentialEvaluator individual;
//		do {
//			TreePotential simplifiedTree = TreePotential.generateTree(m_CutoffDistance, PotentialEvaluator.m_MaxDepthRandomTree, 'f').simplify();
//			individual = new PotentialEvaluator(simplifiedTree, m_DataSetTraining);
//		}while(!individual.isValid());
//		individuals.add(individual);
//	}
	 ArrayList<PotentialEvaluator> individuals = new ArrayList<PotentialEvaluator>();
	 int c0 = 0;
	 while(c0 < PotentialEvaluator.m_MaxLoops) {
		 PotentialEvaluator pe;
		 do {
			 pe = new PotentialEvaluator(new TreePotential(getLinearCombSiteSums(c0+1), m_CutoffDistance), m_DataSetTraining);
		 }while(pe.getPotential().numSiteSums() != c0+1 || !pe.isValid());
		 individuals.add(pe);
		 c0++;
	}
	
    m_DataSetTraining.setSubset(subSet);
	
	//Create local files
	int c = 0;
	while(c<PotentialPopulation.m_NumProcessors) {
		localPath = PotentialPopulation.m_GlobalDirectory+"/"+(c+1);
	    local = new File(localPath+"/local");
		try {
			local.createNewFile();
		    FileWriter fw = new FileWriter(local,true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			for (PotentialEvaluator individual : individuals) {
				out.println(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
			}
			out.close();

		} catch (IOException e) {
			Status.basic("Problem creating initial local file!");
			e.printStackTrace();
		}
		c++;
	}
    
	try {
		globalTrainingFrontierLCiterations.createNewFile();
		globalTrainingFrontierMSEiterations.createNewFile();

		PrintWriter outLCiterations = new PrintWriter(new BufferedWriter(new FileWriter(globalTrainingFrontierLCiterations)));
		PrintWriter outMSEiterations = new PrintWriter(new BufferedWriter(new FileWriter(globalTrainingFrontierMSEiterations)));
		outLCiterations.println("IterationNum 0 "+individuals.size()+" 0.0");
		outMSEiterations.println("IterationNum 0 "+individuals.size()+" 0.0");
		
		//This file doesn't exist if the run wasn't seeded		
//		if(!globalTrainingFrontierMSE.exists()) {
		globalTrainingFrontierLC.createNewFile();
		globalTrainingFrontierMSE.createNewFile();

		PrintWriter outLC = new PrintWriter(new BufferedWriter(new FileWriter(globalTrainingFrontierLC,true)));
		PrintWriter outMSE = new PrintWriter(new BufferedWriter(new FileWriter(globalTrainingFrontierMSE,true)));
		for (PotentialEvaluator individual : individuals) {
			//Write to global populations if they did not exist

			outLC.println(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
			outMSE.println(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
			outLCiterations.println(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
			outMSEiterations.println(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
		}
		outLC.close();
		outMSE.close();
//		}
//		else {
//			String[] strings = new String[] {"MSE", "LC"};
//			for (String s : strings) {
//				String file = PotentialPopulation.m_GlobalDirectory+"/globalTrainingFrontier"+s;
//				BufferedReader br = new BufferedReader(new FileReader(file));
//				String line;
//				while((line = br.readLine())!= null){
//					if(s.equals("MSE")) {
//						outMSEiterations.println(line);
//					}
//					else {
//						outLCiterations.println(line);
//					}
//				}
//				br.close();
//			}
//		}
		outLCiterations.close();
		outMSEiterations.close();


	} catch (IOException e) {
		Status.basic("Problem creating initial global archives!");
		e.printStackTrace();
	}
    
	if(!PotentialPopulation.unlock(PotentialPopulation.m_GlobalDirectory+"/global.lock")) {
		Status.basic("Couldn't unlock from createInitialArchives() from processor "+PotentialPopulation.m_ProcessorNum);
		new Exception().printStackTrace();
	}
}

private static Node getLinearCombSiteSums(int numSiteSums) {
	Node topNode = new SiteSumNode(new DistanceNode(),true);
	for (int i = 0; i < numSiteSums - 1; i++) {
		topNode = TreePotential.combineAddOrSubtract(topNode.deepCopy(), new SiteSumNode(new DistanceNode(),true));
	}
	return topNode;
}
private static void importKnownForces() throws IOException {
	
	//Import all the forces of all the phases for MD
	double[][] forcesMD = new double[m_NumStructures][];
	for (int i = 0; i < forcesMD.length; i++) {
		forcesMD[i] = new double[m_Nets[i].numDefiningSites()*3];
	}
	int index = 0;
	for (int i = 0; i < m_DataNames.length; i++) {
		if(!m_DataNames[i].contains("Strain")) {
			double[][] forcesPhasei = getKnownValues(m_DataNames[i],m_NumsStructuresPerDataName.get(m_DataNames[i]),"Forces");
			for (int j = 0; j < forcesPhasei.length; j++) {
				forcesMD[index+j] = forcesPhasei[j];
			}
			index += m_NumsStructuresPerDataName.get(m_DataNames[i]);
		}
	}
	
	int[] trainingIndices = m_NetsIndicesTraining;
	int[] validationIndices = m_NetsIndicesValidation;

	double[][] training = new double[trainingIndices.length][2];
	
	for (int i = 0; i < training.length; i++) {
		training[i] = forcesMD[trainingIndices[i]];
	}
	m_DataSetTraining.setTargetProperty("Force", training);
	
	if(m_DataSetValidation!=null) {
		double[][] validation = new double[validationIndices.length][2];
		for (int i = 0; i < validation.length; i++) {
			validation[i] = forcesMD[validationIndices[i]];
		}
		m_DataSetValidation.setTargetProperty("Force", validation);
	}
}

private static void importKnownEnergies() throws IOException {
	
	ArrayList<Double> energiesMD = new ArrayList<Double>();

	for (int i = 0; i < m_DataNames.length; i++) {
		double[] energies = getKnownEnergies(true, m_DataNames[i], m_NumsStructuresPerDataName.get(m_DataNames[i]));
		for (int j = 0; j < energies.length; j++) {
			energiesMD.add(energies[j]);
		}
	}
	
	
	double[] energies = new double[energiesMD.size()];
	for (int i = 0; i < energies.length; i++) {
		energies[i] = energiesMD.get(i);
	}
	
	//Convert to energies per atom
	for (int i = 0; i < energies.length; i++) {
		energies[i] /= m_Nets[i].numDefiningSites();
	}
	
	int[] trainingIndices = m_NetsIndicesTraining;
	int[] validationIndices = m_NetsIndicesValidation;

	double[] training = new double[trainingIndices.length];
	
	for (int i = 0; i < training.length; i++) {
		training[i] = energies[trainingIndices[i]];
	}
	//Convert to double[][]
	double[][] values = new double[training.length][2];
	for (int i = 0; i < values.length; i++) {
		values[i] = new double[] {training[i]};
	}
	m_DataSetTraining.setTargetProperty("Energy", values);
	
	if(m_DataSetValidation != null) {
		double[] validation = new double[validationIndices.length];
		for (int i = 0; i < validation.length; i++) {
			validation[i] = energies[validationIndices[i]];
		}
		//Convert to double[][]
		double[][] valuesValidation = new double[validation.length][2];
		for (int i = 0; i < valuesValidation.length; i++) {
			valuesValidation[i] = new double[] {validation[i]};
		}
		m_DataSetValidation.setTargetProperty("Energy", valuesValidation);
	}
}

private static ArrayList<Double> getValues(String path) throws IOException{
	  ArrayList<Double> list = new ArrayList<Double>();
	  BufferedReader brF = new BufferedReader(new FileReader(path)); 
	  String lineF;
	  while((lineF = brF.readLine()) != null){
		  StringTokenizer t = new StringTokenizer(lineF, " ");
		  list.add(Double.parseDouble(t.nextToken()));
	  }
	  brF.close();
	  return list;
}
private static double[] getKnownEnergies(boolean training, String dataname, int num) throws IOException {
	  ArrayList<Double> list = getValues(m_Data_Directory+"/"+dataname+"/E0.data");
	  
	  double[] energies = new double[list.size()];
	  for (int i = 0; i < energies.length; i++) {
		  energies[i] = list.get(i);
	  }
	  double[] returnArray = new double[num];
	  if(training){
		  for (int i = 0; i < returnArray.length; i++) {
			  returnArray[i] = energies[i];
		  }
	  }
	  else {
		  for (int i = 0; i < returnArray.length; i++) {
			  int index = (energies.length-num)+i;
			  returnArray[i] = energies[index];
		  }
	  }
	  	
	return returnArray;
}


private static double[][] getKnownValues(String phase, Integer num, String option) throws IOException {

	ArrayList<Double> list = new ArrayList<Double>();
	  String valuesFile;
	  if(option.equals("Forces")) {
		  if(!(m_Data_Directory.contains("EAM")||m_Data_Directory.contains("LJ"))) {
			  valuesFile = m_Data_Directory+"/"+phase+"/F_coord.data";
			  list = getForces(valuesFile, phase); 
		  }
		  else {
			  valuesFile = m_Data_Directory+"/"+phase+"/F.data";
			  list = getValues(valuesFile);
		  }
	  }
	  else if(option.equals("Stresses")) {
		  valuesFile = m_Data_Directory+"/"+phase+"/F_Cell.data";
		  list = getValues(valuesFile,6);
	  }
	  else {
		  valuesFile = null;
	  }

	  int c = 0;
//	  First dimension: each structure
//	  Second dimension: values of each structure
	  double[][] values;

	  if(option.equals("Forces")) {
		  values = new double[num][];
		  for(int i = 0; i < m_PhasesNetsMap.get(phase).length; i++) {
			  values[i] = new double[3*m_PhasesNetsMap.get(phase)[i].numDefiningSites()];
		  }
	  }
	  else if(option.equals("Stresses")) {
		  values = new double[num][6];
	  }
	  else {
		  values = null;
	  }
	  
	  for (int i = 0; i < values.length; i++) {
		  for (int j = 0; j < values[i].length; j++) {
			  try {
				  values[i][j] = list.get(c);
			  }catch(IndexOutOfBoundsException e) {
				  Status.detail("IndexOutOfBoundsException: Index is "+j+" but length of array is "+values[i].length);
			  }
			  c++;
		  }
	  }
	  return values;
}

private static ArrayList<Double> getValues(String oUTCAR,int numColumns) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(oUTCAR));
	ArrayList<Double> values = new ArrayList<Double>();
	String line;
	while((line = br.readLine())!=null) {
		StringTokenizer st = new StringTokenizer(line);
		for (int i = 0; i < numColumns; i++) {
			values.add(Double.parseDouble(st.nextToken()));
		}
	}
	br.close();
	return values;
}
private static ArrayList<Double> getForces(String oUTCAR, String phase) throws IOException {	

	int numSitesTotal = 0;
	for(StructureNet net : m_PhasesNetsMap.get(phase)) {
		numSitesTotal += net.numDefiningSites();
	}
	
	BufferedReader br = new BufferedReader(new FileReader(oUTCAR));
	double[][] data = new double[numSitesTotal][6];
	
	String line;
	int c = 0;
	while((line = br.readLine())!=null && c < data.length) {
		StringTokenizer st = new StringTokenizer(line);
		for (int i = 0; i < 6; i++) {
			data[c][i]=Double.parseDouble(st.nextToken());
		}
		c++;
	}
	br.close();

	//Order data wrt the coordinates in the nets
	StructureNet[] nets = m_PhasesNetsMap.get(phase);

	double[][] coordinates = new double[numSitesTotal][3];
	int count = 0;
	for (int i = 0; i < nets.length; i++) {
		for (int j = 0; j < nets[i].numDefiningSites(); j++) {
			coordinates[count] = nets[i].getDefiningSite(j).getCoords().getCartesianArray();
			count++;
		}
	}
	double[][] orderedData = new double[coordinates.length][];
	
	for (int i = 0; i < coordinates.length; i++) {
		orderedData[i] = data[Util.find(coordinates[i], data)];
	}
	
	ArrayList<Double> values = new ArrayList<Double>();
	for (int i = 0; i < orderedData.length; i++) {
		for (int j = 3; j < 6; j++) {
			values.add(orderedData[i][j]);
		}
	}
	
	return values;
}
private static void importStructures() {
	ArrayList<StructureNet> structuresMD = new ArrayList<StructureNet>();

	for (int i = 0; i < m_DataNames.length; i++) {
		StructureNet[] structuresPhase = getStructures(m_DataNames[i],m_NumsStructuresPerDataName.get(m_DataNames[i]));
		for (int j = 0; j < structuresPhase.length; j++) {
			structuresMD.add(structuresPhase[j]);		}
	}
		
	m_Nets = new StructureNet[structuresMD.size()];
	int c = 0;
	for (StructureNet structureNet : structuresMD) {
		if(m_Data_Directory.contains("DFT") && !Arrays.toString(m_DataNames).contains("SNAP")) {
			m_Nets[c] = structureNet.getAlignedVASP();
		}
		else if (!Arrays.toString(m_DataNames).contains("SNAP")){
			m_Nets[c] = structureNet.getAlignedLAMMPS();
		}
		else {
			m_Nets[c] = structureNet;
		}
		c++;
	}

	int[] trainingIndices = m_NetsIndicesTraining;
	int[] validationIndices = m_NetsIndicesValidation;

	StructureNet[] trainingNets = new StructureNet[m_NetsIndicesTraining.length];
	
	for (int i = 0; i < trainingNets.length; i++) {
		trainingNets[i] = m_Nets[trainingIndices[i]];
	}
	
	m_NetsTraining = trainingNets;
	
	if(m_TrainingSize < m_NumStructures) {
		StructureNet[] validationNets = new StructureNet[m_NetsIndicesValidation.length];
		for (int i = 0; i < validationNets.length; i++) {
			validationNets[i] = m_Nets[validationIndices[i]];
		}
		m_NetsValidation = validationNets;
	}
}

private static StructureNet[] getStructures(String dataname, int numTraining) {
	//  Create a StructureNet object for each structure in the structuresDirectory
	final ArrayList<Structure> structuresArrayList = new ArrayList<Structure>();
	
	boolean iterate = true;
	int count = 0;
	
	while(iterate){
		String filePathString = "";
		if(m_Data_Directory.contains("DFT_Cu")&&dataname.equals("MD_relax_DFT_fixedvolume")){
			filePathString = m_Data_Directory+"/"+dataname+"/dft.relax."+count+".CONTCAR.vasp";
		}
		else if(m_Data_Directory.contains("DFT_Cu")&&dataname.equals("MD_relax_DFT_fixedatom")){
			if(count<1) {count++;}
			filePathString = m_Data_Directory+"/"+dataname+"/dft.relax."+count+".CONTCAR.vasp";
		}
		else if(m_Data_Directory.contains("MD_MeltQuench_LJ_He")){
			filePathString = m_Data_Directory+"/"+dataname+"/testout."+(count+1)+".POSCAR.vasp";
		}
		else if(m_Data_Directory.contains("Cu32atoms_Optimized")){
			filePathString = m_Data_Directory+"/optimized."+(count+1)+".POSCAR.vasp";
		}
		else{//(structuresDirectory.contains("K")){
			filePathString = m_Data_Directory+"/"+dataname+"/"+(count+1)+".vasp";
		}
		
		File file = new File(filePathString);
		if(!file.isFile()){break;}
		POSCAR infile = new POSCAR(filePathString);
		Structure structure = new Structure(infile);
		structuresArrayList.add(structure);
		count++;
		if(structuresArrayList.size() >= numTraining) {
			break;
		}
	}
	
	Structure[] structures = new Structure[structuresArrayList.size()];
	structuresArrayList.toArray(structures);
	StructureNet[] nets = new StructureNet[structures.length];
	
	for(int i = 0; i < structures.length; i++){
		nets[i] = new StructureNet(structures[i],m_CutoffDistance);
	}
	m_PhasesNetsMap.put(dataname, nets);
	return nets;
}


private static void initializeCGMinimizer(boolean tightParams) {
	int maxNumSteps = 3;
	double minAllowedStep = 1E-4;
	double maxAllowedStep = Double.MAX_VALUE;
	double minGrad = 1E-4;
	int maxIterations = 1;
	if(tightParams) {
		maxNumSteps = 50;
		minAllowedStep = 1E-5;
		maxAllowedStep = Double.MAX_VALUE;
		minGrad = 1E-5;
		maxIterations = 50;
	}

    GRLinearMinimizer linearMinimizer = new GRLinearMinimizer();
    linearMinimizer.setMaxNumSteps(maxNumSteps);
    linearMinimizer.setGradMagConvergence(minGrad);
    linearMinimizer.setMinAllowedStep(minAllowedStep);
    linearMinimizer.setMaxAllowedStep(maxAllowedStep);

    CGMinimizer cgminimizer = new CGMinimizer(linearMinimizer);
    cgminimizer.setGradMagConvergence(minGrad);
    cgminimizer.setMinAllowedStep(minAllowedStep);
    cgminimizer.setMaxAllowedStep(maxAllowedStep);
    CGMinimizer.m_MaxIterations = maxIterations;
    
    PotentialEvaluator.m_Minimizer = cgminimizer;
}

/**
 * 
 * @throws IOException
 */
private static void importData() throws IOException {
	importTrainingAndValidationIndices();
	importStructures();

	m_DataSetTraining = new DataSet(m_NetsTraining);
	m_DataSetTraining.setTitle("Training");
	m_DataSetTraining.setSubset("Local");
	m_DataSetTraining.setSize("MiniBatch", m_MiniBatchSize);
//	m_DataSetTraining.setSize("Local", m_LocalBatchSize);

	if(PotentialPopulation.m_ProcessorNum == 12) {
		m_DataSetTraining.setSize("Local", 10);
	}
	else if(PotentialPopulation.m_ProcessorNum > 9 && PotentialPopulation.m_ProcessorNum < 12) {
		m_DataSetTraining.setSize("Local", 7);
	}
	else {
		m_DataSetTraining.setSize("Local", 5);
	}
	
	if(m_NetsValidation != null) {
		m_DataSetValidation = new DataSet(m_NetsValidation);
		m_DataSetValidation.setSubset("All");
		m_DataSetValidation.setTitle("Validation");
	}
	
	for (int i = 0; i < m_Properties.length; i++) {
		String string = m_Properties[i];
		if(string.equals("Energy")) {
			importKnownEnergies();
		}
		else if(string.equals("Force")) {
			importKnownForces();
		}
		else if(string.equals("Stress")) {
			importKnownStresses();
		}
		else if(string.contains("_0K")) {
//			importKnown_0K(string, true);
		}
		m_DataSetTraining.setWeight(string, m_WeightsProperties[i]);
	}
	
	if(m_Data_Directory.contains("LJ")) {
		//Both Inclusive, maxDepth also determines the max allowed number of distances and number of node
//		PotentialEvaluator.m_MinDepthRandomTree = 4;
//		PotentialEvaluator.m_MaxDepthRandomTree = 6;
		PotentialEvaluator.m_MaxLoops = 3;
		PotentialEvaluator.m_MaxSiteSumsUnderSiteSum = 0;
	}
	if(m_0K_reference != null) {
		importKnown_0K();
	}
}

private static void importKnown_0K() throws IOException {
	String dir = m_Data_Directory+"/0K";
	
	BufferedReader br = new BufferedReader(new FileReader(dir+"/TargetProperties_0K.data"));
	String line;
	//Read all the lines until finding m_0K_reference
	point0:
	while((line = br.readLine()) != null){
		String[] split = line.split("\\s+");
		for (String string : split) {
			if(string.equals(m_0K_reference)) {
				Status.basic("Reference for target properties 0K: "+line);
				break point0;
			}
		}	
	}
//	Store the values from the input file in a Map so that the order of the keywords in the input file does not matter
	Map<String, String> map = new HashMap <String, String>();
	while((line = br.readLine()) != null){
		String[] split = line.split("\\s+");
		if(split[0].contains("reference_")) {
			break;
		}
		if(line.isEmpty()) {
			continue;
		}
		ArrayList<String> list = new ArrayList<String>();
		for (String string : split) {
			if(string.contains("#")) {
				break;
			}
			list.add(string);
		}
		if(list.size() > 2) {
			map.put(list.get(0)+"_"+list.get(1), list.get(2));
		}
	}
	br.close();	
	
	Status.basic("Training properties");
	ArrayList<String> training = new ArrayList<String>();
	for (String property : m_Properties) {
		training.add(property);
		if(map.get(property) != null) {
			double[][] value = new double[1][1];
			value[0][0] = Double.parseDouble(map.get(property));
			Status.basic(property+" "+value[0][0]);
			m_DataSetTraining.setTargetProperty(property, value);
			m_DataSetTraining.setIndices("MiniBatch", property, m_DataSetTraining.getIndices("All", property));
		}
	}
	ArrayList<String> validation = new ArrayList<String>();
	for (String property : map.keySet()) {
		if(!training.contains(property)) {
			validation.add(property);
		}
	}
	Status.basic("Validation properties");
	for (String property : validation) {
		double[][] value = new double[1][1];
		value[0][0] = Double.parseDouble(map.get(property));
		Status.basic(property+" "+value[0][0]);
		m_DataSetValidation.setTargetProperty(property, value);
	}
	
	File folder = new File(dir);
	File[] listOfFiles = folder.listFiles();
	for (int i = 0; i < listOfFiles.length; i++) {
		String name = listOfFiles[i].getName();
		if(FilenameUtils.getExtension(name).equals("vasp")) {
			String basename = FilenameUtils.getBaseName(name);
			Structure structure = new Structure(new POSCAR(listOfFiles[i].getAbsolutePath()));
			m_DataSetTraining.setMinEnergyNet(basename, new StructureNet(structure,m_CutoffDistance));
			m_DataSetValidation.setMinEnergyNet(basename, new StructureNet(structure,m_CutoffDistance));

		}
	}
}

private static void importKnownStresses() throws IOException {
	
	//Import all the forces on the cell of all the phases for MD
	double[][] stressesMD = new double[m_NumStructures][];
	for (int i = 0; i < stressesMD.length; i++) {
		stressesMD[i] = new double[6];
	}
	int index = 0;
	for (int i = 0; i < m_DataNames.length; i++) {
		if(!m_DataNames[i].contains("Strain")) {
			double[][] stressesPhasei = getKnownValues(m_DataNames[i],m_NumsStructuresPerDataName.get(m_DataNames[i]),"Stresses");
			for (int j = 0; j < stressesPhasei.length; j++) {
				stressesMD[index+j] = MSMath.arrayMultiply(stressesPhasei[j], -1.0/10.0);
			}
			index += m_NumsStructuresPerDataName.get(m_DataNames[i]);
		}
	}
		
	int[] trainingIndices = m_NetsIndicesTraining;
	int[] validationIndices = m_NetsIndicesValidation;
	
	double[][] training = new double[trainingIndices.length][2];
	
	for (int i = 0; i < training.length; i++) {
		training[i] = stressesMD[trainingIndices[i]];
	}

	m_DataSetTraining.setTargetProperty("Stress", training);
	
	if(m_DataSetValidation != null) {
		double[][] validation = new double[validationIndices.length][2];
		for (int i = 0; i < validation.length; i++) {
			validation[i] = stressesMD[validationIndices[i]];
		}
		
//		//Convert to stress instead of force on cell and to GPa
//		for (int i = 0; i < validation.length; i++) {
//			for (int j = 0; j < validation[i].length; j++) {
//				validation[i][j] /= (-10.0);
//			}
//		}
		m_DataSetValidation.setTargetProperty("Stress", validation);
	}
}

private static void importTrainingAndValidationIndices() {
	if(!PotentialPopulation.lock(PotentialPopulation.m_GlobalDirectory+"/global.lock")) {
		Status.basic("Couldn't lock to import training and validation indices!");
		new Exception().printStackTrace();
		return;
	}
	
	String filePath = PotentialPopulation.m_GlobalDirectory+"/trainingAndValidationIndices";
	int[] trainingIndices = null; 
	int[] validationIndices = null;
	
    File trainingAndValidationIndicesFile = new File(PotentialPopulation.m_GlobalDirectory+"/trainingAndValidationIndices");

    //create the file if it doesn't exist
    if(!trainingAndValidationIndicesFile.exists()){
    	try {
    		trainingAndValidationIndicesFile.createNewFile();
    	    FileWriter fw = new FileWriter(trainingAndValidationIndicesFile);
    		BufferedWriter bw = new BufferedWriter(fw);
    		PrintWriter out = new PrintWriter(bw);

//    		int numRandomIndices = m_TrainingSize;
//    		trainingIndices = Statistics.generateRandomIndices(numRandomIndices, m_NumStructures, m_indices_training_must_include);
        	//Train on first snapshots of molecular dynamics 
    		trainingIndices = new int[m_TrainingSize];
    		int c = 0;
    		int c1 = 0;
        	for (int i = 0; i < m_DataNames.length; i++) {
        		int num = m_NumsStructuresPerDataName.get(m_DataNames[i]);
        		int numTrain = (int)(m_FractionsTraining[i]*num);
        		for (int j = 0; j < num; j++) {
					if(j<numTrain) {
						trainingIndices[c1] = c;
						c1++;
					}
					c++;
				}
    		}

    		
    		validationIndices = new int[m_NumStructures-trainingIndices.length];
    		int index = 0;
    		for (int i = 0; i < m_NumStructures; i++) {
    			boolean isInTraining = false;
    			for (int j = 0; j < trainingIndices.length; j++) {
    				if(i==trainingIndices[j]) {
    					isInTraining = true;
    					break;
    				}
    			}
    			if(!isInTraining) {
    				validationIndices[index] = i;
    				index ++;
    			}
    		}
    		for (int i = 0; i < trainingIndices.length; i++) {
				out.print(trainingIndices[i]+" ");
			}
    		out.println();
    		for (int i = 0; i < validationIndices.length; i++) {
				out.print(validationIndices[i]+" ");
			}
    		out.close();
		} catch (IOException e) {
			Status.basic("Problem creating initial training and validation indices file!");
			e.printStackTrace();
		}
    }
    else {
    	try{
    		BufferedReader br = new BufferedReader(new FileReader(filePath));
    		StringTokenizer tokenizer = new StringTokenizer(br.readLine());
    		trainingIndices = new int[tokenizer.countTokens()];
    		for (int j = 0; j < trainingIndices.length; j++) {
    			trainingIndices[j] = Integer.parseInt(tokenizer.nextToken());
    		}
//    		if(!(m_Data_Directory.contains("LJ_Ar"))) {
    		String nextLine = br.readLine();
    		if(nextLine!=null) {
    			tokenizer = new StringTokenizer(nextLine);
    			validationIndices = new int[tokenizer.countTokens()];
    			for (int j = 0; j < validationIndices.length; j++) {
    				validationIndices[j] = Integer.parseInt(tokenizer.nextToken());
    			}    			
    		}
//    		}
    		br.close();
    	}catch(IOException e){
			Status.basic("Problem importing initial training and validation indices file!");
			e.printStackTrace();

    	}
    }
    
	m_NetsIndicesTraining = trainingIndices;
	m_NetsIndicesValidation = validationIndices;
	
	if(!PotentialPopulation.unlock(PotentialPopulation.m_GlobalDirectory+"/global.lock")) {
		Status.basic("Couldn't unlock when importing training and validation indices! From processor "+PotentialPopulation.m_ProcessorNum);
		new Exception().printStackTrace();
	}
}

	
	/**
	 * @throws IOException
	 */
	private static void readInputFile() throws IOException {
		
		Map<String, ArrayList<String>> map = new HashMap <String, ArrayList<String>>();

		BufferedReader br = new BufferedReader(new FileReader(m_InputFile_Directory+"/"+"inputPM.txt"));
		
		//Import the file into a map
		String line;
		while((line = br.readLine()) != null){
			String[] split = line.split("\\s+");
			if(split[0].contains("#") || line.isEmpty()) {
				continue;
			}
			ArrayList<String> list = new ArrayList<String>();
			for (int i = 1; i < split.length; i++) {
				if(split[i].contains("#")) {
					break;
				}
				list.add(split[i]);
			}
			if(list.size() > 0) {
				map.put(split[0], list);
			}
		}
		br.close();
		
	    //Directory that contains the inputPM.txt file. POET will write the output to this directory.
	    PotentialPopulation.m_GlobalDirectory = map.get("global_directory").get(0);

		PotentialPopulation.m_NumProcessors = map.get("num_instances") != null ? Integer.parseInt(map.get("num_instances").get(0)) : 2;
	    PotentialPopulation.m_ProcessorNum = Integer.parseInt(map.get("processorNum").get(0));

		//The path of the data. The forces must be in a file called F.data, the energies in a file called E0.data, and the structures must be in POSCAR format with extension .vasp
		m_Data_Directory = map.get("data_directory").get(0);
		
	    // Target values for the 0K properties (e.g., DFT, reference_number). See the file poet_run/data/DFT_Cu/0K/TargetProperties_0K.data for an example file
	    m_0K_reference = map.get("0K_reference") != null ? map.get("0K_reference").get(0) : null;
	    
	    // cutoff distance for the atomic neighborhood
        m_CutoffDistance = map.get("cutoffDistance") != null ? Double.parseDouble(map.get("cutoffDistance").get(0)) : 5.5;
        
        double rin = map.get("rIn") != null ? Double.parseDouble(map.get("rIn").get(0)) : 1.0;
        SiteSumNode.setR(rin, m_CutoffDistance);
        
	    // the frequency of changing the training sub-set with respect to the number of mutations and crossovers
	    PotentialPopulation.m_DynamicFrequency = map.get("dynamicFrequency") != null ? Integer.parseInt(map.get("dynamicFrequency").get(0)) : 20000;
	    
	    //Indicate the number of valid trees created during the initialization per depth (depths from 2 to 6, with ramped half and half method)
	    PotentialPopulation.m_numValidPerMethodPerDepth = map.get("numValidPerMethodPerDepth") != null ? Integer.parseInt(map.get("numValidPerMethodPerDepth").get(0)) : 6;

	    ArrayList<String> datanames = new ArrayList<String>();
	    for (String string : map.get("datanames")) {
			datanames.add(string);
		}
	    // the phases
    	m_DataNames = new String[datanames.size() > 0 ? datanames.size() : 0];
    	
    	//Fraction of the number of structures in datanames used for training, the rest is used for validation
    	m_FractionsTraining = new double[map.get("trainingFractions") != null ?  map.get("trainingFractions").size() : 0];
    	
    	m_TrainingSizes = new int[m_DataNames.length];
    	if(m_DataNames.length != m_FractionsTraining.length) {
    		Status.basic("Number of datanames and number of trainingFractions must be equal!");
    	}
        m_TrainingSize = 0;
    	for (int i = 0; i < m_DataNames.length; i++) {
			m_DataNames[i] = datanames.get(i);
    		int num = Integer.parseInt(map.get("numsDataName").get(i));
    		
    		//number of training values (or equivalently, structures) per dataname
    		m_NumsStructuresPerDataName.put(m_DataNames[i], num);
    		m_NumStructures += num;
			m_FractionsTraining[i] = Double.parseDouble(map.get("trainingFractions").get(i));
			m_TrainingSize += (int)(m_FractionsTraining[i]*num);
			m_TrainingSizes[i] = (int)(m_FractionsTraining[i]*num);
		}
    	
        //the size of the mini-batch
        m_MiniBatchSize = map.get("m_MiniBatchSize") != null ? Integer.parseInt(map.get("m_MiniBatchSize").get(0)) : m_TrainingSize;

    	//Check which datanames must include all the structures
    	m_indices_training_must_include = new ArrayList<Integer>();
    	for (int i = 0; i < m_FractionsTraining.length; i++) {
			if((int)(m_FractionsTraining[i]*m_NumsStructuresPerDataName.get(m_DataNames[i])) == m_NumsStructuresPerDataName.get(m_DataNames[i])) {
				int indexStart = 0;
				for (int j = 0; j < i; j++) {
					indexStart += m_NumsStructuresPerDataName.get(m_DataNames[j]);
				}
				for (int j = indexStart; j < indexStart + m_NumsStructuresPerDataName.get(m_DataNames[i]); j++) {
					m_indices_training_must_include.add(j);
				}
			}
		}
    	
    	TreePotential.m_ScaleDistances = map.get("scaleDistances") != null ? Boolean.parseBoolean(map.get("scaleDistances").get(0)) : false;

    	m_Properties = new String[ map.get("properties") != null ?  map.get("properties").size() : 0];
    	for (int i = 0; i < m_Properties.length; i++) {
			m_Properties[i] = map.get("properties").get(i);
		}
    	m_WeightsProperties = new double[ map.get("weightsProperties") != null ?  map.get("weightsProperties").size() : 0];
    	for (int i = 0; i < m_WeightsProperties.length; i++) {
    		m_WeightsProperties[i] = Double.parseDouble(map.get("weightsProperties").get(i));
		}


    	
    	
    //  For the GA engine
    	m_MaxOperations = map.get("gA_maxOperations") != null ? Integer.parseInt(map.get("gA_maxOperations").get(0)) : Integer.MAX_VALUE;
    	
	    PotentialPopulation.m_MaxOperations = m_MaxOperations;
	    
    	m_RatioCrossover = map.get("gA_ratioCrossover") != null ? Double.parseDouble(map.get("gA_ratioCrossover").get(0)) : 0.9;
    	
    	m_PopulationType = map.get("populationType") != null ? map.get("populationType").get(0) : "List";
    	
    	m_SubPopulationType = map.get("subPopulationType") != null ? map.get("subPopulationType").get(0) : "ConvexHull";
    	 
    	m_SelectionMethod = map.get("selectionMethod") != null ? map.get("selectionMethod").get(0) : "Tournament";

    	PotentialEvaluator.m_MeanDepthRandomTree = map.get("meanDepthRandomTree") != null ? Integer.parseInt(map.get("meanDepthRandomTree").get(0)) : 5;
		PotentialEvaluator.m_MinDepthRandomTree = PotentialEvaluator.m_MeanDepthRandomTree - 3 < 1 ? 1 : PotentialEvaluator.m_MeanDepthRandomTree - 3;
		PotentialEvaluator.m_MaxDepthRandomTree = PotentialEvaluator.m_MeanDepthRandomTree + 3;
    	PotentialPopulation.m_Tournament_Size = map.get("tournamentSize") != null ? Integer.parseInt(map.get("tournamentSize").get(0)) : 10;
    	PotentialEvaluator.m_Convergence = map.get("convergenceThreshold") != null ? Double.parseDouble(map.get("convergenceThreshold").get(0)) : 1E-11;        
    	PotentialEvaluator.m_Force_Analytical = map.get("ForcesAnalytical") != null ? Boolean.parseBoolean(map.get("ForcesAnalytical").get(0)) : false;        
  }
  
  private static void createInitialPopulation() {
	  
	  ArrayList<IIndividual> initialIndivs = new ArrayList<IIndividual>();
	  ArrayList<IIndividual> individualsFromOtherProcessors = new ArrayList<IIndividual>();
	  if(m_PopulationType.contains("ParetoFrontiers") || m_PopulationType.equals("List")){
		  PotentialEvaluator evaluator = null;
		  do {
				int depth;
				do {
					depth = (int)Math.round(PotentialEvaluator.m_MeanDepthRandomTree + GENERATOR.nextGaussian());
				}while(depth < 1);
				char c;
				if(GENERATOR.nextBoolean()) {
					c = 'f';
				}
				else {
					c = 'g';
				}
			  TreePotential simplifiedTree = TreePotential.generateTree(m_CutoffDistance, depth, c).simplify();
			  evaluator = new PotentialEvaluator(simplifiedTree, m_DataSetTraining);
		  }while(!evaluator.isValid());
		  initialIndivs.add(evaluator);
		  individualsFromOtherProcessors.add(new PotentialEvaluator(new TreePotential(evaluator.getPotential().getTopNode().deepCopy(), evaluator.getPotential().getCutoffDistance()), m_DataSetTraining));
	  }	  
	  PotentialSubPopulation initialPop = new PotentialSubPopulation(initialIndivs, 0, m_PopulationType);
	  PotentialEvaluator eval = (PotentialEvaluator) initialPop.getIndividuals().get(0);
	  ArrayList<IIndividual> gTMSE = new ArrayList<IIndividual>();
	  gTMSE.add(new PotentialEvaluator(eval.getPotential(), m_DataSetTraining));
	  ArrayList<IIndividual> gTLC = new ArrayList<IIndividual>();
	  gTLC.add(new PotentialEvaluator(eval.getPotential(), m_DataSetTraining));	  
	  
	  PotentialSubPopulation[][] pspsArrays = new PotentialSubPopulation[2][PotentialEvaluator.m_MaxLoops];
	  String[] types = new String[] {"globalTrainingFrontierMSE","globalTrainingFrontierLC"};
	  for (int i = 0; i < types.length; i++) {
		  int c = 0;
		  while(c < PotentialEvaluator.m_MaxLoops) {
			  PotentialEvaluator pe;
			  do {
				  pe = new PotentialEvaluator(new TreePotential(getLinearCombSiteSums(c+1), m_CutoffDistance), m_DataSetTraining);
			  }while(pe.getPotential().numSiteSums() != c+1 || !pe.isValid());
			  ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
			  individuals.add(pe);
			  pspsArrays[i][c] = new PotentialSubPopulation(individuals, 0, m_SubPopulationType);
			  c++;
		  }
	  }
	  
	  m_InitialPopulation = new PotentialPopulation(initialPop, m_SelectionMethod, pspsArrays[0],pspsArrays[1], m_DataSetTraining, individualsFromOtherProcessors);
	  
//	  m_InitialPopulation = new PotentialPopulation(initialPop, m_SelectionMethod, globalTrainingFrontierMSE,globalTrainingFrontierLC, m_DataSetTraining, individualsFromOtherProcessors);
	  m_InitialPopulation.createInitialPopulation(PotentialPopulation.m_numValidPerMethodPerDepth);
	
  }
  
  

private static void veify(String species, String model) throws IOException, NotFiniteValueException{

	//Need this to use dynamic indices
	m_DataSetTraining.setSubset("All");
	
	TreePotential tree0 = null;
	boolean sc = species.equals("Ni")||species.equals("Cu")||species.equals("Au");
	if(sc) {
		Status.basic("Sutton-Chen EAM "+species);
		tree0 = TreePotentials.generateSC(m_CutoffDistance, species, true);
	}
	else if(species.equals("He")) {
		Status.basic("LJ He");
		double kB = 8.6173303E-5; //Boltzmann constant eV/K
		double sigma = 2.551;  //Angstroms
		double epsilon = 10.22*kB; // eV
		double powerRepulsive = 12.0;
		double powerAttractive = 6.0;
		String eqn = "((((133.7762859602084)*(((r)^(-12.0)))))-(((0.4854185658088797)*(((r)^(-6.0))))))";
		tree0 = TreePotentials.generateLJ(m_CutoffDistance, species);
		tree0 = TreePotential.buildTree(eqn, m_CutoffDistance);
	}
	  //
	else if(species.equals("Ar")) {
		Status.basic("LJ Ar");
		double kB = 8.6173303E-5; //Boltzmann constant eV/K
		double sigma = 3.35;  //Angstroms
		double epsilon = 143.2*kB; // eV
		double powerRepulsive = 12.0;
		double powerAttractive = 6.0;
		
		tree0 = TreePotentials.generateLJ(m_CutoffDistance, species);
	}
	  
	PotentialEvaluator individual = new PotentialEvaluator(tree0, m_DataSetTraining);
	Status.basic(individual.getFitness(null, true)+" "+individual.getFitness(null, false)+" "+individual.getComplexity()+" "+individual.numNodes()+" "+individual.getExpression());
	  

	m_DataSetTraining.setSubset("Local");
}

  private static void runGAEnginePareto() throws IOException{
	  GAEngine engine = new GAEngine(m_RatioCrossover, m_MaxOperations, m_InitialPopulation);
	  engine.run();
  }

}