package evaluator;
/*
 * Created on Sep 4, 2016
 *
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.apache.commons.math4.analysis.MultivariateFunction;
import org.apache.commons.math4.analysis.MultivariateVectorFunction;
import org.apache.commons.math4.optim.ConvergenceChecker;
import org.apache.commons.math4.optim.InitialGuess;
import org.apache.commons.math4.optim.MaxEval;
import org.apache.commons.math4.optim.MaxIter;
import org.apache.commons.math4.optim.OptimizationData;
import org.apache.commons.math4.optim.PointValuePair;
import org.apache.commons.math4.optim.SimpleBounds;
import org.apache.commons.math4.optim.SimplePointChecker;
import org.apache.commons.math4.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math4.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math4.optim.nonlinear.scalar.ObjectiveFunctionGradient;
import org.apache.commons.math4.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer;
import org.apache.commons.math4.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math4.exception.*;
import org.apache.commons.math4.fitting.PolynomialCurveFitter;
import org.apache.commons.math4.fitting.WeightedObservedPoints;
import org.apache.commons.rng.UniformRandomProvider;
import evaluator.DataSet.SubSetElement;
import gAEngine.IIndividual;
import matsci.engine.IContinuousFunctionState;
import matsci.engine.minimizer.CGMinimizer;
import matsci.io.app.log.Status;
import matsci.location.Vector;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.DivisionNode;
import potentialModelsGP.tree.MultiplicationNode;
import potentialModelsGP.tree.Node;
import potentialModelsGP.tree.PowerNode;
import potentialModelsGP.tree.SubtractionNode;
import potentialModelsGP.tree.TreePotential;
import util.Statistics;
import util.UniformRandomProviderImplementation;

public class PotentialEvaluator implements IIndividual, IContinuousFunctionState, MultivariateFunction {
  private static final Random GENERATOR = new Random();
  
  //Both Inclusive, maxDepth also determines the max allowed number of distances and number of node
  protected static int m_MinDepthRandomTree = 4;
  protected static int m_MaxDepthRandomTree = 6;
  protected static int m_MeanDepthRandomTree = 6;
  
  protected static int m_MaxLoops = 6;

  protected static int m_MaxSiteSumsUnderSiteSum = 0;
  
  protected static CGMinimizer m_Minimizer;// CGMinimizer or GRLinearMinimizer
  
  protected static double m_Convergence;

  private Double m_FitnessTrainingLC = null;
  private Double m_FitnessTrainingMSE = null;
  private int m_NumTimesSelected;
  private double m_CumulativeSelectivityScore;
  
  private TreePotential m_Potential;

  protected ConstantNode[] m_MinimizeOnlyTheseNodes;
  
  private double m_MinValueMinimizer = Double.POSITIVE_INFINITY;
  private double[] m_MinParametersMinimizer;
    
  private String m_NodeTypes = null;
  private String m_Expression = null;
  
  private DataSet m_DataSet;
  
  private Double[][] m_Pred_Energies;
  private Double[][] m_Pred_Forces;
  private Double[][] m_Pred_Stresses;
  
  //This map contains an array of a target property for every structure in the dataset
  //This approach gives flexibility when choosing which properties to use for training
  private HashMap<String, Double[][]> m_DataPredicted;
  
  private AppliedTreePotential[] m_AppliedPotentials;
  
  protected static boolean m_Force_Analytical = true;
  
  private String m_MultivariateFunction_value = "Fitness"; //Fitness xor Energy xor Force xor Stress
  
  public PotentialEvaluator(TreePotential potential, DataSet dataSet) {
	  	  
	  m_Potential = potential;
	  m_NumTimesSelected = 0;

	  m_DataSet = dataSet;
	  
	  m_AppliedPotentials = new AppliedTreePotential[this.getDataSet().getNets().length];
	  for (int i = 0; i < m_AppliedPotentials.length; i++) {
		  m_AppliedPotentials[i] = new AppliedTreePotential(m_Potential, this.getDataSet().getNets()[i]);
	  }
	  this.intitiallizeDataPredicted();
  }
  
  private void intitiallizeDataPredicted() {
	  m_DataPredicted = new HashMap<String, Double[][]>();
	  String[] properties = this.getDataSet().getTargetProperties();
	  for (String property : properties) {
			double[][] target = this.getDataSet().getDataMap().get(property);
			Double[][] array = new Double[target.length][];
			for (int i = 0; i < array.length; i++) {
				array[i] = new Double[target[i].length];
			}
			if(property.equals("Energy")) {
				m_Pred_Energies = array;
			}
			else if(property.equals("Force")) {
				m_Pred_Forces = array;
			}
			else if(property.equals("Stress")) {
				m_Pred_Stresses = array;
			}
			m_DataPredicted.put(property, array);
	  }
  }
  
  @Override
  public int numParameters() {
	  return m_MinimizeOnlyTheseNodes.length;
  }
  
//  Constant nodes (for the optimizer)
  @Override
  public double[] getUnboundedParameters(double[] template) {
    if (template == null) {
      template = new double[numParameters()];
    }
    
    for (int nodeNum = 0; nodeNum < template.length; nodeNum++) {
      template[nodeNum] = m_MinimizeOnlyTheseNodes[nodeNum].getValue();
    }
    return template;
  }  
  
///**	 Total energies per atom.
// * 	Usually, during the data pre processing, the standard deviation (SD) of the known energies per atom is computed.
// *  In that case, these are the total energies of each structure per atom, normalized by that SD
//**/
//public double[] getEnergiesPredictedMD(Boolean optimizer) {
//	int[] indices;
//	if(optimizer == null) {
//		indices = new int[m_AppliedPotentialsMD.length];
//		for (int i = 0; i < indices.length; i++) {indices[i] = i;}
//	}
//	else if(optimizer) {indices = this.m_OptimizerIndicesEnergiesMD;}
//	//Get the training values indicated in indices
//	else {
//		if(TrainingData.getDynamicIndicesEnergiesMD()==null) {indices = TrainingData.getIndicesGlobalEnergiesMD();}
//		else{indices = TrainingData.getDynamicIndicesEnergiesMD();}
//
//	}
// 	 		
//	return this.getEnergiesPredictedMDIndices(indices);
//}

//public double[] getEnergiesPredictedMDIndices(int[] indices) { 	 		
//    double[] predictedFew = new double[indices.length];
//    for (int i : indices) {
//    	if(m_PredictedEnergiesDynamicMD[i] == null){
//		      try{m_PredictedEnergiesDynamicMD[i] = m_AppliedPotentialsMD[i].getEnergy();
//		      }catch(NotFiniteValueException e){
//		    	  Arrays.fill(predictedFew, Double.NaN);
//		    	  return predictedFew;
//		      }
//    	}
//    }
//    
//    int c = 0;
//    for (int i : indices) {
//    	//Energies per atom, and also consider that the energies were re-scaled by their standard deviation
//    	double denominator = TrainingData.getNets("MD")[i].numDefiningSites();
//    	predictedFew[c] = m_PredictedEnergiesDynamicMD[i]/denominator; 
//    	c++;
//	}
//    return predictedFew;
//}

/**	 Normalized total energies per atom.
**/
public double[] getEnergiesPredicted(StructureNet[] nets) {
	AppliedTreePotential[] atps = new AppliedTreePotential[nets.length];
	for (int i = 0; i < atps.length; i++) {
		atps[i] = new AppliedTreePotential(this.getPotential(), nets[i]);
	}
 	
    double[] predicted = new double[atps.length];
    
    for (int i = 0; i < predicted.length; i++) {
        try{predicted[i] = atps[i].getEnergy();
        }catch(NotFiniteValueException e){
      	  Arrays.fill(predicted, Double.NaN);
      	  return predicted;
        }
	}
    
    int c = 0;
    for (int i = 0; i < predicted.length; i++) {
    	double denominator = nets[i].numDefiningSites();
    	predicted[c] = predicted[i]/denominator; 
    	c++;
	}
    return predicted;
}

///**	 Total energies per atom.
// * 	Usually, during the data pre processing, the standard deviation (SD) of the known energies per atom is computed.
// *  In that case, these are the total energies of each structure per atom, normalized by that SD
//**/
//public double[] getEnergiesPredictedElasticConstant(String elasticConstant) {
//	
//	Double[] array = null;
//	AppliedTreePotential[] atps = null;
//	if(elasticConstant.equals("BulkModulus")) {
//		if(m_PredictedEnergiesBulkModulus!=null) {
//			array = m_PredictedEnergiesBulkModulus;
//		}
//		atps = m_AppliedPotentialsBulkModulus;
//	}
//	else if(elasticConstant.equals("C11")) {
//		if(m_PredictedEnergiesC11!=null) {
//			array = m_PredictedEnergiesC11;
//		}
//		atps = m_AppliedPotentialsC11;
//	}
//	else if(elasticConstant.equals("C44")) {
//		if(m_PredictedEnergiesC44!=null) {
//			array = m_PredictedEnergiesC44;
//		}
//		atps = m_AppliedPotentialsC44;
//	}
//	
//	double[] returnArray = new double[5];
//	if(array!=null) {
//		int c = 0;
//		for (double d : array) {returnArray[c] = d;c++;}
//		return returnArray;
//	}
//	
//	for (int i = 0; i < atps.length; i++) {
//	      try{returnArray[i] = atps[i].getEnergy();
//	      }catch(NotFiniteValueException e){
//	    	  Arrays.fill(returnArray, Double.NaN);
//	    	  return returnArray;
//	      }
//    }
//    
//    int c = 0;
//    for (int i = 0; i < atps.length; i++) {
//    	double denominator = atps[i].getStructureNet().numDefiningSites();
//    	returnArray[c] = returnArray[i]/denominator; 
//    	c++;
//	}
//    
//    Double[] energies = new Double[5];
//    for (int i = 0; i < energies.length; i++) {
//		energies[i] = returnArray[i];
//	}
//	if(elasticConstant.equals("BulkModulus")) {
//		m_PredictedEnergiesBulkModulus = energies;
//	}
//	else if(elasticConstant.equals("C11")) {
//		m_PredictedEnergiesC11 = energies;
//	}
//	else if(elasticConstant.equals("C44")) {
//		m_PredictedEnergiesC44 = energies;
//	}
//	
//    return returnArray;
//}

@Override
public double[] getGradient(double[] template) {	
	if(this.getGradientOptimizationObjective().equals("Energy")) {
		return this.getGradientMSEEnergy(template,true);
	}
	else if(this.getGradientOptimizationObjective().equals("Force")) {
		return this.getGradientMSEForce(template);
	}
	else if(this.getGradientOptimizationObjective().equals("Stress")) {
		return this.getGradientMSEForce(template);
	}
	else if(this.getGradientOptimizationObjective().equals("Fitness")) {
		double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
		Arrays.fill(returnArray, 0); // In case we received a template
		
		String[] properties = this.getDataSet().getTargetProperties();
		List<String> propertiesList = Arrays.asList(properties);
		String[] efs = new String[] {"Energy","Force","Stress"};
		for (int i = 0; i < efs.length; i++) {
			if(propertiesList.contains(efs[i])) {
				double w = this.getDataSet().getWeight(efs[i]);
				if(efs[i].equals("Energy")) {
					returnArray = MSMath.arrayAdd(returnArray,MSMath.arrayMultiply(this.getGradientMSEEnergy(template, true), w));
				}
				else if(efs[i].equals("Force")) {
					returnArray = MSMath.arrayAdd(returnArray,MSMath.arrayMultiply(this.getGradientMSEForce(template), w));
				}
				else if(efs[i].equals("Stress")) {
					returnArray = MSMath.arrayAdd(returnArray,MSMath.arrayMultiply(this.getGradientMSEStress(template), w));
				}
			}
		}
		return returnArray;
	}
	else {
		double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
    	Arrays.fill(returnArray, Double.NaN);
		return returnArray;
	}
}

///**
// * 
// * @param template
// * @return gradient
// */
//protected double[] getGradientMSE(double[] template) {
//	//Negative of the gradient of the fitness function with respect to each unbounded parameter
//	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
//	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
//	
//	double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
//	Arrays.fill(returnArray, 0); // In case we received a template
//	
//
//	int[] indices = this.getDataSet().getIndicesEnergies("Local");
//	
//	double[] known0 = this.getDataSet().getTargetProperty("Energy");
//	double[] predicted0 = this.getPredictedProperty("Energy");
//	
//	int c = 0;
//	for (ConstantNode node : this.m_MinimizeOnlyTheseNodes) {
//		for (int i = 0; i < indices.length; i++) {
//			double dedc = m_AppliedPotentials[indices[i]].getGradientEnergyPerAtom(node);
//			double diff = (known0[i]-predicted0[i]);
//			returnArray[c] += -2.0*diff*dedc;
//		}
//		c++;
//	}
//	returnArray = MSMath.arrayDivide(returnArray, indices.length);
//	return returnArray; 
//}

/**
 * MSE uses minimum of predicted and target, or average of target. 
 * If min=false, then the MSE uses the average of the target values
 * @param 
 * @return gradient
 */
private double[] getGradientMSEEnergy(double[] template, boolean min) {
	//Negative of the gradient of the fitness function with respect to each unbounded parameter
	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
	
	double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
	Arrays.fill(returnArray, 0); // In case we received a template
	
	int[] indices = this.getDataSet().getIndicesEnergies(this.getDataSet().getSubSet());
	AppliedTreePotential[] atps = m_AppliedPotentials;
	
	double[] known0;
	double[] predicted0;
	if(this.getDataSet().getSubSet().equals("Local")) {
		known0 = this.getDataSet().getEnergy_Local();
		predicted0 = this.getPredictedProperty(this.getDataSet().getLocalIndicesEnergy(), m_Pred_Energies, 0, null);
	}
	else {
		known0 = this.getDataSet().getTargetProperty("Energy");
		predicted0 = this.getPredictedProperty("Energy");
	}
	
	double stdKnown = Statistics.getStandardDeviation(known0, true);
	
	double[] gradientMinEnergy = null;
	if(min) {
		double minKnown = Statistics.getMinValue(known0);
		double minPredicted = Statistics.getMinValue(predicted0);
		
		int indexMinEnergy = 0;
		int index = 0;
		for (double d : predicted0) {
			if(Statistics.compare(minPredicted, d) == 0) {
				indexMinEnergy = index;
				break;
			}
			index++;
		}
		
		for (int i = 0; i < known0.length; i++) {known0[i] = (known0[i]-minKnown);}
		for (int i = 0; i < predicted0.length; i++) {predicted0[i] = (predicted0[i]-minPredicted);}
		
		gradientMinEnergy = atps[indices[indexMinEnergy]].getGradientsEnergyPerAtom(this.m_MinimizeOnlyTheseNodes);
	}
	
	
	double multiplier = -2.0/(indices.length*stdKnown*stdKnown);
	int c = 0;
	for (ConstantNode node : this.m_MinimizeOnlyTheseNodes) {
		for (int i = 0; i < indices.length; i++) {
			double dedc = atps[indices[i]].getGradientEnergyPerAtom(node);
			double diff = (known0[i]-predicted0[i]);
			if(min) {
				returnArray[c] += multiplier*diff*(dedc-gradientMinEnergy[c]);
			}
			else {
				returnArray[c] += multiplier*diff*(dedc);
			}
		}
		c++;
	}
	return returnArray;
}

private double[] getGradientMSEForce(double[] template) {
	//Negative of the gradient of the fitness function with respect to each unbounded parameter
	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
	
	double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
	Arrays.fill(returnArray, 0); // In case we received a template
	
	double[] known0 = this.getDataSet().getTargetProperty("Force");
	double[] predicted0 = this.getPredictedProperty("Force");
	
	if(predicted0 == null) {
    	Arrays.fill(returnArray, Double.NaN);
		return returnArray;
	}
	double stdKnown = Statistics.getStandardDeviation(known0, true);
	
	SubSetElement[] subSetElements = this.getDataSet().getIndices(this.getDataSet().getSubSet(), "Force");
	int numPoints = 0;
	for (int i = 0; i < subSetElements.length; i++) {
		numPoints += subSetElements[i].getIndices().length;
	}
	double multiplier = -2.0/(numPoints*stdKnown*stdKnown);
	double h = 1E-6;
	
	int c = 0;
	for (ConstantNode node : this.m_MinimizeOnlyTheseNodes) {
		int forceIndex = 0;
		for (int i = 0; i < subSetElements.length; i++) {
			double f0;
			double f1;
			try {
				int netIndex = subSetElements[i].getNetIndex();
				int[] indicesSubElements = subSetElements[i].getIndices();
				for (int j = 0; j < indicesSubElements.length; j++) {
					f0 = m_AppliedPotentials[netIndex].getForceAnalytical(indicesSubElements[j]);				
					double value0 = node.getValue();
					node.setValue(value0+h);
					this.clearValues(true);
					f1 = m_AppliedPotentials[netIndex].getForceAnalytical(indicesSubElements[j]);
					node.setValue(value0);
					this.clearValues(true);
					double df = f1-f0;
					double dedf = df/h;			
					double diff = (known0[forceIndex]-predicted0[forceIndex]);
					forceIndex++;
					returnArray[c] += multiplier*diff*(dedf); //-2.0*diff*dedf/denominator;
				}				
			} catch (NotFiniteValueException e) {
		    	Arrays.fill(returnArray, Double.NaN);
				return returnArray;
			}
		}
		c++;
	}
	return returnArray; 
}

private double[] getGradientMSEStress(double[] template) {
	//Negative of the gradient of the fitness function with respect to each unbounded parameter
	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
	
	double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
	Arrays.fill(returnArray, 0); // In case we received a template
	
	double[] known0 = this.getDataSet().getTargetProperty("Stress");
	double[] predicted0 = this.getPredictedProperty("Stress");
	
	if(predicted0 == null) {
    	Arrays.fill(returnArray, Double.NaN);
		return returnArray;
	}
	double stdKnown = Statistics.getStandardDeviation(known0, true);
	
	SubSetElement[] subSetElements = this.getDataSet().getIndices(this.getDataSet().getSubSet(), "Stress");
	int numPoints = 0;
	for (int i = 0; i < subSetElements.length; i++) {
		numPoints += subSetElements[i].getIndices().length;
	}
	double multiplier = -2.0/(numPoints*stdKnown*stdKnown);
	double h = 1E-6;
	
	int c = 0;
	for (ConstantNode node : this.m_MinimizeOnlyTheseNodes) {
		int stressIndex = 0;
		for (int i = 0; i < subSetElements.length; i++) {
			double f0;
			double f1;
			try {
				int netIndex = subSetElements[i].getNetIndex();
				int[] indicesSubElements = subSetElements[i].getIndices();
				for (int j = 0; j < indicesSubElements.length; j++) {
					f0 = m_AppliedPotentials[netIndex].getStressComponent(indicesSubElements[j]); //.getForceAnalytical(indicesSubElements[j]);				
					double value0 = node.getValue();
					node.setValue(value0+h);
					this.clearValues(true);
					f1 = m_AppliedPotentials[netIndex].getStressComponent(indicesSubElements[j]);//.getForceAnalytical(indicesSubElements[j]);
					node.setValue(value0);
					this.clearValues(true);
					double ds = f1-f0;
					double deds = ds/h;			
					double diff = (known0[stressIndex]-predicted0[stressIndex]);
					stressIndex++;
					returnArray[c] += multiplier*diff*(deds);
				}				
			} catch (NotFiniteValueException e) {
		    	Arrays.fill(returnArray, Double.NaN);
				return returnArray;
			}
		}
		c++;
	}
	return returnArray; 
}

//protected double[] getGradientPCC(double[] template) {
//	//Negative of the gradient of the fitness function with respect to each unbounded parameter
//	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
//	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
//	
//	double[] returnArray = (template == null) ? new double[this.m_MinimizeOnlyTheseNodes.length] : template;
//	Arrays.fill(returnArray, 0); // In case we received a template
//	
//	int[] indices = this.getDataSet().getIndicesEnergies(this.getDataSet().getSubSet());
//	
//	double[] known0 = this.getDataSet().getTargetProperty("Energy");
//	double[] predicted0 = this.getPredictedProperty("Energy");
//	
//	double avgKnown = Statistics.getMean(known0);
//	double avgPredicted = Statistics.getMean(predicted0);
//	double stdKnown = Statistics.getStandardDeviation(known0, true);
//	double stdPredicted = Statistics.getStandardDeviation(predicted0, true);
//	double varPredicted = Statistics.getVariance(predicted0, true);
//	double cov = Statistics.getCovariance(known0, predicted0, true);
//
//	int num = indices.length;
//	
//	int c = 0;
//	for (ConstantNode node : this.m_MinimizeOnlyTheseNodes) {
//		//derivatives of predicted energies of each structure with respect to this constant
//		double[] de = new double[num];
//		//derivative of average predicted energy with respect to this constant
//		double deAvg = 0.0;
//		for (int i = 0; i < num; i++) {
//			double dedcIndex = m_AppliedPotentials[indices[i]].getGradientEnergyPerAtom(node);
//			de[i] = dedcIndex;
//			deAvg += dedcIndex;
//		}
//		deAvg /= num;
//		
//		//derivative of covariance of known and predicted with respect to this constant
//		double dcov = 0.0;
//		//derivative of variance of predicted with respect to this constant
//		double dvar = 0.0;
//		for (int i = 0; i < num; i++) {
//			double diffderiv = de[i] - deAvg;
//			double diffKnown = known0[i] - avgKnown;
//			double diffPredicted = predicted0[i] - avgPredicted;
//			
//			dcov += diffKnown*diffderiv;
//			dvar += 2*diffPredicted*diffderiv;
//			
//		}
//		dvar /= num;
//		dcov /= num;
//		
//		//derivative of (standard deviation of predicted) with respect to this constant
//		double dstdPred = 0.5*(Math.pow(varPredicted, -0.5))*dvar;
//		
//		//derivative of ((standard deviation of known)*(standard deviation of predicted)) with respect to this constant
//		double dstd = stdKnown*dstdPred;
//		
//		//derivative of pcc = covarianceKnownPredicted/(stdKnown*stdPredicted) with respect to this constant
//		double numerator1 = stdKnown*stdPredicted*dcov;
//		double numerator2 = cov*dstd;
//		double denominator = (stdKnown*stdPredicted)*(stdKnown*stdPredicted);
//		double dpcc = (numerator1-numerator2)/denominator;
//		
//		returnArray[c] = -dpcc;
//		
//		c++;
//	}
//
//	return returnArray; 
//}

private double[] getGradientFitness(double[] template) {	
	//Negative of the gradient of the fitness function with respect to each unbounded parameter
	//Use the fact that dg(f1, f2, f3...)/dxi = sum(j)(dg/dfj)(dfj/dxi)
	//Where g is the fitness function, f are the arguments of g, and xi is an unbounded parameter
	
	double[] returnArray = (template == null) ? new double[m_MinimizeOnlyTheseNodes.length] : template;
	Arrays.fill(returnArray, 0); // In case we received a template
	
//	Forward difference approximation
//	source: https://www.mathworks.com/help/matlab/ref/gradient.html#bvhqkfr

	double fitness0 = this.getFitness(null, false);
	
	for (int i = 0; i < m_MinimizeOnlyTheseNodes.length; i++) {
		double h = 1E-8;
		double value0 = m_MinimizeOnlyTheseNodes[i].getValue();
				
		m_MinimizeOnlyTheseNodes[i].setValue(value0+h);
		this.clearValues(true);
		
		double fitness1 = this.getFitness(null, false);
		m_MinimizeOnlyTheseNodes[i].setValue(value0);
		this.clearValues(true);
		
		returnArray[i] = (fitness1-fitness0)/h;
	}
	
	return returnArray;
}

//  Set values of constants; for the optimizer
@Override
public IContinuousFunctionState setUnboundedParameters(double[] parameters) {
	for (int i = 0; i < parameters.length; i++) {
		m_MinimizeOnlyTheseNodes[i].setValue(parameters[i]);
	}
	this.clearValues(true);
	return this;	  
}

@Override
public double getValue(){	
	double quantityToBeMinimized = this.getValueMinimizer();
	//The minimizer sometimes does not return the best parameters that it found
	if(quantityToBeMinimized < m_MinValueMinimizer) {
		m_MinValueMinimizer = quantityToBeMinimized;
		m_MinParametersMinimizer = this.getUnboundedParameters(null);
	}
	return quantityToBeMinimized;
}

protected double getValueMinimizer(){	
	
	if(this.getGradientOptimizationObjective().equals("Energy")) {
		double[] knownEMD;
		double[] predictedEMD;
		if(this.getDataSet().getSubSet().equals("Local")) {
			knownEMD = this.getDataSet().getEnergy_Local();
			predictedEMD = this.getPredictedProperty(this.getDataSet().getLocalIndicesEnergy(), m_Pred_Energies, 0, null);
		}
		else {
			knownEMD = this.getDataSet().getTargetProperty("Energy");
			predictedEMD = this.getPredictedProperty("Energy");
		}
		return this.getFitness(knownEMD, predictedEMD, false, true);
	}
	else if(this.getGradientOptimizationObjective().equals("Force")) {
		double[] known0 = this.getDataSet().getTargetProperty("Force");
		double[] predicted0 = this.getPredictedProperty("Force");
		return this.getFitness(known0, predicted0, false, false);
	}
	else if(this.getGradientOptimizationObjective().equals("Stress")) {
		double[] known0 = this.getDataSet().getTargetProperty("Stress");
		double[] predicted0 = this.getPredictedProperty("Stress");
		return this.getFitness(known0, predicted0, false, false);
	}
	else if(this.getGradientOptimizationObjective().equals("Fitness")) {
		return this.getFitness(null, false);
	}
	return Double.MAX_VALUE;
}

// The best fitness is 0; a greater fitness is worse
@Override
public double getFitness(Double alpha, Boolean fitnessByLC){		  	  
	  boolean compute = (fitnessByLC && (m_FitnessTrainingLC==null)) || (!fitnessByLC && (m_FitnessTrainingMSE==null));
	  if(compute){
		  double fitness = this.getFitnessDataSet(fitnessByLC);
		  
		  if(fitness > 1E300 || !Double.isFinite(fitness)){
			  fitness = Double.MAX_VALUE;
		  }
		  
		  if(fitnessByLC){m_FitnessTrainingLC = fitness;}
		  else{m_FitnessTrainingMSE = fitness;}
	  }
	  
	  if(!fitnessByLC){
		  if(Statistics.compare(m_FitnessTrainingMSE, m_Convergence) == -1){
			  return 0;
		  }
	  }
//	  else{if(Statistics.compare(m_FitnessTrainingLC, m_Convergence) == -1){return 0;}}

	  if(alpha == null){
		  if(fitnessByLC){return m_FitnessTrainingLC;}
		  else{return m_FitnessTrainingMSE;}
	  }
	  
	  if(fitnessByLC){return m_FitnessTrainingLC + alpha*this.getPotential().numNodes();}
	  else{return m_FitnessTrainingMSE + alpha*this.getPotential().numNodes();}
}

public double getFitnessDataSet(boolean lc) {
	String[] properties = this.getDataSet().getTargetProperties();
	double fitness = 0.0;
	boolean local = this.getDataSet().getSubSet().equals("Local");
	for (String property : properties) {
		boolean energy = false;
		boolean force = false;
		boolean stress = false;
		if(property.equals("Energy")) {
			energy = true;
		}
		else if(property.equals("Force")) {
			force = true;
		}
		else if(property.equals("Stress")) {
			stress = true;
		}
		boolean efs = energy || force || stress;

		if(efs || !local) {
			double[] target = null;
			double[] predicted = null;
			if(local && efs) {
				if(energy) {
					target = this.getDataSet().getEnergy_Local();
					predicted = this.getPredictedProperty(this.getDataSet().getIndices("Local", property), m_Pred_Energies, 0, null);
				}
				else if(force) {
					target = this.getDataSet().getForce_Local();
					predicted = this.getPredictedProperty(this.getDataSet().getIndices("Local", property), m_Pred_Forces, 1, null);
				}
				else if(stress) {
					target = this.getDataSet().getStress_Local();
					predicted = this.getPredictedProperty(this.getDataSet().getIndices("Local", property), m_Pred_Stresses, 2, null);
				}
			}
			else {
				target = this.getDataSet().getTargetProperty(property);
				predicted = this.getPredictedProperty(property);
			}
			double error = 0.0;
			
			if(efs) {
				error = this.getFitness(target, predicted, lc, energy);
			}
			else if(!lc){
				error = Math.abs(Statistics.getPercenErrors(target, predicted)[0]);
			}
			fitness += this.getDataSet().getWeight(property)*error;		
		}
	}
	return fitness;
}

/**
 * 
 * @param subSetElements
 * @param values
 * @param propertyIndex 0=energy, 1=force, 2=stress, null
 * @return property
 */
public double[] getPredictedProperty(SubSetElement[] subSetElements, Double[][] values, Integer propertyIndex, String property) {
	
	int numPoints = 0;
	for (int i = 0; i < subSetElements.length; i++) {
		numPoints += subSetElements[i].getIndices().length;
	}
	double[] returnArray = new double[numPoints];
	int c = 0;
	for (int i = 0; i < subSetElements.length; i++) {
		int netIndex = subSetElements[i].getNetIndex();
		double  numSites = this.getDataSet().getNets()[netIndex].numDefiningSites();
		//Convert to property per atom
		if(propertyIndex!=null) {
			if(propertyIndex == 0) {
				numSites = this.getDataSet().getNets()[netIndex].numDefiningSites();
			}
			else {
				numSites = 1.0;
			}
		}
		//Convert to property per atom
		else if(property!=null) {
			if(property.equals("Energy")) {
				numSites = this.getDataSet().getNets()[netIndex].numDefiningSites();
			}
			else {
				numSites = 1.0;
			}
		}
		int[] indices = subSetElements[i].getIndices();
		for (int j = 0; j < indices.length; j++) {
			Double value = values[netIndex][indices[j]];
			if(value == null) {
				try {
					if(propertyIndex!=null) {
						if(propertyIndex == 0) {
							value = m_AppliedPotentials[netIndex].getEnergy();
						}
						else if(propertyIndex == 1) {
							if(m_Force_Analytical) {
								value = m_AppliedPotentials[netIndex].getForceAnalytical(indices[j]);
							}
							else {
								value = m_AppliedPotentials[netIndex].getForceForwardDifference(indices[j]);
							}
						}
						else if(propertyIndex == 2) {
							value = m_AppliedPotentials[netIndex].getStressComponent(indices[j]);
						}
					}
					else if(property.equals("Energy")) {
						value = m_AppliedPotentials[netIndex].getEnergy();
					}
					else if(property.equals("Force")) {
						if(m_Force_Analytical) {
							value = m_AppliedPotentials[netIndex].getForceAnalytical(indices[j]);
						}
						else {
							value = m_AppliedPotentials[netIndex].getForceForwardDifference(indices[j]);
						}					
					}
					else if(property.equals("Stress")) {
						value = m_AppliedPotentials[netIndex].getStressComponent(indices[j]);
					}
					else {
						value = this.computePredictedProperty(property);
					}
					value /= numSites;
				} catch (NotFiniteValueException e) {
					Arrays.fill(returnArray, Double.NaN);
					return returnArray;
				}
				values[netIndex][indices[j]] = value;
			}
			returnArray[c] = value;
			c++;
		}
	}
	return returnArray;
}

public double[] getPredictedProperty(String property) {

	SubSetElement[] subSetElements = this.getDataSet().getIndices(this.getDataSet().getSubSet(), property);
	Double[][] values = this.getPredictedMap().get(property);
	return this.getPredictedProperty(subSetElements, values, null, property);
	
//	int numPoints = 0;
//	for (int i = 0; i < subSetElements.length; i++) {
//		numPoints += subSetElements[i].getIndices().length;
//	}
//	double[] returnArray = new double[numPoints];
//	int c = 0;
//	for (int i = 0; i < subSetElements.length; i++) {
//		int netIndex = subSetElements[i].getNetIndex();
//		double  numSites = this.getDataSet().getNets()[netIndex].numDefiningSites();;
//		//Convert to energy per atom
//		if(property.equals("Energy")) {
//			numSites = this.getDataSet().getNets()[netIndex].numDefiningSites();
//		}
//		else {
//			numSites = 1.0;
//		}
//		int[] indices = subSetElements[i].getIndices();
//		for (int j = 0; j < indices.length; j++) {
//			Double value = values[netIndex][indices[j]];
//			if(value == null) {
//				try {
//					if(property.equals("Energy")) {
//						value = m_AppliedPotentials[netIndex].getEnergy();
//					}
//					else if(property.equals("Force")) {
//						value = m_AppliedPotentials[netIndex].getForceForwardDifference(indices[j]);
//					}
//					else if(property.equals("Stress")) {
//						value = m_AppliedPotentials[netIndex].getStressComponent(indices[j]);
//					}
//					else {
//						value = this.computePredictedProperty(property);
//					}
//					value /= numSites;
//				} catch (NotFiniteValueException e) {
//					Arrays.fill(returnArray, Double.NaN);
//					return returnArray;
//				}
//				values[netIndex][indices[j]] = value;
//			}
//			returnArray[c] = value;
//			c++;
//		}
//	}
//	return returnArray;
}

public double computePredictedProperty(String property) throws NotFiniteValueException {
//	if(property.equals("Energy")) {
//		return m_AppliedPotentials[structureIndex].getEnergy();
//	}
//	else if(property.endsWith("Force")) {
//		return m_AppliedPotentials[structureIndex].getForceForwardDifference(propertyIndex);
//	}
//	else if(property.equals("Stress")) {
//		return m_AppliedPotentials[structureIndex].getStressComponent(propertyIndex);
//	}
	if(property.contains("delta_E0")) {
		String[] array = property.split("_");
		String phase1 = array[2];
		String phase0 = array[3];
		StructureNet net1 = this.getDataSet().getMinEnergyNet(phase1);
		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase0);
		AppliedTreePotential atp1 = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net1);
		AppliedTreePotential atp0 = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net0);
		atp1.relax(true);
		atp0.relax(true);
		return atp1.getEnergy()/atp1.getStructureNet().numDefiningSites() - atp0.getEnergy()/atp0.getStructureNet().numDefiningSites();
	}
	
//	else if(property.contains("C11") || property.contains("C12") || property.contains("C44")) {
//		String[] array = property.split("_");
//		String propertyName = array[0];
//		double[] strains = new double[] {-0.01,-0.005,0,0.005,0.01};
//		double[] stresses = new double[strains.length];
//		int index = 0;
//		String direction = "";
//		if(propertyName.equals("C11")) {
//			index = 0;
//			direction = "11";
//		}
//		else if(propertyName.equals("C12")) {
//			index = 1;
//			direction = "11";
//		}
//		else if(propertyName.equals("C44")) {
//			index = 4;
//			direction = "12";
//		}
//		for (int i = 0; i < strains.length; i++) {
//			StructureNet net = this.getDataSet().getMinEnergyNet((i+1)+"_"+direction);
//			AppliedTreePotential atp_stress = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net);
//			stresses[i] = atp_stress.getStressComponent(index);
//
//		}
//		double[] x = strains;
//		double[] y = stresses;
//	    WeightedObservedPoints obs = new WeightedObservedPoints();
//	    for (int i = 0; i < x.length; i++) {obs.add(x[i],y[i]);}	    
//	    PolynomialCurveFitter fitter = PolynomialCurveFitter.create(1);
//	    fitter = fitter.withMaxIterations(10000);
//	    double[] coeff = new double[2];
//	    try {
//	    	coeff = fitter.fit(obs.toList());
//		}catch(org.apache.commons.math4.exception.TooManyIterationsException |
//				org.apache.commons.math4.exception.ConvergenceException e) {
//			return Double.NaN;
//		}
//	    
//	    return coeff[1];
//
//	}
	else if(property.contains("a0") || property.contains("C11") ||
			property.contains("C12") || property.contains("C44") || property.contains("BVRH")) {
		String[] array = property.split("_");
		String propertyName = array[0];

		String phase0 = array[1];
		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase0);
		AppliedTreePotential atp0 = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net0);
		
		Status.basic("Relaxing structure with num sites = "+atp0.getStructureNet().numDefiningSites()+" using potential: "+atp0.getPotential().getExpression(false));
		atp0.relax(true);
		
		if(propertyName.equals("a0")) {
			return atp0.getLatticeParameter();
		}
		if(propertyName.equals("BVRH")) {
			return atp0.getBulkModulusVRH();
		}
		return atp0.getElasticConstant(propertyName);
	}

//	else if(property.contains("a0") || property.contains("BVRH")) {
//		String[] array = property.split("_");
//		String propertyName = array[0];
//
//		String phase0 = array[1];
//		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase0);
//		AppliedTreePotential atp0 = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net0);
//		
//		Status.basic("Relaxing structure with num sites = "+atp0.getStructureNet().numDefiningSites()+" using potential: "+atp0.getPotential().getExpression(false));
//		atp0.relax(true);
//		
//		if(propertyName.equals("a0")) {
//			return atp0.getLatticeParameter();
//		}
//		if(propertyName.equals("BVRH")) {
//			return atp0.getBulkModulusVRH();
//		}
//		return atp0.getElasticConstant(propertyName);
//	}
	else if(property.contains("Esurf")) {
		String[] array = property.split("_");
		String surfIndices = array[1];
		String phase = array[2];

		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase);
		AppliedTreePotential atp0 = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), net0);
		Boolean b = atp0.relax(true);
		if(b == null) {
			return Double.NaN;
		}
		double energy0 = atp0.getEnergy()/atp0.getStructureNet().numDefiningSites();
		
		int[] indicesInt = new int[] {Integer.parseInt(Character.toString(surfIndices.charAt(0))),Integer.parseInt(Character.toString(surfIndices.charAt(1))),Integer.parseInt(Character.toString(surfIndices.charAt(2)))};
		
		//Build surface
		Status.detail("Building surface with potential: "+this.getPotential().getExpression(false));
		
		double minSlab = 12.0;
		double minGap = 12.0;
		Status.detail("Making ("+indicesInt[0]+""+indicesInt[1]+""+indicesInt[2]+") surface using minSlab = "+minSlab+" Angstroms and minGap = "+minGap+" Angstroms");
		Structure surf = atp0.getStructureNet().getStructure().makeSurface(indicesInt, minSlab, minGap, 0.0).findPrimStructure().getCompactStructure();
		surf = StructureNet.removeVacancies(surf);
		
		StructureNet surfNet = new StructureNet(surf,atp0.getStructureNet().getCutoffDistance());
		AppliedTreePotential atpSurf = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), surfNet);
		
		Status.detail("Relaxing surface ("+surfIndices+") with num sites = "+atpSurf.getStructureNet().numDefiningSites());
		
		b = atpSurf.relax(false);
		if(b == null) {
			return Double.NaN;
		}
		surf = atpSurf.getStructureNet().getStructure();
		
//		Structure surf = this.getDataSet().getMinEnergyNet(Integer.toString(indicesInt[0])+""+Integer.toString(indicesInt[1])+""+Integer.toString(indicesInt[2])).getStructure();
//		surf = StructureNet.removeVacancies(surf);
//		StructureNet surfNet = new StructureNet(surf,atp0.getStructureNet().getCutoffDistance());
//		AppliedTreePotential atpSurf = new AppliedTreePotential(new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance()), surfNet);

		double energy = atpSurf.getEnergy();
		double surfarea = MSMath.magnitude(MSMath.crossProduct(surf.getCellVectors()[0].getCartesianDirection(), surf.getCellVectors()[1].getCartesianDirection()));
		double n = surf.numDefiningSites();
		
		return 1000.0*(energy - n*energy0)/(2.0*surfarea);
	}
	else if(property.contains("Ev_f")) {
		String[] array = property.split("_");
		String phase = array[2];
		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase);
		TreePotential tree0 = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
		AppliedTreePotential atp0 = new AppliedTreePotential(tree0, net0);
		atp0.relax(true);
		int[][] dime = new int[3][];
		dime[0] = new int[] {2,0,0};
		dime[1] = new int[] {0,2,0};
		dime[2] = new int[] {0,0,2};
		SuperStructure ss = new SuperStructure(atp0.getStructureNet().getStructure(), dime);
		//Create a vacancy
		Structure strucure_vacancy = ss.removeDefiningSite(0);
		StructureNet netvac = new StructureNet(strucure_vacancy, net0.getCutoffDistance());
		TreePotential treevac = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
		AppliedTreePotential atpvac = new AppliedTreePotential(treevac, netvac);
		atpvac.relax(false);
		
		double energy0 = atp0.getEnergy()/atp0.getStructureNet().numDefiningSites();
		double energyvac = atpvac.getEnergy();
		double nvac = atpvac.getStructureNet().numDefiningSites();
		return energyvac - nvac*energy0;
	}
	else if(property.contains("Edumbbell_f")) {
		String[] array = property.split("_");
		String phase = array[2];
		StructureNet net0 = this.getDataSet().getMinEnergyNet(phase);
		TreePotential tree0 = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
		AppliedTreePotential atp0 = new AppliedTreePotential(tree0, net0);
		atp0.relax(true);
		int[][] dime = new int[3][];
		dime[0] = new int[] {2,0,0};
		dime[1] = new int[] {0,2,0};
		dime[2] = new int[] {0,0,2};
		SuperStructure ss = new SuperStructure(atp0.getStructureNet().getStructure(), dime);
		
		StructureBuilder sb = new StructureBuilder(ss);
		//Create a vacancy
		Structure.Site s0 = ss.getDefiningSite(0);
      	Structure strucure_vacancy = ss.removeDefiningSite(0);
      	sb = new StructureBuilder(strucure_vacancy);
        //This gives a vector of magnitude 1 Angstrom along the lattice vector
        Vector v1 = new Vector(ss.getCellVectors()[0].unitVectorCartesian().getCartesianDirection());
        sb.addSite(s0.getCoords().translateBy(v1), s0.getSpecies());
        sb.addSite(s0.getCoords().translateBy(v1.multiply(-1.0)), s0.getSpecies());
        StructureNet netDumbbell = new StructureNet(new Structure(sb), net0.getCutoffDistance());
        
		TreePotential treeDumbbell = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
		AppliedTreePotential atpDumbbell = new AppliedTreePotential(treeDumbbell, netDumbbell);
		atpDumbbell.relax(false);
		
		
		double energy0 = atp0.getEnergy()/atp0.getStructureNet().numDefiningSites();
		double energyDumbbell = atpDumbbell.getEnergy();
		double nDumbbell = atpDumbbell.getStructureNet().numDefiningSites();
		return energyDumbbell - nDumbbell*energy0;
	}
	else {
		Status.basic("Haven't implemented computation of "+property+" !");
	}
	return Double.NaN;
}

//public double[] getEnergiesPredicted(String option) {
//	double[] array;
//	AppliedTreePotential[] atps;
//	if(option.equals("EvsV")) {
//		array = m_PredictedEnergiesEvsV;
//		atps = m_AppliedPotentialsEvsV;
//	}
//	else if(option.equals("Strain11")) {
//		array = m_PredictedEnergiesStrain11;
//		atps = m_AppliedPotentialsStrain11;
//	}
//	else if(option.equals("Strain44")) {
//		array = m_PredictedEnergiesStrain44;
//		atps = m_AppliedPotentialsStrain44;
//	}
//	else {
//		Status.basic("Invalid option!");
//		return null;
//	}
//	if(array == null) {
//		array = new double[atps.length];
//	    for (int i = 0; i < atps.length; i++) {
//		      try{array[i] = atps[i].getEnergy();
//		      }catch(NotFiniteValueException e){
//		    	  Arrays.fill(array, Double.NaN);
//		    	  return array;
//		      }
//	    }
//	    
//	    for (int i = 0; i < array.length; i++) {
//	    	//Energies per atom
//	    	double denominator = TrainingData.getNets(option)[i].numDefiningSites();
//	    	array[i] /= denominator; 
//		}
//		if(option.equals("EvsV")) {m_PredictedEnergiesEvsV = array;}
//		else if(option.equals("Strain11")) {m_PredictedEnergiesStrain11 = array;}
//		else if(option.equals("Strain44")) {m_PredictedEnergiesStrain44 = array;}
//	}
//    return Arrays.copyOf(array, array.length);
//}

/**
 * Use raw data for PCC because the method subtracts average and it is independent of the scale
 * However, for MSE, you have to scale by the standard deviation of the target values. 
 * Moreover, if you have energies and MSE, you should subtract the minimum value. 
 * The minimum value should not be the minimum value of the entire data set because 
 * we are interested in the local performance of the model. If we were interested in the global
 * performance, then we would use all the values in the data set as the target values and then 
 * the minimum would be the one of the entire data set.
 * @param targetValues
 * @param predictedValues
 * @param fitnessByLC
 * @param energy
 * @return
 */
public double getFitness(double[] targetValues, double[] predictedValues, boolean fitnessByLC, boolean energy){
	if(predictedValues == null) {
		return Double.MAX_VALUE;
	}
	
	if(fitnessByLC) {
		double fitness = Statistics.getPCC1minusAbs(targetValues, predictedValues);
		if(!Double.isFinite(fitness)){
			return Double.MAX_VALUE;
		}
		return fitness;
	}
	
	double stdTarget = Statistics.getStandardDeviation(targetValues, true);
	double fitness;
	if (!energy) {
		fitness = Statistics.getMSE(targetValues, predictedValues)/(stdTarget*stdTarget);
	}
	else {
		double minTarget = Statistics.getMinValue(targetValues);
		double minPredicted = Statistics.getMinValue(predictedValues);
		
		double[] newTarget = new double[targetValues.length];
		double[] newPredicted = new double[targetValues.length];
		
		for (int i = 0; i < newTarget.length; i++) {newTarget[i] = targetValues[i] - minTarget;}
		for (int i = 0; i < newPredicted.length; i++) {newPredicted[i] = predictedValues[i] - minPredicted;}
		
		fitness = Statistics.getMSE(newTarget, newPredicted)/(stdTarget*stdTarget);
	}
	if(!Double.isFinite(fitness)){
		return Double.MAX_VALUE;
	}
	if(!fitnessByLC){
		if(Statistics.compare(fitness, m_Convergence) == -1){
			return 0.0;
		}
	}
	return fitness;
}

@Override
public PotentialEvaluator mutate(){
	PotentialEvaluator returnEvaluator;
	double random = GENERATOR.nextDouble();
	double numMutationOptions = 3.0;
	if(random < 1.0/numMutationOptions) {
		returnEvaluator = this.mutateRandomTree();
	}
	else if(random < 2.0/numMutationOptions) {
		returnEvaluator = this.mutateSwap();
	}
	else {
		returnEvaluator = this.mutateGrapht();
	}

	if(returnEvaluator == null) {return null;}
	return returnEvaluator;
}

public static PotentialEvaluator generateRandomEvaluator(boolean valid, double cutoffDistance,DataSet dataSet) {
	PotentialEvaluator randomEvaluator;
	do {
		char method;
		if(GENERATOR.nextDouble() < 0.5) {method = 'f';}
		else {method = 'g';}
		//Generate trees with depth between minDepth and maxDepth, both limits inclusive
//		int depth = m_MinDepthRandomTree+GENERATOR.nextInt(1+m_MaxDepthRandomTree-m_MinDepthRandomTree);
		int depth;
		do {
			depth = (int)Math.round(m_MeanDepthRandomTree + GENERATOR.nextGaussian());
		}while(depth < 1);
		
		TreePotential otherPotential = TreePotential.generateTree(cutoffDistance, depth, method);
		randomEvaluator = new PotentialEvaluator(otherPotential, dataSet);
	}while(!randomEvaluator.isValid() || !valid);
	return randomEvaluator;
}



////From: http://cswww.essex.ac.uk/staff/poli/gp-field-guide/52GPMutation.html
////Subtree mutation: replaces a randomly selected subtree with another randomly created subtree.
//Crossover with a valid randomly created potential
private PotentialEvaluator mutateRandomTree() {
	PotentialEvaluator otherEvaluator = PotentialEvaluator.generateRandomEvaluator(true,this.getPotential().getCutoffDistance(),this.getDataSet());
//  crossover(IIndividual,IIndividual) assumes that "this" and "other" are not always in the same order
//	Therefore, sometimes the mutated individual must be the first one, others it should be the second
	if(GENERATOR.nextBoolean()) {return this.crossover(otherEvaluator);}
	else {return otherEvaluator.crossover(this);}
}


//	  From: http://cswww.essex.ac.uk/staff/poli/gp-field-guide/52GPMutation.html
//	  Swap mutation: permutation mutation restricted to binary non-commutative functions
//	  Permutation mutation: selects a random function node in a tree and then randomly permuting its arguments (subtrees)
//	  Used in this paper: doi: 10.1063/1.3294562
private PotentialEvaluator mutateSwap(){
	Node nodeThisPotential = this.getPotential().getNonCommutative();
	if(nodeThisPotential==null){return null;}
	Node child0 = nodeThisPotential.getChildNode(0).deepCopy();
	Node child1 = nodeThisPotential.getChildNode(1).deepCopy();
	Node swappedNode = null;
	String type = nodeThisPotential.getNodeType();
	if(type.equals("Division")){swappedNode = new DivisionNode(child1,child0);}
	else if(type.equals("Power")){swappedNode = new PowerNode(child1,child0);}
	else if(type.equals("Subtraction")){swappedNode = new SubtractionNode(child1,child0);}
	if(swappedNode==null){return null;}
	TreePotential newPotential = this.getPotential().replaceNode(swappedNode,nodeThisPotential);
	return new PotentialEvaluator(newPotential, this.getDataSet());
}

////Mutate a constant. 
////If done randomly, I think that it would be similar to Brown et al. (2010). http://dx.doi.org/10.1063/1.3294562
////Instead, we optimize it
//// Can optimize the constant with the greatest gradient or a randomly selected constant
//protected PotentialEvaluator simplifyAndOptimizeOneConstant(boolean greatestGradient) {
//	if(this.getPotential().numConstantNodes() < 1) {
//		return null;
//	}
////	Simplify the potential so that the optimization is more 
//	TreePotential simplifiedPotential = this.getPotential().simplify();
//	int numConstants = simplifiedPotential.numConstantNodes();
//	if(numConstants < 1) {
//		return null;
//	}
//	PotentialEvaluator simplifiedEvaluator = new PotentialEvaluator(simplifiedPotential, this.getDataSet());
//	
//	int index = 0;
//	if(greatestGradient) {
//		//Optimize constant with greatest gradient
//		simplifiedEvaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[simplifiedEvaluator.getPotential().getConstantNodes().length];
//		for (int i = 0; i < simplifiedEvaluator.m_MinimizeOnlyTheseNodes.length; i++) {
//			simplifiedEvaluator.m_MinimizeOnlyTheseNodes[i] = simplifiedEvaluator.getPotential().getConstantNode(i);
//		}
//		double[] gradients = simplifiedEvaluator.getGradient(null);
//		double minValue = Statistics.getMinValue(gradients);
//		double maxValue = Statistics.getMaxValue(gradients);
//		double maxabs = Math.abs(maxValue) >= Math.abs(minValue) ? maxValue : minValue;
//		for (int i = 0; i < gradients.length; i++) {if(Statistics.compare(gradients[i], maxabs)==0) {index = i;break;}}
//	}
//	else {
//		index = GENERATOR.nextInt(numConstants);
//	}
//	
//	simplifiedEvaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[] {simplifiedEvaluator.getPotential().getConstantNode(index)};
//	simplifiedEvaluator.optimize("GR-CG");
//	return simplifiedEvaluator;
//}
//
//protected PotentialEvaluator simplifyAndOptimizeOneConstant(int index) {
//	if(this.getPotential().numConstantNodes() < 1) {
//		return null;
//	}
////	Simplify the potential so that the optimization is more 
//	TreePotential simplifiedPotential = this.getPotential().simplify();
//	int numConstants = simplifiedPotential.numConstantNodes();
//	if(numConstants < 1) {
//		return null;
//	}
//	PotentialEvaluator simplifiedEvaluator = new PotentialEvaluator(simplifiedPotential, this.getDataSet());
//	
//	simplifiedEvaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[] {simplifiedEvaluator.getPotential().getConstantNode(index)};
//	simplifiedEvaluator.optimize("GR-CG");
//
//	if(!simplifiedEvaluator.isValid()){
//		return null;
//	}
//	return simplifiedEvaluator;
//}

public PotentialEvaluator simplifyAlgebraic() {
	//Use a simplified potential because if not, then this method just simplifies algebraically
	TreePotential simplified = this.getPotential().simplify();
	PotentialEvaluator thisPotential = new PotentialEvaluator(simplified, this.getDataSet());
	return thisPotential;
}

public PotentialEvaluator optimizeConstants(String[] option) {
	TreePotential potential = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
	PotentialEvaluator newEvaluator = new PotentialEvaluator(potential, this.getDataSet());
	if(this.getPotential().numConstantNodes() < 1) { 
		return newEvaluator;
	}
		
//	Optimize all the constants
//	newEvaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[this.getPotential().numConstantNodes()];
//	for (int i = 0; i < newEvaluator.m_MinimizeOnlyTheseNodes.length; i++) {
//		newEvaluator.m_MinimizeOnlyTheseNodes[i] = newEvaluator.getPotential().getConstantNode(i);
//	}
	newEvaluator.setMinimizeOnlyTheseNodes_all();
	
	newEvaluator.optimize(option);
	return newEvaluator;
}

public void setMinimizeOnlyTheseNodes_all() {
	this.m_MinimizeOnlyTheseNodes = new ConstantNode[this.getPotential().numConstantNodes()];
	for (int i = 0; i < this.m_MinimizeOnlyTheseNodes.length; i++) {
		this.m_MinimizeOnlyTheseNodes[i] = this.getPotential().getConstantNode(i);
	}
}

//From: http://cswww.essex.ac.uk/staff/poli/gp-field-guide/52GPMutation.html
//Used in this paper: doi: 10.1063/1.3294562
//Grapht mutation: Grafting slightly changes the individual. Inserting a single random node is an example of graphting
private PotentialEvaluator mutateGrapht() {
	//False because we don't want to replace the topNode of m_Potential with a random node
	Node nodeThisPotential = this.getPotential().getNonTerminal(false);
	if(nodeThisPotential == null) {
		return null;
	}
	TreePotential newPotential;
//	Grapht by inserting
	if(GENERATOR.nextBoolean()) {
		  newPotential = this.getPotential().insertRandomNonTerminal(nodeThisPotential);
	}
//	Grapht by replacing
	else {
		  newPotential = this.getPotential().replaceRandomNonTerminal(nodeThisPotential);
	}
	return new PotentialEvaluator(newPotential, this.getDataSet());
}

////Similar to: http://cswww.essex.ac.uk/staff/poli/gp-field-guide/52GPMutation.html
////Similar concept as the one used in this paper: doi: 10.1063/1.3294562
////Shrink mutation: special case of subtree mutation where the randomly chosen subtree is replaced by a randomly chosen terminal
//private PotentialEvaluator simplifyAlgebraicAndVariance() {	
//	//Use a simplified potential because if not, then this method just simplifies algebraically
//	TreePotential simplified = this.getPotential().simplify();
//	PotentialEvaluator thisPotential = new PotentialEvaluator(simplified, this.getDataSet());
//	//Need to compute the energies in order to get the average values
//	thisPotential.getPredictedProperty("Energy");
//	Node nodeThisPotential = thisPotential.getNodeWithLeastVariance();
//	TreePotential newPotential = thisPotential.getPotential().replaceNodeWithAvgOverAllStructures(nodeThisPotential,nodeThisPotential.getAverageValueOverAllStructures());
//	return new PotentialEvaluator(newPotential, this.getDataSet());
//}

//private Node getNodeWithLeastVariance() {
//	//Compute the energies in order to get the number of times visited
//	this.getPredictedProperty("Energy");
//	TreePotential copyOfPotential = new TreePotential(this.getPotential().getTopNode().deepCopy(), this.getPotential().getCutoffDistance());
//	PotentialEvaluator copyOfThisEval = new PotentialEvaluator(copyOfPotential, this.getDataSet());
//	Node[] allNodesOfCopy = copyOfPotential.getAllNodes();
//	
//	int c = 0;
//	for (Node node : this.getPotential().getAllNodes()) {
//		double number = node.numForAvgOverAllStructures();
//		//Want to get 100 data points per node, although it will be less if number < 100
//		double probability = 100.0/number;
//		if(!Double.isFinite(probability)) {probability = 0.0;}
//		allNodesOfCopy[c].setSamplingProbability(probability); 
//		if(!allNodesOfCopy[c].getNodeType().equals(node.getNodeType())) {
//			Status.basic("Problem getting node with minimum variance!");
//		}
//		c++;
//	}
//	
//	copyOfThisEval.getPredictedProperty("Energy");
////	Variance of each node
//	double[] variances = new double[this.getPotential().getAllNodes().length];
////	Compute the variance of each node
//	int nodeInd = 0;
//	for (Node node : allNodesOfCopy) {
////		Do not try to simplify constant or distance
//		if(!node.getNodeType().equals("Distance") && !node.getNodeType().equals("Constant")) {
//			ArrayList<Double> sampleValues = node.getSampleValues();
//			double avg = node.getAverageValueOverAllStructures();
//			double variance = 0;
//			for (Double double1 : sampleValues) {
//				variance += Math.pow(double1-avg, 2);
//			}
//			variance /= sampleValues.size();
//			variances[nodeInd] = variance;
//		}
////		Do not try to simplify constant or distance
//		else {
//			variances[nodeInd] = Double.POSITIVE_INFINITY;
//		}
//		nodeInd++;
//	}
//				
//	double minVar = Double.POSITIVE_INFINITY;
//	int minindex = 0;
//	int minIndex = 0;
//	for (double d : variances) {
//		if(minVar > d) {
//			minIndex = minindex; 
//			minVar = d;
//		}
//		minindex++;
//	}
//	return this.getPotential().getAllNodes()[minIndex];
//}

@Override
public IIndividual evolve(IIndividual other) {
	
	PotentialEvaluator returnEvaluator;
	
	double random = GENERATOR.nextDouble();
	
	if(random<0.9) {
		if(GENERATOR.nextDouble() < 0.90) {
			returnEvaluator = this.crossover(other);
		}
		else {
			returnEvaluator = this.getLinearCombination(other);
		}
	}
	else {
		returnEvaluator = this.mutate();
	}
	
	if(returnEvaluator == null) {
//		//Update average score of this operation with the worst value (e.g., 0.0)
//		this.updateAvgScore(0.0);
		return null;
	}
	
	returnEvaluator = returnEvaluator.simplifyAlgebraic();
	
//	double distance = PotentialPopulation.msedistanceToConvexHull(returnEvaluator);
//	double probabilityOfOptimizing = FastMath.exp(-distance);
//	if(probabilityOfOptimizing > GENERATOR.nextDouble()) {
	
//	returnEvaluator = returnEvaluator.optimizeConstants(new String[] {"GR-CG"});

	returnEvaluator = returnEvaluator.optimizeConstants(new String[] {"NLCG,1,3"});

	if(returnEvaluator == null) {
//		//Update average score of this operation with the worst value (e.g., 0.0)
//		this.updateAvgScore(0.0);
		return null;
	}
	
	if(!returnEvaluator.isValid()){
//		//Update average score of this operation with the worst value (e.g., 0.0)
//		this.updateAvgScore(0.0);
		return null;
	}

//	returnEvaluator.m_EvolutionPath = m_EvolutionPath;
	return returnEvaluator;
}

public PotentialEvaluator addScalingFactors() {
	return new PotentialEvaluator(this.getPotential().addScalingToAllDistances(), this.getDataSet());
}

private PotentialEvaluator getLinearCombination(IIndividual other) {
	if(other == null) {
		other = PotentialEvaluator.generateRandomEvaluator(true, this.getPotential().getCutoffDistance(),this.getDataSet());
	}
	
	TreePotential otherTreePotential = ((PotentialEvaluator) other).getPotential();
	
//	True because we want to consider the top node of m_Potential
//	Deep copy because we want to combine two branches
	Node otherBranch = otherTreePotential.getNodeWP(true).deepCopy();
	Node thisBranch = this.getPotential().getNodeWP(true).deepCopy();
//	The method getLinearCombination(Node, Node) returns null if its individual is invalid
	return PotentialEvaluator.getLinearCombination(thisBranch, otherBranch,true,this.getPotential().getCutoffDistance(), this.getDataSet());
}

@Override
public PotentialEvaluator crossover(IIndividual other) {
//	True because we want to consider the top node of m_Potential
//	Deep copy because we want to combine two branches
	Node otherBranch = ((PotentialEvaluator) other).getPotential().getNodeWP(true).deepCopy();
	
//	It is not necessary to do replaceNode(thisBranch,otherNode) because the order of selection from the population
//	is not always the same. So, "this" might be "other" in a future crossover operation
	
//	False because we don't want to replace the top node of m_Potential with a branch of otherTreePotential
//	Replacing the top node is analogous to doing the shrink operation from the method simplifyAlgebraicAndVariance()
	
	Node thisNode = this.getPotential().getNodeWP(false);
	TreePotential newPotential = this.getPotential().replaceNode(otherBranch,thisNode);
	return new PotentialEvaluator(newPotential, this.getDataSet());
}

private static PotentialEvaluator getLinearCombination(Node branch0, Node branch1, boolean optimize, double cutoffDistance, DataSet dataSet) {
//	Add weights
	//Assume almost equal weights, but not equal in case the two branches are the same and they are combined with subtraction
	//In that case, the fitness would be the maximum number in the case of linear correlation and forces
	double x = 0.5 + 1E-8;//add small number so that it is valid in case the two branches are equal
	Node weight0 = new ConstantNode(x);
	Node weightedBranch0 = new MultiplicationNode(weight0,branch0);
	Node weight1 = new ConstantNode(1.0-x);
	Node weightedBranch1 = new MultiplicationNode(weight1,branch1);
	
	Node linearCombination = TreePotential.combineAddOrSubtract(weightedBranch0, weightedBranch1);
	TreePotential tree = new TreePotential(linearCombination, cutoffDistance);
	PotentialEvaluator evaluator = new PotentialEvaluator(tree, dataSet);
	
	if(optimize) {
//		Optimize the weights
		evaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[] {(ConstantNode)weight0,(ConstantNode)weight1};
		evaluator.optimize(new String[] {"GR-CG"});
	}
		
	return evaluator;
}

//private PotentialEvaluator optimizeEnergyScale() {
//
//	Node scaleNode = new ConstantNode(1.0);
//	Node scaledTopNode = new MultiplicationNode(scaleNode,this.getPotential().getTopNode().deepCopy());
//	TreePotential tree = new TreePotential(scaledTopNode, this.getPotential().getCutoffDistance());
//	PotentialEvaluator evaluator = new PotentialEvaluator(tree, this.getDataSet());
//	
////	Optimize the scale
//	evaluator.m_MinimizeOnlyTheseNodes = new ConstantNode[] {(ConstantNode)scaleNode};
//	evaluator.optimize("GR-CG");
//	
//	return evaluator;
//}

private void optimize(String[] option) {
	if(option[0].equals("GR-CG")) {
		PotentialEvaluator.m_Minimizer.minimize(this);
	} 
	else if(option[0].equals("CMA-ES")){
	    try {
	    	this.optimizeWithApache(option);
	    }catch(MaxCountExceededException e) {
	    	Status.basic("MaxCountExceededException");
	    }
		
	}
	else if(option[0].equals("NLCG")){
	    try {
	    	this.optimizeWithApache(option);
	    }catch(MaxCountExceededException e) {
	    	Status.basic("MaxCountExceededException");
	    }
		
	}
	//The minimizer sometimes does not return the best parameters that it found
	if((this.getValueMinimizer() > m_MinValueMinimizer)|| !Double.isFinite(this.getValueMinimizer())) {
		if(m_MinParametersMinimizer == null) {
			m_MinParametersMinimizer = this.getUnboundedParameters(null);
			for (int i = 0; i < m_MinParametersMinimizer.length; i++) {
				int value = (int)m_MinParametersMinimizer[i];
				if((value == 0)||(value == 1)) {value = 2;}
				m_MinParametersMinimizer[i] = value;
			}
		}
		this.setUnboundedParameters(m_MinParametersMinimizer);
	}
}

public TreePotential getPotential() {
    return m_Potential;
}
 
//Check if the generated tree is valid
public boolean isValid() {
	PotentialPopulation.m_NumIsValidCalls++;
	
	TreePotential potential = this.getPotential();
	
	int numLoops = potential.numSiteSums();
	int maxLoops = m_MaxLoops;
	if(numLoops < 1 || numLoops > maxLoops) {return false;}
	int maxSiteSumsUnderASiteSum = m_MaxSiteSumsUnderSiteSum;
	if(potential.maxSiteSumsUnderASiteSum() > maxSiteSumsUnderASiteSum) {return false;}
	
	double maxNumLeaves = Math.pow(2.0, m_MaxDepthRandomTree);
	int numDistances = potential.numDistanceNodes();
	if(numDistances > maxNumLeaves || numDistances < 1) {return false;} //TODO is this necessary?
	if(potential.numConstantNodes() > maxNumLeaves) {return false;}//TODO is this necessary?
	
	int numNodes = potential.numNodes();
	//Maximum number of nodes corresponds to a full binary tree of depth m_MaxDepthRandomTree
	double maxNumNodes = Math.pow(2.0, m_MaxDepthRandomTree + 1.0) - 1.0; //TODO use a greater max numNodes?
	
	if(numNodes < 2) {return false;}
	if(numNodes > maxNumNodes) {return false;}
//	Max depth: we want to allow the formation of constants by addition, and other non-terminal nodes
	if(potential.getDepth() > 4*m_MaxDepthRandomTree) {return false;}

	try {
		if(Arrays.asList(this.getDataSet().getTargetProperties()).contains("Force")) {
			int[][] indicesglobalforce = this.getDataSet().getIndices("Force");
			int i = indicesglobalforce[GENERATOR.nextInt(indicesglobalforce.length)][0];
			m_AppliedPotentials[i].getForceIsValidAnalytical();
		}
		else {
			m_AppliedPotentials[GENERATOR.nextInt(m_AppliedPotentials.length)].getForceIsValidAnalytical();
			}
	} catch (NotFiniteValueException e) {return false;}
	
	if(this.getFitness(null, false) > 1E200) {return false;}
	if(this.getFitness(null, true) > 1E200) {return false;}

    return true;
}

public double numTimesSelected(){
//		return this.getPotential().numTimesSelected();
	return m_NumTimesSelected;
}
	
@Override
public double getCumulativeSelectivityScore() {
	return m_CumulativeSelectivityScore;
}
	
@Override
public void setCumulativeSelectivityScore(double score) {
	m_CumulativeSelectivityScore = score;
}

@Override
public double getSelectivityScore() {
//	double score = getFitness();
//	double score = getFitness()/Math.sqrt(1+m_NumTimesSelected);
//	Offset by 50 to avoid negative scores. Need his to select with weighted probability
//	double score = getFitness() - getPenaltyBIC()+50;
	
//	double s = 2.0;
//	double m = 0.0;
//	double n = this.numTimesSelected();
//	double diffSqr = (n-m)*(n-m);
//	double score = Math.exp(-diffSqr/(2*s*s)); //Gaussian
//	double score = 1.0/(1.0+this.numTimesSelected());
	double score = 1.0;//(1.0+this.getFitness(true, null));
	
//	double score = (getFitness()-getPenaltyAIC()+50)/(1+Math.log(1+m_NumTimesSelected));

	return score;
}

@Override
public int getComplexity() {
	return this.getPotential().getComplexity();
}

public void setComplexity(int complexity){
	this.getPotential().setComplexity(complexity);
}

public void setNumNodes(int numNodes){
	this.getPotential().setNumNodes(numNodes);
}

public String getNodeTypes(){
	if(m_NodeTypes == null) {
	    m_NodeTypes = this.getPotential().getExpression(true);
	}
	return m_NodeTypes;
}

protected void clearValues(boolean changedConstants){
	if(changedConstants){
		this.getPotential().getTopNode().clearKnownValues();
		this.getPotential().getTopNode().clearKnownDerivatives();

	    m_Expression = null;
	    for (AppliedTreePotential atp : m_AppliedPotentials) {
			atp.clearKnownValues();
		}
	    
	    this.intitiallizeDataPredicted();
	}
	m_FitnessTrainingLC = null;
	m_FitnessTrainingMSE = null;
}
	
public PotentialEvaluator createEvaluator(String treeString){
	TreePotential tree = TreePotential.buildTree(treeString,this.getPotential().getCutoffDistance());
	return new PotentialEvaluator(tree, this.getDataSet());
}

public int numNodes() {
	return this.getPotential().numNodes();
}

@Override
public void increaseNumTimesSelected() {
//		this.getPotential().increaseNumTimesSelected();
	m_NumTimesSelected++;
}

//public double[] getValuesPredicted(StructureNet[] nets, String option) {
//	AppliedTreePotential[] atp = new AppliedTreePotential[nets.length];
//	for (int i = 0; i < atp.length; i++) {
//		atp[i] = new AppliedTreePotential(this.getPotential(), nets[i]);
//	}
//	ArrayList<Double> list = new ArrayList<Double>();
//	for (int i = 0; i < nets.length; i++) {
//		Double[] value = null;
//		try{
//			if(option.equals("Stress")) {
//				value = atp[i].getStressComponents();
//			}
//			else if(option.equals("Force")) {
//				value = atp[i].getForcesForwardDifference();
//			}
//		} catch (NotFiniteValueException e) {return null;}
//		for (int j = 0; j < value.length; j++) {
//			list.add(value[j]);
//		}
//	}
//	
//	double[] returnArray = new double[list.size()];
//	int c = 0;
//	for (Double d : list) {
//		returnArray[c] = d.doubleValue();
//		c++;
//	}
//	
//	return returnArray;
//}

//public double[] getForcesPredictedMD(boolean useDynamicIndices) {
//	if(useDynamicIndices) {		
//		int[][] indices;
//		if(TrainingData.getDynamicIndicesForcesMD() != null) {
//			indices = TrainingData.getDynamicIndicesForcesMD();
//		}
//		
//		else {
//			indices = TrainingData.getIndicesGlobalForcesMD();
//		}
//		
//		ArrayList<Double> list = new ArrayList<Double>();
//		for (int[] specificIndices : indices) {
//			//This is the index of the structure
//			int i = specificIndices[0];
//			//This is the index of the force
//			int forceIndex = specificIndices[1];
//			
//			if(m_PredictedForcesDynamicMD[i] == null) {
//				int numForces = TrainingData.getNets("MD")[i].numDefiningSites()*3;
//				m_PredictedForcesDynamicMD[i] = new Double[numForces];
//			}
//			if(m_PredictedForcesDynamicMD[i][forceIndex] == null){
//				double force;
//				try{force = m_AppliedPotentialsMD[i].getForceForwardDifference(forceIndex);
//				} catch (NotFiniteValueException e) {return null;}
//				m_PredictedForcesDynamicMD[i][forceIndex] = force;
//				list.add(force);
//			}
//			else {list.add(m_PredictedForcesDynamicMD[i][forceIndex]);}
//		}
//		
//		double[] returnArray = new double[list.size()];
//		int c = 0;
//		for (Double d : list) {
//			returnArray[c] = d.doubleValue();
//			c++;
//		}
//		
//		return returnArray;
//	}
//	else {
//		ArrayList<Double> list = new ArrayList<Double>();
//		for (int i = 0; i < m_AppliedPotentialsMD.length; i++) {
//			if(m_PredictedForcesDynamicMD[i] == null) {
//				int numForces = TrainingData.getNets("MD")[i].numDefiningSites()*3;
//				m_PredictedForcesDynamicMD[i] = new Double[numForces];
//			}
//			Double[] forces = null;
//			try{
//				forces = m_AppliedPotentialsMD[i].getForcesForwardDifference();
//			} catch (NotFiniteValueException e) {return null;}
//			m_PredictedForcesDynamicMD[i] = forces;
//			for (int j = 0; j < m_PredictedForcesDynamicMD[i].length; j++) {
//				list.add(m_PredictedForcesDynamicMD[i][j]);
//			}
//		}
//		
//		double[] returnArray = new double[list.size()];
//		int c = 0;
//		for (Double d : list) {
//			returnArray[c] = d.doubleValue();
//			c++;
//		}
//		
//		return returnArray;
//	}
//}

//public double[] getStressesPredictedMD(boolean useDynamicIndices) {
//	if(useDynamicIndices) {		
//		int[][] indices;
//		if(TrainingData.getDynamicIndicesStressesMD() != null) {
//			indices = TrainingData.getDynamicIndicesStressesMD();
//		}
//		
//		else {
//			indices = TrainingData.getIndicesGlobalStressesMD();
//		}
//		
//		ArrayList<Double> list = new ArrayList<Double>();
//		for (int[] specificIndices : indices) {
//			//This is the index of the structure
//			int i = specificIndices[0];
//			//This is the index of the force
//			int index = specificIndices[1];
//			
//			if(m_PredictedStressesDynamicMD[i] == null) {
//				m_PredictedStressesDynamicMD[i] = new Double[6];
//			}
//			if(m_PredictedStressesDynamicMD[i][index] == null){
//				double value;
//				try{value = m_AppliedPotentialsMD[i].getVirialStressComponent(index);
//				} catch (NotFiniteValueException e) {return null;}
//				m_PredictedStressesDynamicMD[i][index] = value;
//				list.add(value);
//			}
//			else {list.add(m_PredictedStressesDynamicMD[i][index]);}
//		}
//		
//		double[] returnArray = new double[list.size()];
//		int c = 0;
//		for (Double d : list) {
//			returnArray[c] = d.doubleValue();
//			c++;
//		}
//		
//		return returnArray;
//	}
//	else {
//		ArrayList<Double> list = new ArrayList<Double>();
//		for (int i = 0; i < m_AppliedPotentialsMD.length; i++) {
//			if(m_PredictedStressesDynamicMD[i] == null) {
//				m_PredictedStressesDynamicMD[i] = new Double[6];
//			}
//			Double[] values = null;
//			try{
//				values = m_AppliedPotentialsMD[i].getVirialStresseComponents();
//			} catch (NotFiniteValueException e) {return null;}
//			m_PredictedStressesDynamicMD[i] = values;
//			for (int j = 0; j < m_PredictedStressesDynamicMD[i].length; j++) {
//				list.add(m_PredictedStressesDynamicMD[i][j]);
//			}
//		}
//		
//		double[] returnArray = new double[list.size()];
//		int c = 0;
//		for (Double d : list) {
//			returnArray[c] = d.doubleValue();
//			c++;
//		}
//		
//		return returnArray;
//	}
//}

//public Double[][] getStressesPredictedMD(StructureNet[] nets) {
//	Double[][] returnArray = new Double[nets.length][6];
//
//	AppliedTreePotential[] atps = new AppliedTreePotential[nets.length];
//	for (int i = 0; i < atps.length; i++) {
//		atps[i] = new AppliedTreePotential(this.getPotential(), nets[i]);
//	}
//	for (int i = 0; i < atps.length; i++) {
//		try{
//			returnArray[i] = atps[i].getVirialStresseComponents();
//		} catch (NotFiniteValueException e) {return null;}
//	}
//		
//	return returnArray;
//
//}

public String getExpression(){
	if(m_Expression == null){
		m_Expression = this.getPotential().getExpression(false);
	}
	return m_Expression;
}
public void setFitness(double trainingLC, double trainingMSE){
	m_FitnessTrainingLC = trainingLC;
	m_FitnessTrainingMSE = trainingMSE;
}

//private void updateAvgScore(double score){
//	if(m_EvolutionPath != null) {		
//		score += 1E-8;//Avoids division by zero when computing the probabilities
//		for (String string : m_EvolutionPath) {
//			//Need this because the probability of a particular operation is equal to its average operation score
//			//divided by the sum of all the average operation scores
//			if(string.equals("crossover")) {
//				m_CrossoverNum++;
//				double num = m_CrossoverNum;
//				double avg = m_CrossoverAvgScore;
//				m_CrossoverAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("mutateRandomTree")){
//				m_MutateRandomTreeNum++;
//				double num = m_MutateRandomTreeNum;
//				double avg = m_MutateRandomTreeAvgScore;
//				m_MutateRandomTreeAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("mutateSwap")){
//				m_MutateSwapNum++;
//				double num = m_MutateSwapNum;
//				double avg = m_MutateSwapAvgScore;
//				m_MutateSwapAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("mutateGrapht")){
//				m_MutateGraphtNum++;
//				double num = m_MutateGraphtNum;
//				double avg = m_MutateGraphtAvgScore;
//				m_MutateGraphtAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("simplifyAlgebraicallyAndVariance")){
//				m_SimplVarNum++;
//				double num = m_SimplVarNum;
//				double avg = m_SimplVarAvgScore;
//				m_SimplVarAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("crossoverLinearCombination")){
//				m_CrossLinearCombNum++;
//				double num = m_CrossLinearCombNum;
//				double avg = m_CrossLinearCombAvgScore;
//				m_CrossLinearCombAvgScore = (avg*(num-1)+score)/(num);
//			}
//			else if(string.equals("mutateLinearCombination")){
//				m_MutLinearCombNum++;
//				double num = m_MutLinearCombNum;
//				double avg = m_MutLinearCombAvgScore;
//				m_MutLinearCombAvgScore = (avg*(num-1)+score)/(num);
//			}
//		}
//		m_EvolutionPath = null;
//	}
//}
//
//private static void updateEvolutionProbabilities() {
//	double sum = PotentialEvaluator.getSumAvgOperationScores();
//	m_CrossoverProb = m_CrossoverAvgScore/sum;
//	m_MutateRandomTreeProb = m_MutateRandomTreeAvgScore/sum;
//	m_MutateSwapProb = m_MutateSwapAvgScore/sum;
//	m_MutateGraphtProb = m_MutateGraphtAvgScore/sum;
//	m_SimplVarProb = m_SimplVarAvgScore/sum;
//	m_CrossLinearCombProb = m_CrossLinearCombAvgScore/sum;
//	m_MutLinearCombProb = m_MutLinearCombAvgScore/sum;
//}
//
//private static double getSumAvgOperationScores() {
//	double sum = 0;
//	sum += m_CrossoverAvgScore;
//	sum += m_MutateRandomTreeAvgScore;
//	sum += m_MutateSwapAvgScore;
//	sum += m_MutateGraphtAvgScore;
//	sum += m_SimplVarAvgScore;
//	sum += m_CrossLinearCombAvgScore;
//	sum += m_MutLinearCombAvgScore;
//	return sum;
//}
//
//private ArrayList<String> getEvolutionPath(){
//	if(m_EvolutionPath != null) {return m_EvolutionPath;}
//	return null;
//}

///**
// * Parameter bulkModAndC11: bulk modulus at 0, C11 at 1
// * 
// * @param elasticConstant of cubic system
// * @param bulkModAndC11: bulk modulus at 0, C11 at 1
// * @return value of the elastic constant
// */
//public double getElasticConstant(String elasticConstant, Double[] bulkModAndC11, double[] predictedEnergiesPerAtom) {
//	if(m_PredictedElasticConstants == null) {m_PredictedElasticConstants = new Double[4];}
//	if(elasticConstant.equals("C12")) {
//		if(m_PredictedElasticConstants[2]!=null) {
//			return m_PredictedElasticConstants[2];
//		}
//		double c12 = (3*bulkModAndC11[0] - bulkModAndC11[1])/2;
//	    m_PredictedElasticConstants[2] = c12;
//		return c12;
//	}
//	
//	StructureNet[] nets;
//	if(elasticConstant.equals("BulkModulus")) {
//		if(m_PredictedElasticConstants[0]!=null) {
//			return m_PredictedElasticConstants[0];
//		}
//		nets = TrainingData.getNets("BulkModulus");
//	}
//	else if(elasticConstant.equals("C11")) {
//		if(m_PredictedElasticConstants[1]!=null) {
//			return m_PredictedElasticConstants[1];
//		}
//		nets = TrainingData.getNets("C11");
//	}
//	else if(elasticConstant.equals("C44")) {
//		if(m_PredictedElasticConstants[3]!=null) {
//			return m_PredictedElasticConstants[3];
//		}
//		nets = TrainingData.getNets("C44");
//	}
//	else {
//		Status.basic("Invalid elastic constant");
//		return Double.NaN;
//	}
//	
//	//These should be the strains or volumes per atom
//	double[] x;
//	//Use atomic volume if you want BulkModulus
//	if(elasticConstant.equals("BulkModulus")) {
//		x = new double [nets.length];
//		for (int i = 0; i < x.length; i++) {
//			x[i] = nets[i].getStructure().getDefiningVolume()/nets[i].numDefiningSites();
//		}
//	}
//	//Use the strain if you want other elastic constants
//	else if(elasticConstant.equals("C11")) {x = new double[] {-0.01,-0.005,0.0,0.005,0.01};}
//	else if(elasticConstant.equals("C44")) {x = new double[] {-0.0125,-0.0075,0.0,0.0075,0.0125};}
//	else {x = null;}
//	
//	//These should be the predicted energies per atom
//	double[] y = new double[x.length];
//	
//	if(elasticConstant.equals("BulkModulus")) {
//		y = predictedEnergiesPerAtom;
//	}
//	else {
//		double v0 = nets[2].getStructure().getDefiningVolume();
//		for (int i = 0; i < y.length; i++) {
//			double energyPerVolume = predictedEnergiesPerAtom[i]*nets[i].numDefiningSites()/v0;
//		    //Convert from eV per cubic angstrom to GPa
//			y[i] = energyPerVolume*160.218;
//		}
//	}
//	
//    WeightedObservedPoints obs = new WeightedObservedPoints();
//    for (int i = 0; i < x.length; i++) {obs.add(x[i],y[i]);}
//    PolynomialCurveFitter fitter = PolynomialCurveFitter.create(2);
//    fitter = fitter.withMaxIterations(20);
//    
//    double[] coeff = new double[3];
//    try {coeff = fitter.fit(obs.toList());}catch(org.apache.commons.math3.exception.TooManyIterationsException e) {}
//    
//	if(elasticConstant.equals("BulkModulus")) {
//	    //Solve for equilibrium volume by dE/dv = 0
//	    double v0 = -coeff[1]/(2*coeff[2]);
//	    //Evaluate the second derivative at v0
//	    double secondEdv = coeff[2]*2;
//	    double bM = v0*secondEdv;
//	    //Convert from eV per cubic angstrom to GPa
//	    bM *= 160.218;
//	    m_PredictedElasticConstants[0] = bM;
//		return bM;
//	}
//	//Note: the case elasticConstant is an invalid String is covered by returning at the beginning of this method
//	else {
//		double elasticConstantValue =  2*coeff[2];
//	    //C11 or C44 = second derivative of (fitted second order polynomial) with respect to strain
//		if(elasticConstant.equals("C11")) {
//			m_PredictedElasticConstants[1] = elasticConstantValue;
//		}
//		else {
//			m_PredictedElasticConstants[3] = elasticConstantValue;
//		}
//	    return elasticConstantValue;
//	}
//}

///**
// * Parameter bulkModAndC11: bulk modulus at 0, C11 at 1
// * 
// * @param elasticConstant of cubic system
// * @param bulkModAndC11: bulk modulus at 0, C11 at 1
// * @return value of the elastic constant
// */
//public double getElasticConstantValidation(String elasticConstant, Double[] bulkModAndC11, double[] predictedEnergiesPerAtom) {
//	if(elasticConstant.equals("C12")) {
//		double c12 = (3*bulkModAndC11[0] - bulkModAndC11[1])/2;
//		return c12;
//	}
//	StructureNet[] nets;
//	if(elasticConstant.equals("BulkModulus")) {
//		nets = ValidationData.getNets("BulkModulus");
//	}
//	else if(elasticConstant.equals("C11")) {
//		nets = ValidationData.getNets("C11");
//	}
//	else if(elasticConstant.equals("C44")) {
//		nets = ValidationData.getNets("C44");
//	}
//	else {
//		Status.basic("Invalid elastic constant");
//		return Double.NaN;
//	}
//	
//	//These should be the strains or volumes per atom
//	double[] x;
//	//Use atomic volume if you want BulkModulus
//	if(elasticConstant.equals("BulkModulus")) {
//		x = new double [nets.length];
//		for (int i = 0; i < x.length; i++) {
//			x[i] = nets[i].getStructure().getDefiningVolume()/nets[i].numDefiningSites();
//		}
//	}
//	//Use the strain if you want other elastic constants
//	else if(elasticConstant.equals("C11")) {x = new double[] {-0.01,-0.005,0.0,0.005,0.01};}
//	else if(elasticConstant.equals("C44")) {x = new double[] {-0.0125,-0.0075,0.0,0.0075,0.0125};}
//	else {x = null;}
//	
//	//These should be the predicted energies per atom
//	double[] y = new double[x.length];
//	
//	if(elasticConstant.equals("BulkModulus")) {
//		y = predictedEnergiesPerAtom;
//	}
//	else {
//		double v0 = nets[2].getStructure().getDefiningVolume();
//		for (int i = 0; i < y.length; i++) {
//			double energyPerVolume = predictedEnergiesPerAtom[i]*nets[i].numDefiningSites()/v0;
//		    //Convert from eV per cubic angstrom to GPa
//			y[i] = energyPerVolume*160.218;
//		}
//	}
//	
//    WeightedObservedPoints obs = new WeightedObservedPoints();
//    for (int i = 0; i < x.length; i++) {obs.add(x[i],y[i]);}
//    PolynomialCurveFitter fitter = PolynomialCurveFitter.create(2);
//    fitter = fitter.withMaxIterations(20);
//    
//    double[] coeff = new double[3];
//    try {coeff = fitter.fit(obs.toList());}catch(org.apache.commons.math3.exception.TooManyIterationsException e) {}
//    
//	if(elasticConstant.equals("BulkModulus")) {
//	    //Solve for equilibrium volume by dE/dv = 0
//	    double v0 = -coeff[1]/(2*coeff[2]);
//	    //Evaluate the second derivative at v0
//	    double secondEdv = coeff[2]*2;
//	    double bM = v0*secondEdv;
//	    //Convert from eV per cubic angstrom to GPa
//	    bM *= 160.218;
//		return bM;
//	}
//	//Note: the case elasticConstant is an invalid String is covered by returning at the beginning of this method
//	else {
//		double elasticConstantValue =  2*coeff[2];
//	    //C11 or C44 = second derivative of (fitted second order polynomial) with respect to strain
//	    return elasticConstantValue;
//	}
//}

//public double getFitnessElasticConstants(boolean fitnessByLC) {
//	
//	double[] predictedEnergiesBulkModulus = this.getEnergiesPredictedElasticConstant("BulkModulus");
//	double[] predictedEnergiesC11 = this.getEnergiesPredictedElasticConstant("C11");
//	double[] predictedEnergiesC44 = this.getEnergiesPredictedElasticConstant("C44");
//	double[] knownEnergiesBulkModulus = TrainingData.getEnergiesKnown("BulkModulus");
//	double[] knownEnergiesC11 = TrainingData.getEnergiesKnown("C11");
//	double[] knownEnergiesC44 = TrainingData.getEnergiesKnown("C44");
//
//	double fitnessEnergiesBulkModulus = this.getFitness(knownEnergiesBulkModulus, predictedEnergiesBulkModulus, fitnessByLC, true);
//	double fitnessEnergiesC11 = this.getFitness(knownEnergiesC11, predictedEnergiesC11, fitnessByLC, true);
//	double fitnessEnergiesC44 = this.getFitness(knownEnergiesC44, predictedEnergiesC44, fitnessByLC, true);
//	
//	double bulkModulus = this.getElasticConstant("BulkModulus", null,predictedEnergiesBulkModulus);
//	double c11 = this.getElasticConstant("C11", null,predictedEnergiesC11);
//	double c12 = this.getElasticConstant("C12", new Double[] {bulkModulus,c11},null);
//	double c44 = this.getElasticConstant("C44", null,predictedEnergiesC44);
//	
//	double[] predictedElasticConstants = new double[] {c11, c12,c44,bulkModulus};
//	double[] knonwnElasticConstants = TrainingData.getElasticConstants();
//	double fitnessElasticConstants = this.getFitness(knonwnElasticConstants, predictedElasticConstants, fitnessByLC, false);
//	
////	for (int i = 0; i < knonwnElasticConstants.length; i++) {
//////		System.out.println(100*(predictedElasticConstants[i]-knonwnElasticConstants[i])/knonwnElasticConstants[i]);
//////		System.out.println(Math.pow(predictedElasticConstants[i]-knonwnElasticConstants[i], 2));
////		System.out.println(predictedElasticConstants[i]+" "+knonwnElasticConstants[i]);
////	}
//
//	double wE = 0.25;
//	return (wE/3.0)*fitnessEnergiesBulkModulus + (wE/3.0)*fitnessEnergiesC11 + (wE/3.0)*fitnessEnergiesC44 + (1.0-wE)*fitnessElasticConstants;
//}


public DataSet getDataSet() {
	return m_DataSet;
}
private HashMap<String, Double[][]> getPredictedMap(){
	return m_DataPredicted;
}

private void optimizeWithApache(String[] option) throws MaxCountExceededException {
	MultivariateFunction mf = this;
	double[] initialGuess = this.getUnboundedParameters(null);
	if(option[0].equals("CMA-ES")) {
		ConvergenceChecker<PointValuePair> checker = new SimplePointChecker<PointValuePair>(1e-12, 1e-12);
		UniformRandomProvider rng = new UniformRandomProviderImplementation();
		// source: https://github.com/CMA-ES/pycma/blob/master/cma/evolution_strategy.py
		//'popsize': '4+int(3*np.log(N))  # population size, AKA lambda, number of new solution per iteration',
//		int popSize = 4 + (int)(3*Math.log(initialGuess.length)); // 10;
		int popSize = option.length > 1 ? Integer.parseInt(option[1]) : 4 + (int)(3*Math.log(initialGuess.length));
		// 'maxiter': '100 + 150 * (N+3)**2 // popsize**0.5  #v maximum number of iterations',
		int maxIterations = 100 + 150*(int)(Math.pow((initialGuess.length+3), 2.0)/(Math.pow(popSize, 0.5)));
		double stopFitness = 1e-9;
		boolean isActiveCMA = true;//Chooses the covariance matrix update method
		int diagonalOnly = 0;// Number of initial iterations, where the covariance matrix remains diagonal.
		int checkFeasableCount = 0;// Determines how often new random objective variables are generated in case they are out of bounds.
		boolean generateStatistics = false;// Whether statistic data is collected.

		CMAESOptimizer cmaes = new CMAESOptimizer(maxIterations, stopFitness, 
				isActiveCMA, diagonalOnly, checkFeasableCount, rng, generateStatistics, checker);
		OptimizationData goalType = GoalType.MINIMIZE;
		OptimizationData ig  = new InitialGuess(initialGuess);
		OptimizationData ps = new CMAESOptimizer.PopulationSize(popSize);
		double[] initialSigma = new double[initialGuess.length];
		//CMA-ES Source Code -> Practical Hints -> Initial Values
		//http://cma.gforge.inria.fr/cmaes_sourcecode_page.html#practical
		for (int i = 0; i < initialSigma.length; i++) {
			initialSigma[i] = 0.001;
		}
		
		OptimizationData sigma = new CMAESOptimizer.Sigma(initialSigma);
		double[] lower = new double[initialGuess.length];
		double[] upper = new double[initialGuess.length];
		for (int i = 0; i < upper.length; i++) {
			lower[i] = Double.NEGATIVE_INFINITY;
			upper[i] = Double.POSITIVE_INFINITY;
		}
		OptimizationData bounds = new SimpleBounds(lower,upper);
		OptimizationData of = new ObjectiveFunction(mf);
		OptimizationData maxEval = new MaxEval(maxIterations);
		OptimizationData maxIter = new MaxIter(maxIterations);
		cmaes.optimize(ig, goalType,ps,sigma,bounds,of,maxEval,maxIter);
	}
	else if(option[0].equals("NLCG")) {
		ConvergenceChecker<PointValuePair> checker = new SimplePointChecker<PointValuePair>(1e-5, 1e-5);
		NonLinearConjugateGradientOptimizer optimizer = new NonLinearConjugateGradientOptimizer(NonLinearConjugateGradientOptimizer.Formula.POLAK_RIBIERE, checker);
		//Function to be minimized
		ObjectiveFunction of = new ObjectiveFunction(mf);
		OptimizationData ofg = new ObjectiveFunctionGradient(((PotentialEvaluator)mf).getGradient());
		OptimizationData goalType = GoalType.MINIMIZE; 
		OptimizationData ig  = new InitialGuess(initialGuess);
		int maxIterlValue = option.length > 1 ? Integer.parseInt(option[1]) : 1;
		OptimizationData maxIter = new MaxIter(maxIterlValue);
		int maxEvalValue = option.length > 2 ? Integer.parseInt(option[2]) : 3;
		OptimizationData maxEval = new MaxEval(maxEvalValue);
//		System.out.println("MSE = " +this.getValue()+" and MSE_Gradient = "+Arrays.toString(this.getGradient(null))+ " at Point = "+Arrays.toString(this.getUnboundedParameters(null)));	
		optimizer.optimize(of,ofg,ig,goalType,maxIter, maxEval);
//		System.out.println("MSE = " +pvp.getValue()+" and MSE_Gradient = "+Arrays.toString((new Gradient_of_MultivariateFunction(mf)).value(pvp.getPoint()))+ " at Point = "+Arrays.toString(pvp.getPoint()));	
	}

}
/**
 * @param args
 * 			args[0]="MiniBatch" or "All" (training data to use), 
 * 			args[1]=initial population size for CMA-ES
 * 			args[2]=increment in population size
 * 			args[3]=number of restarts 
 * @return
 */
public PotentialEvaluator optimize_CMA_ES_restarting(String[] args) {
	String subsetName = args[0];
	Integer popSizeInitial = Integer.parseInt(args[1]);
	Integer popSizeIncrement = Integer.parseInt(args[2]);
	Integer numberOfRestarts = Integer.parseInt(args[3]);
	
	PotentialEvaluator pe = this;
	Status.basic(pe.getExpression());
	
	String subsetName0 = pe.getDataSet().getSubSet();
	pe.getDataSet().setSubset(subsetName);
	pe.clearValues(false);
	Status.basic("SubsetName "+subsetName);
	
	Integer popSize = popSizeInitial;
	int num = 0;
	while(num <= numberOfRestarts) {
		Status.basic("Population size "+popSize);
		Status.basic("Model "+pe.getPotential().getExpression(false));
		Status.basic(this.getDataSet().getTitle()+" Fitness "+pe.getFitness(null, false));
		
		pe.setMinimizeOnlyTheseNodes_all();
//		pe.setUnboundedParameters(PotentialEvaluator.transformParams(pe.getUnboundedParameters(null), true));
		pe = pe.optimizeConstants(new String[] {"CMA-ES",popSize.toString()});
		Status.basic("Model Optimized with CMA-ES "+pe.getPotential().getExpression(false));
		Status.basic(this.getDataSet().getTitle()+" Fitness "+pe.getFitness(null, false));

		pe = pe.optimizeConstants(new String[] {"NLCG","100","100"});
		Status.basic("Model Optimized with NLCG "+pe.getPotential().getExpression(false));
		Status.basic(this.getDataSet().getTitle()+" Fitness "+pe.getFitness(null, false));
		
		String[] properties = this.getDataSet().getTargetProperties();
		for (String string : properties) {
			double[] predicted = pe.getPredictedProperty(string);
			double[] target = pe.getDataSet().getTargetProperty(string);
			if(string.equals("Energy")) {
				predicted = Statistics.arrayAdd(predicted, -Statistics.getMinValue(predicted));
				target = Statistics.arrayAdd(target, -Statistics.getMinValue(target));
			}
			Status.basic(this.getDataSet().getTitle()+" "+string+" "+Statistics.getMAE(predicted, target));
		}
		popSize += popSizeIncrement;
		num++;
	}

	pe.getDataSet().setSubset(subsetName0);
	pe.clearValues(false);
	return pe;
}


@Override
public double value(double[] point) {
//	this.setUnboundedParameters(transformParams(point, false));
	this.setUnboundedParameters(point);
	
	return this.getValue();
	
//	double quantityToBeMinimized = Double.MAX_VALUE;
////	quantityToBeMinimized = this.getFitness(null, false);
//
//	if(quantityToBeMinimized < m_MinValueMinimizer) {
//		m_MinValueMinimizer = quantityToBeMinimized;
//		m_MinParametersMinimizer = this.getUnboundedParameters(null);
//	}
////	this.setUnboundedParameters(point);
//	return quantityToBeMinimized;
}

private String getGradientOptimizationObjective() {
	return m_MultivariateFunction_value;
}
/**
 * 
 * @param params
 * @param inverse means that params are converted into range [0,10]
 * @return
 */
// Encoding of Variables
//Source: http://cma.gforge.inria.fr/cmaes_sourcecode_page.html#python
public static double[] transformParams(double[] params,boolean inverse) {
//	In summary, to map the values [0;10] into [a;b] 
//	we have the alternative transformations 
//	a + (b-a) * x/10
//	a + (b-a) * (x/10)^2 ≥ a
//	a × (b/a)^(x/10) ≥ 0
	
	//Boundary and Constraint Handling
//	if(!inverse) {
//		double maxVal = 10;
//		double minVal = 0;
//		for (int i = 0; i < params.length; i++) {
//			params[i] = minVal + (maxVal-minVal) * (1.0 - Math.cos(Math.PI * params[i] / 10.0)) / 2.0;
//		}
//	}
//	if(!inverse) {
//		for(int i = 0; i < params.length; i++) {
//			if(params[i]<0 | params[i] > 10 | !Double.isFinite(params[i])) {
//				System.out.println("issue");;
//			}
//		}	
//	}

	double[] returnArray = new double[params.length];
	//I want even search through the linear parameters
	//across a wide range of values
	double a0 = 100;
	double b0 = 100000;
	if(inverse) {
		returnArray[0] = (10*Math.log(params[0]/a0))/Math.log(b0/a0);
		returnArray[2] = (10*Math.log(params[2]/a0))/Math.log(b0/a0);
	}
	else {
		returnArray[0] = a0 * Math.pow((b0/a0),params[0]/10);
		returnArray[2] = a0 * Math.pow((b0/a0),params[2]/10);
	}
	
	double a1 = -20;
	double b1 = -1;
	if(inverse) {
		returnArray[1] = 10.0*(a1-params[1])/(a1-b1);
		returnArray[3] = 10.0*(a1-params[3])/(a1-b1);
	}
	else {
		returnArray[1] = a1 + (b1 - a1)*params[1]/10;
		returnArray[3] = a1 + (b1 - a1)*params[3]/10;
	}
	
	double a2 = -1;
	double b2 = 1;
//	if(inverse) {
//		returnArray[4] = 10.0*(a2-params[4])/(a2-b2);
//	}
//	else {
//		returnArray[4] = a2 + (b2 - a2)*params[4]/10;
//	}
//	if(inverse) {
//		for(int i = 0; i < returnArray.length; i++) {
//			if(returnArray[i]<0 | returnArray[i] > 10 | !Double.isFinite(returnArray[i])) {
//				System.out.println("issue");;
//			}
//		}
//	}
	return returnArray;
}
public Gradient_of_MultivariateFunction getGradient() {
	return new Gradient_of_MultivariateFunction(this);
}
private class Gradient_of_MultivariateFunction implements MultivariateVectorFunction{
	public PotentialEvaluator m_mvi;
	public Gradient_of_MultivariateFunction(MultivariateFunction mvi) {
		m_mvi = (PotentialEvaluator) mvi;
	}
	@Override
	public double[] value(double[] point) throws IllegalArgumentException {
		m_mvi.setUnboundedParameters(point);
		return m_mvi.getGradient(null);
	}
}

}
