/*
 * Created on Aug 10, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import matsci.structure.Structure;
import matsci.util.arrays.ArrayUtils;
import potentialModelsGP.tree.neighbors.SiteSumNode;
import evaluator.NotFiniteValueException;
import evaluator.StructureNet;
import evaluator.StructureNet.Bond;

public abstract class Node implements INodeFactory {

  public static final Random GENERATOR = new Random();
  protected Node[] m_ChildNodes;
  private double m_NumTimesSelected = 0;
//ID is used to find parent of this node
//  private String m_ID;
  private Integer m_NumDescendants;
//  private double m_AverageOverAllStructures = 0.0;
//  private double m_CountForAverageOverAllStructures = 0;
//  private double m_SamplingProbability = 0;
//  private ArrayList<Double> m_SamplesForVariance =  new ArrayList<Double>();
  
  protected ConcurrentHashMap<Structure.Site, Double> m_KnownValues = new ConcurrentHashMap<Structure.Site, Double>();
  
  protected ConcurrentHashMap<InfoHash, Double> m_KnownDerivatives = new ConcurrentHashMap<InfoHash, Double>();
  
  public Node(Node[] childNodes) {
	  m_ChildNodes = (Node[]) ArrayUtils.copyArray(childNodes);
  }
  public Node(Node[] childNodes, HashMap<StructureNet.Site, Double> siteSumRMap) {
	  m_ChildNodes = (Node[]) ArrayUtils.copyArray(childNodes);
  }
  
  public double getValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException{
	Double value = this.getFromKnownValues(bond); 
	if (value == null) {
	  value = this.calculateValue(bond, definingSite, axis, daxis);
	  if(!Double.isFinite(value)){throw new NotFiniteValueException();}
	  this.putInKnownValues(bond, value);
	  
//	  if(definingSite == null) {
//		  boolean b = axis == null || daxis == null;
//		  if((!this.getNodeType().equals("Constant")) && (!this.getNodeType().equals("Distance")) && b) {
//			  m_CountForAverageOverAllStructures += 1.0;
//			  double count = m_CountForAverageOverAllStructures;
//			  double average = m_AverageOverAllStructures;
//			  m_AverageOverAllStructures = ((count-1.0)/count)*average + (value/count);
//			  
//			  if(m_SamplingProbability > 0) {
//				  if(GENERATOR.nextDouble() < m_SamplingProbability) {
//					  m_SamplesForVariance.add(value);
//				  }
//			  }
//		  }
//	  }
	}
	return value;
  }
  
	public double getDerivative(Bond bond, ConstantNode variable, Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
		Double derivative = this.getFromKnownDerivatives(bond, definingSite, axis);	
		if (derivative == null) {
			derivative = this.calculateDerivative(bond, variable, definingSite, axis);
			if(!Double.isFinite(derivative)){throw new NotFiniteValueException();}
			this.putInKnownDerivatives(bond, definingSite, axis, derivative);
		}
		
		return derivative;
	}
	
  public void clearKnownValues() {
    m_KnownValues = new ConcurrentHashMap<Structure.Site, Double>();
    for (int childNum = 0; childNum < m_ChildNodes.length; childNum++) {
      m_ChildNodes[childNum].clearKnownValues();
    }
  }
  
  public void clearKnownDerivatives() {
	    m_KnownDerivatives = new ConcurrentHashMap<InfoHash, Double>();
	    for (int childNum = 0; childNum < m_ChildNodes.length; childNum++) {
	      m_ChildNodes[childNum].clearKnownDerivatives();
	    }
	  }

  
  private void numDescendants(Node root) {
	  for(Node child : root.m_ChildNodes){
		  root.m_NumDescendants += (1+child.numDescendants());
	  }        
	} 
  
  public int numDescendants() {
	  if(m_NumDescendants == null) {
		  m_NumDescendants = 0;
		  this.numDescendants(this);
	  }
	  return m_NumDescendants;
  }
  
  public int getComplexity(int complexity) {
	  	if(m_ChildNodes.length == 0){
	  		return this.getComplexity();
	  	}
	  	int localComplexity = 0;
	    for (int childNum = 0; childNum < m_ChildNodes.length; childNum++) {
	    	localComplexity += m_ChildNodes[childNum].getComplexity(complexity);
	    }
	    complexity += localComplexity;
//	    if(this.getNodeType().equals("SiteSum")) {
//		    complexity += SiteSumNode.m_AvgNumNeighbors*(complexity + this.getComplexity());
//	    }
//	    else {
	    	complexity += this.getComplexity();
//	    }
	    return complexity;
  }
  
  public int numSiteSums(int num) {
	  	if(m_ChildNodes.length == 0){
	  		if(this.getNodeType().equals("SiteSum")) {return 1;}
	  		return 0;
	  	}
	  	int localNum = 0;
	    for (int childNum = 0; childNum < m_ChildNodes.length; childNum++) {
	    	localNum += m_ChildNodes[childNum].numSiteSums(num);
	    }
	    num += localNum;
	    if(this.getNodeType().equals("SiteSum")) {num ++;}
	    
	    return num;
	  }

//  public double getAverageValueOverAllStructures() {
//	 return m_AverageOverAllStructures;
//  }
  
  public double getNumTimesSelected(){
	  return m_NumTimesSelected;
  }
  
  public int numChildNodes() {
    return m_ChildNodes.length;
  }

  public Node getChildNode(int childNodeNum) {
    return m_ChildNodes[childNodeNum];
  }
  
  protected void replaceChildNode(int childNodeNum, Node node) {
	m_NumDescendants = null;
    m_ChildNodes[childNodeNum] = node;
  }
  
  public Node deepCopy() {
    Node newNode = this.copy();
    for (int nodeNum = 0; nodeNum < m_ChildNodes.length; nodeNum++) {
      newNode.replaceChildNode(nodeNum, m_ChildNodes[nodeNum].deepCopy());
    }
    return newNode;
  }
  
public void increaseNumTimesSelected() {
	m_NumTimesSelected++;
}
  
  public void setValues(double count){
	  m_NumTimesSelected = count;
  }
  
public static double generateRandomValue(){	  
	double value = GENERATOR.nextDouble()*20.0;
	if(GENERATOR.nextBoolean()){
		value *= -1.0;
	}
	return value;
	  
//	//2 and 3 because 1 simplifies for division and multiplication. 
//	//2 and 3 because if only use 2 then subtraction introduces 0
//	if(GENERATOR.nextDouble()<0.5) {
//		return 2.0;
//	}
//	return 3.0;
}
  
public ConcurrentHashMap<Structure.Site, Double> getKnownValues(){
	return m_KnownValues;
}
  
public ConcurrentHashMap<InfoHash, Double> getKnownDerivatives(){
	return m_KnownDerivatives;
}
  
public Node copyReference(){
	return this;
}
	
//public double numForAvgOverAllStructures() {
//	return m_CountForAverageOverAllStructures;
//}
//public void setSamplingProbability(double probability) {
//	m_SamplingProbability = probability;
//}
//public ArrayList<Double> getSampleValues(){
//	return m_SamplesForVariance;
//}

  
  public abstract void putInKnownValues(Bond bond, double value);
  public abstract Double getFromKnownValues(Bond bond);
  public abstract Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis);
  public abstract void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value);
  public abstract double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException;
  public abstract double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException;
  public abstract Node copy();
  public abstract String getNodeType();
  public abstract Integer getComplexity();
  public abstract ArrayList<String> getExpression(boolean symbolic);
  public abstract String getSymbol();
  public abstract Node getLeft();
  public abstract Node getRight();

   @Override
   public Node generateNode(){
	   String type = this.getNodeType();
       switch (type) {
           case "Distance":  
        	   		return new DistanceNode();
           case "Constant":
			   		return new ConstantNode(Node.generateRandomValue());
           case "Addition": 
			   		return new AdditionNode(null,null);
           case "Subtraction":
			   		return new SubtractionNode(null,null);
           case "Multiplication":
			   		return new MultiplicationNode(null,null);
           case "Division": 
           			return new DivisionNode(null,null);
           case "Power": 
           			return new PowerNode(null,null);
           case "SiteSum": 
      			return new SiteSumNode(null,GENERATOR.nextBoolean());
           default:
                    return null;
       }
   }
	  public class InfoHash {
		    
		  	private Structure.Site m_Site;
		    private Structure.Site m_DefiningSite;
		    private Integer m_Axis;
		    
		    public InfoHash(Structure.Site site, Structure.Site definingSite, Integer axis) {
		    	m_Site = site;
		    	m_DefiningSite = definingSite;
		    	m_Axis = axis;
		    }
		    
		    @Override
		    public boolean equals(Object o) {
		        if (this == o) return true;
		        if (!(o instanceof InfoHash)) return false;
		        InfoHash infoHash = (InfoHash) o;
		        
		        if(m_Site != null){
		        	return (infoHash.m_Site == m_Site) && (infoHash.m_DefiningSite == m_DefiningSite) && (infoHash.m_Axis == m_Axis);
		        }
		        
	        	return (infoHash.m_DefiningSite == m_DefiningSite) && (infoHash.m_Axis == m_Axis);
		    }

		    @Override
		    public int hashCode() {
		    	int num = Objects.hash(m_Site, m_DefiningSite, m_Axis);
		        return num;
		    }
		  }
}