/*
 * Created on Aug 10, 2016
 *
 */
package potentialModelsGP.tree.neighbors;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet;
import evaluator.StructureNet.Bond;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.Node;

public class SiteProductNode extends Node {
  
  private boolean m_UseFirstSite;
  
  public SiteProductNode() {
    super(new Node[0]);
  }
  public SiteProductNode(Node childNode, boolean useFirstSite) {
    
    super(new Node[] {childNode});
    m_UseFirstSite = useFirstSite;
    
  }

  public double getValue(StructureNet.Site site, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
    int numNeighbors = site.numNeighbors();
    double returnValue = 1;
    for (int neighborNum = 0; neighborNum < numNeighbors; neighborNum++) {
    	returnValue *= this.getChildNode(0).getValue(site.getBond(neighborNum), definingSite, axis, daxis);
    }
    return returnValue;
    
  }
  
  public String toString() {
    
    return " productOverNeighbors(" + this.getChildNode(0).toString() + ")";
    
  }

  @Override
  public Node generateNode(Node childNode) {
    return new SiteProductNode(childNode, GENERATOR.nextBoolean());
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	return m_UseFirstSite ? this.getValue(bond.getCentralSiteOfBond(), definingSite, axis, daxis) : this.getValue(bond.getNeighborSiteOfBond(), definingSite, axis, daxis);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {

//	Product is the product of all ai from i = 1 to i = n = numNeighbors
//	d(Product)/dvar = d(a1*a2*a3*...*an)/dvar. Where var stands for variable
//	= (Product/a1)*da1/dvar + (Product/a2)*da2/dvar +...+(Product/an)*dan/dvar
	  
	StructureNet.Site site = m_UseFirstSite ? bond.getCentralSiteOfBond() : bond.getNeighborSiteOfBond();
	int numNeighbors = site.numNeighbors();
	double returnValue = 0;
	double siteProduct = this.getValue(site, definingSite, axis, null);
	
    for (int neighborNum = 0; neighborNum < numNeighbors; neighborNum++) {
    	returnValue += (siteProduct/this.getChildNode(0).getValue(site.getBond(neighborNum), definingSite, axis, null))*
    			this.getChildNode(0).getDerivative(site.getBond(neighborNum), variable, definingSite, axis);
    }
    return returnValue;
  }
  
  @Override
  public Node copy() {
	  Node copy = new SiteProductNode(this.getChildNode(0), m_UseFirstSite);
		copy.setValues(this.getNumTimesSelected());
	    return copy;	        
  }

  @Override
  public String getNodeType(){
	  return "SiteProduct";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
 	  ArrayList<String> aL = new ArrayList<String>();
 	  aL.add(0,"(SiteProduct(");
 	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
 	  aL.add(aL.size(),"))");
 	  return aL;
  }
  @Override
  public Node getLeft(){
	  return this.getChildNode(0);
  }
  @Override
  public Node getRight(){
	  return null;
  }
  @Override
  public Double getFromKnownValues(StructureNet.Bond bond){
// 	 Checking this is faster than not checking
 	  if(m_UseFirstSite){return this.getKnownValues().get(bond.getCentralSiteOfBond().getDefiningSite());}
 	  return this.getKnownValues().get(bond.getNeighborSiteOfBond().getDefiningSite());
 	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
// 	 It is faster to cache the values and check for the opposite node
	  if(m_UseFirstSite){this.getKnownValues().put(bond.getCentralSiteOfBond().getDefiningSite(),value);}
	  else{this.getKnownValues().put(bond.getNeighborSiteOfBond().getDefiningSite(),value);} 	
	  
// 	 SitePair sitePair = new SitePair(site1, site2);
// 	  this.getKnownValues().put(sitePair,value);
 	 
 	  }
@Override
public Node generateNode(Node child0, Node child1) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
	// TODO Auto-generated method stub
	
}
@Override
public String getSymbol() {
	return "SiteProduct";
}
}
