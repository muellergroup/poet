/*
 * Created on Aug 28, 2016
 *
 */

package potentialModelsGP.tree.neighbors;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet;
import evaluator.StructureNet.Bond;
import evaluator.StructureNet.Site;
import matsci.structure.Structure;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.Node;

public class GeneralizedMeanNode extends Node {
  
  private boolean m_UseFirstSite;
  
  public GeneralizedMeanNode() {
    super(new Node[0]);
  }  
  //TODO Originally was protected instead of public, so check.
  public GeneralizedMeanNode(Node neighborsNode, Node sitePowerNode, Node sumPowerNode, boolean useFirstSite) {
    
    super(new Node[] {neighborsNode, sitePowerNode, sumPowerNode});
    m_UseFirstSite = useFirstSite;
    
  }
  
  public Node getNeighborsNode() {
    return this.getChildNode(0);
  }
  
  public Node getSitePowersNode() {
    return this.getChildNode(1);
  }
  
  public Node getSumPowerNode() {
    return this.getChildNode(2);
  }

  public double getValue(Site site, Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
    
	//Generalized mean = power mean = (sum(x_i ^ q_i))^(1.0/p) but we define 1/p as the sumPowerSiteNode
	// = [Sum(neighborNode_i^sitePowerNode_i)/numNeighbors]^sumPowerSiteNode
	//Thus, sumPowerNode = 1/p

	int numNeighbors = site.numNeighbors();
    double returnValue = 0;
    for (int neighborNum = 0; neighborNum < numNeighbors; neighborNum++) {
    	StructureNet.Bond bond = site.getBond(neighborNum);
    	returnValue += Math.pow(this.getNeighborsNode().getValue(bond, definingSite, axis, daxis), this.getSitePowersNode()
    		  .getValue(bond, definingSite, axis, daxis));
    }
	StructureNet.Bond bond = site.getBond(-1);
    return Math.pow(returnValue/numNeighbors, this.getSumPowerNode().getValue(bond, definingSite, axis, daxis));
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	  return m_UseFirstSite ? this.getValue(bond.getCentralSiteOfBond(), definingSite, axis, daxis) : this.getValue(bond.getNeighborSiteOfBond(), definingSite, axis, daxis);
  }

  @Override
  public Node generateNode(Node childNode) {
    
    boolean useFirst = GENERATOR.nextBoolean();
    double p = 1 - GENERATOR.nextDouble(); // Ensures we don't get zero.
    ConstantNode pNode1 = new ConstantNode(p);
    ConstantNode pNode2 = new ConstantNode(p);
    return new GeneralizedMeanNode(childNode, pNode1, pNode2, useFirst);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    
    //
    // d/dvar (sum(x_i ^ q_i))^(1.0/p) but we define 1/p as the sumPowerSiteNode
    // 
	  
	StructureNet.Site site = m_UseFirstSite ? bond.getCentralSiteOfBond() : bond.getNeighborSiteOfBond();
	int numNeighbors = site.numNeighbors();
	
	double sum1 = 0;
	double sum2 = 0;
	double oneOverp = this.getSumPowerNode().getValue(bond, definingSite, axis, null); 
	double oneOverpPrime = this.getSumPowerNode().getDerivative(bond, variable, definingSite, axis);
	
	for(int neighborNum = 0; neighborNum <numNeighbors;neighborNum ++){
		
		double x = this.getNeighborsNode().getValue(site.getBond(neighborNum), definingSite, axis, null);
		double q = this.getSitePowersNode().getValue(site.getBond(neighborNum), definingSite, axis, null);
		double xPrime = this.getNeighborsNode().getDerivative(site.getBond(neighborNum), variable, definingSite, axis);
		double qPrime = this.getSitePowersNode().getDerivative(site.getBond(neighborNum), variable, definingSite, axis);
		
		sum1 += Math.pow(x, q);
		sum2 += Math.pow(x, q)*((q/x)*xPrime+Math.log(x)*qPrime);
	}
	return Math.pow(sum1/numNeighbors, oneOverp)*(oneOverp*sum2/sum1+oneOverpPrime*Math.log(sum1/numNeighbors));
	
  }

  @Override
  public Node copy() {
	  Node copy = new GeneralizedMeanNode(this.getNeighborsNode(), this.getSitePowersNode(), this.getSumPowerNode(), m_UseFirstSite);
		copy.setValues(this.getNumTimesSelected());
	    return copy;	        
  }
  @Override
  public String getNodeType(){
	  return "GeneralizedMean";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){//TODO fix by adding power nodes
 	  ArrayList<String> aL = new ArrayList<String>();
 	  aL.add(0,"(GeneralizedMean(");
 	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
 	  aL.add(aL.size(),"))");
 	  return aL;
  }
  
//  TODO this should be adapted for Generalized mean node
  @Override
  public Node getLeft(){
	  return null;
  }
  @Override
  public Node getRight(){
	  return null;
  }
  
  @SuppressWarnings("unlikely-arg-type")
@Override
  public Double getFromKnownValues(Bond bond){
// 	 Checking this is faster than not checking
 	  if(m_UseFirstSite){return this.getKnownValues().get(bond.getCentralSiteOfBond());}
 	  return this.getKnownValues().get(bond.getNeighborSiteOfBond());
 	 
 	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
// 	 It is faster to cache the values and check for the opposite node
	  if(m_UseFirstSite){this.getKnownValues().put(bond.getCentralSiteOfBond().getDefiningSite(),value);}
	  else{this.getKnownValues().put(bond.getNeighborSiteOfBond().getDefiningSite(),value);} 	
 	   
 	  }
@Override
public Node generateNode(Node child0, Node child1) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
	// TODO Auto-generated method stub
	
}
@Override
public String getSymbol() {
	return "GeneralizedMean";
}
  
}
