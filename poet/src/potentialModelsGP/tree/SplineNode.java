/*
 * Created on May 22, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;

public class SplineNode extends Node {
  
  public SplineNode() {
    super(new Node[0]);
  }
  
  public SplineNode(Node child1, Node child2) {
    super (new Node[] {child1, child2});
  }
  
  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
    return 0;
  }

  @Override
  public Node generateNode(Node childNode) {
    Node newNode = new ConstantNode(Node.generateRandomValue());//0
    return new SplineNode(childNode, newNode);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
	    return 0;
  }

  @Override
  public Node copy() {
	  	Node copy = new SplineNode(this.getChildNode(0), this.getChildNode(1));
	      copy.setValues(this.getNumTimesSelected());
	  	return copy;	
  }
  
  @Override
  public String getNodeType(){
	  return "Spline";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  ArrayList<String> aL = new ArrayList<String>();
	  aL.add(0,"((");
	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
	  aL.add(aL.size(),")+(");
	  aL.addAll(aL.size(),this.getChildNode(1).getExpression(symbolic));
	  aL.add(aL.size(),"))");
	  return aL;
  }
  @Override
  public Node getLeft(){
	  return this.getChildNode(0);
  }
  @Override
  public Node getRight(){
	  return this.getChildNode(1);
  }
  @Override
  public Double getFromKnownValues(Bond bond){
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
    return new SplineNode(child0, child1);
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
	// TODO Auto-generated method stub
	
}
@Override
public String getSymbol() {
	return "spline";
}
}
