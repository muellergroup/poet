/*
 * Created on Aug 29, 2016
 *
 */
package potentialModelsGP.tree;

public interface INodeFactory {
  public Node generateNode(Node childNode);
  public Node generateNode();
  public Node generateNode(Node child0, Node child1);
}