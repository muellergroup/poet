/*
 * Created on Aug 10, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;
import java.util.Random;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;
public class PowerNode extends Node {
//	private Boolean m_PositiveBase = null;
	private static final Random GENERATOR = new Random();
	
  public PowerNode() {
    super(new Node[0]);
  }
  
  public PowerNode(Node base, Node power) {
    super(new Node[] {base, power});
  }
  
  public Node getBaseNode() {
    return this.getChildNode(0);
  }
  
  public Node getPowerNode() {
    return this.getChildNode(1);
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	double base = m_ChildNodes[0].getValue(bond, definingSite, axis, daxis);
	double power = m_ChildNodes[1].getValue(bond, definingSite, axis, daxis);
		
////  Protected powerNode. In case power < 0 or in case power == 0.
////  Also, the derivative of powerNode uses natural log of base
//    if(base == 0.0) {return 0.0;}
//    
////  Because the derivative of powerNode uses natural log of base
//	if(base < 0){
//		return -Math.pow(-base, power);
//	}

    return Math.pow(base, power);
  }

  @Override
  public Node generateNode(Node childNode) {
	if(GENERATOR.nextBoolean()){
		double value = PowerNode.generateRandomValuePower();
	    ConstantNode randomConstantNode = new ConstantNode(value);
	    return new PowerNode(childNode, randomConstantNode);
	}
	else{
		double value = Node.generateRandomValue();
	    ConstantNode randomConstantNode = new ConstantNode(value);
	    return new PowerNode(randomConstantNode, childNode);
	}
  }

  private static double generateRandomValuePower() {
	  	
	  	double maxValue = 20.0;
	  	
	  	double value;
	  	do {
	  		value = GENERATOR.nextDouble()*maxValue;
			if(GENERATOR.nextDouble() < 0.8){//Approximately 90% of values are integers
				value = ((int) value) + 1;
			}
	  		boolean negative = GENERATOR.nextBoolean();
	  		if(negative) {
	  			value *= -1;
	  		}
	  	}while(value == 0.0 || value == 1.0);
	  	
		return value;

}

@Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    // From https://www.physicsforums.com/threads/derivative-of-a-function-to-a-function.191892/
    // g(x) = h(x)^k(x) then g'(x) = g(x) [k(x)h'(x)/h(x) + Log(h(x))k'(x)]
	  
//	m_ChildNodes[0] is the base node and m_ChildNodes[1] is the power node
    double h = m_ChildNodes[0].getValue(bond, definingSite, axis, null);
    double k = m_ChildNodes[1].getValue(bond, definingSite, axis, null);

////  Protected powerNode. In case power < 0 or in case power == 0.
////  Also, the derivative of powerNode uses natural log of base
//    if(h == 0.0) {return 0.0;}
    
    double hPrime = m_ChildNodes[0].getDerivative(bond, variable, definingSite, axis);
    double kPrime = m_ChildNodes[1].getDerivative(bond, variable, definingSite, axis);
    
////  Because the derivative of powerNode uses natural log of base
//	if(h < 0){
//		return -Math.pow(-h, k)*(k*hPrime/h+Math.log(-h)*kPrime);
//	}
    return Math.pow(h, k)*(k*hPrime/h+Math.log(h)*kPrime);
    
  }

  @Override
  public Node copy() {   
	  Node copy = new PowerNode(m_ChildNodes[0], m_ChildNodes[1]);
      copy.setValues(this.getNumTimesSelected());
	  return copy;
  }

  @Override
  public String getNodeType(){
	  return "Power";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  boolean positiveBase = true;
	  ArrayList<String> aL = new ArrayList<String>();
	  if(positiveBase){
		  aL.add(0,"((");
	  }
	  else{
		  aL.add(0,"((-1.0)*(((((-1.0)*(");
	  }
	  aL.addAll(1,m_ChildNodes[0].getExpression(symbolic));
	  if(positiveBase){
		  aL.add(aL.size(),")^(");
	  }
	  else{
		  aL.add(aL.size(),")))^(");
	  }
	  aL.addAll(aL.size(),m_ChildNodes[1].getExpression(symbolic));
	  if(positiveBase){
		  aL.add(aL.size(),"))");
	  }
	  else{
		  aL.add(aL.size(),"))))");
	  }
	  return aL;
  }
  
  @Override
  public Node getLeft(){
	  return m_ChildNodes[0];
  }
  @Override
  public Node getRight(){
	  return m_ChildNodes[1];
  }
  @Override
  public Double getFromKnownValues(Bond bond){
//	  It is faster to calculate this value than to cache it
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
    return new PowerNode(child0, child1);
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
}
@Override
public String getSymbol() {
	return "^";
}
}
