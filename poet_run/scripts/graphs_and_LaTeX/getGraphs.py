#!/usr/bin/python

#Save images of the LaTeX and graph representations of the models in every snapshots in a directory
#Arguments:
#  The absolute path of the directory that contains the snapshots

import matplotlib.pyplot as plt
import numpy as np
import os
from graphviz import Source
from PIL import Image
import glob
import sys

dirSnapshots = sys.argv[1] + "\\"

for snapshot_data in glob.glob(dirSnapshots+"*_data"):
   
   dirResults = dirSnapshots + os.path.basename(snapshot_data) +"\\"
   baseSnapshot = os.path.basename(snapshot_data)
   snapshotName = baseSnapshot.split("_")[0] + "_"
   
   for file in glob.glob(dirResults+"*.dot"):
      
      base=os.path.basename(file)
      modelNum=int(os.path.splitext(base)[0])      
         
      #Import a DOT file and save the graph                                                                                                        
      dotFile = np.genfromtxt(dirResults+str(modelNum)+".dot",delimiter='          ',dtype=None, names=['expressions'],encoding=None)
      var=""
      for x in range(0,len(dotFile)):
         var += dotFile['expressions'][x]+"\n"
      s = Source(var, format="png")
      s.render(filename=snapshotName+str(modelNum)+"_graph",directory=dirResults,cleanup=True)
      
      #Save the LaTeX expression as an image
      laTexEquations = np.genfromtxt(dirResults+'\\LaTeX.txt',delimiter='  ',dtype=None, names=['expressions'],encoding=None)
      expression=laTexEquations['expressions'][modelNum-1]   
      fig = plt.figure()
      fig.set_size_inches([1,1])
      fig.text(0.5, 0.5,expression, fontsize=12,horizontalalignment='center',verticalalignment='center')
      fig.savefig(dirResults+snapshotName+str(modelNum)+"_expression.png", dpi=None, bbox_inches='tight', pad_inches=0.1)

##      #Paste LaTeX to graph
##      modelLaTeX = Image.open(dirResults+str(modelNum)+"_expression.png", 'r')
##      modelLaTeX_w, modelLaTeX_h = modelLaTeX.size
##      modelGraph = Image.open(dirResults+str(modelNum)+"_graph.png", 'r')
##      modelGraph_w, modelGraph_h = modelGraph.size
##      modelGraph.paste(modelLaTeX)
##      modelGraph.save(dirResults+str(modelNum)+".png")
##   
##      os.remove(dirResults+str(modelNum)+"_expression.png")
##      os.remove(dirResults+str(modelNum)+"_graph.png")
