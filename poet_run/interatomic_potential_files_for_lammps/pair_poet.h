#ifdef PAIR_CLASS

PairStyle(poet,PairPOET)

#else

#ifndef LMP_PAIR_POET_H
#define LMP_PAIR_POET_H

#include <cstdio>
#include "pair.h"

namespace LAMMPS_NS {


class PairPOET : public Pair {
 public:
  // public variables so USER-ATC package can access them

  double cutmax;

  // potentials as array data

  int nrho,nr;
  int nfrho,nrhor,nz2r,nz3r;
  double **frho,**rhor,**z2r,**z3r;
  int *type2frho,**type2rhor,**type2z2r,**type2z3r;

  // potentials in spline form used for force computation

  double dr,rdr,drho,rdrho,rhomax,c1,c2;
  double ***rhor_spline,***frho_spline,***z2r_spline,***z3r_spline;

  PairPOET(class LAMMPS *);
  virtual ~PairPOET();
  virtual void compute(int, int);
  void settings(int, char **);
  virtual void coeff(int, char **);
  void init_style();
  double init_one(int, int);
  double single(int, int, int, int, double, double, double, double &);
  virtual void *extract(const char *, int &);

  virtual int pack_forward_comm(int, int *, double *, int, int *);
  virtual void unpack_forward_comm(int, int, double *);
  int pack_reverse_comm(int, int, double *);
  void unpack_reverse_comm(int, int *, double *);
  double memory_usage();
  void swap_POET(double *, double **);

 protected:
  int nmax;                   // allocated size of per-atom arrays
  double cutforcesq;
  double **scale;

  // per-atom arrays

  double *rho,*fp,*embed,*phi3f;

  // potentials as file data

  int *map;                   // which element each atom type maps to

  struct Funcfl {
    char *file;
    int nrho,nr;
    double drho,dr,cut,mass,c1,c2;
    double *frho,*rhor,*zr;
  };
  Funcfl *funcfl;
  int nfuncfl;

  struct Setfl {
    char **elements;
    int nelements,nrho,nr;
    double drho,dr,cut,c1,c2;
    double *mass;
    double **frho,**rhor,***z2r,***z3r;
  };
  Setfl *setfl;

  struct Fs {
    char **elements;
    int nelements,nrho,nr;
    double drho,dr,cut,c1,c2;
    double *mass;
    double **frho,***rhor,***z2r,***z3r;
  };
  Fs *fs;

  virtual void allocate();
  virtual void array2spline();
  void interpolate(int, double, double *, double **);
  void grab(FILE *, int, double *);

  virtual void read_file(char *);
  virtual void file2array();
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Incorrect args for pair coefficients

Self-explanatory.  Check the input script or data file.

E: Cannot open POET potential file %s

The specified POET potential file cannot be opened.  Check that the
path and name are correct.

E: Invalid POET potential file

UNDOCUMENTED

*/
