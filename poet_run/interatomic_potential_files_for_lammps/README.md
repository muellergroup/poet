# Using GP1, GP2, GP3, GP1-c, GP2-c, GP3-c, SC4-c, SC5-c, and LB-c in LAMMPS

The interatomic potential files in this supplementary material with extension .eam or .eam.alloy, and names containing GP1, GPn, LB, and SC can be used under the “pair_style eam/alloy” in LAMMPS. The following is an example input specification:
* pair_style eam/alloy
* pair_coeff * * Cu_GP1.eam Cu
	
The interatomic potential files in this supplementary material with extension .poet, and names containing GP2 and GP3 use “pair_style poet”. The following is an example input specification:
* pair_style poet
* pair_coeff * * Cu_GP2.poet Cu
	
The poet pair_style can be compiled in LAMMPS following these steps:
1. Copy the files “pair_poet.cpp” and “pair_poet.h” (available in the Supplementary Information) to <lammps_main_directory>/src/MANYBODY and edit the file <lammps_main_directory>/src/Makefile.list by adding “pair_poet.cpp” and “pair_poet.h” to the end of the respective lines
2. make LAMMPS by including the “yes-manybody” flag


