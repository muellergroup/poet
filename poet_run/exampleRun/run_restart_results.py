'''
Created on April 28, 2020

@author: Alberto Hernandez
'''

import time
import subprocess
import os
from pathlib import Path
import sys
from glob import iglob
import natsort
import shutil
from scipy.spatial import ConvexHull
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from sklearn.metrics import mean_squared_error
from os import path
import pandas as pd
from sympy.parsing.sympy_parser import parse_expr
from matplotlib.backends.backend_pdf import PdfPages
from decimal import Decimal
from matplotlib.ticker import (MultipleLocator)
from sklearn.metrics import mean_absolute_error
import seaborn as sns
import matplotlib.cbook 
import warnings 

warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)
sns.set()
sns.set_style("ticks")
sns.set_palette("colorblind")

operators_list = ["+", "-", "*", "/", "^"]
functions_list = ["T", "F"]
descriptors_list = ["r"]

def getXY(fileNameFactor,propertyName,dataSetName):
    """
    Get target and predicted values
    
    Args:
        fileNameFactor (str): path of the directory that contains raw data up to the ID of the model
        propertyName (str): Energy, Force or Stress
        dataSetName (str): Training or Validation
        
    Returns:
        two dimensional array with target and predicted values
    """
    
    values0 = np.genfromtxt(fileNameFactor+propertyName+dataSetName+".data",delimiter=' ', names=['xaxis', 'y0axis','y1axis'])
    y = values0['y0axis']
    x = values0['y1axis']
    return [x,y]

def getTickFrequency(end,start):
    if(end-start < 10): 
        freqMajor = 1
        freqMinor = 0.2
    elif(end-start < 100): 
        freqMajor = 5
        freqMinor = 1
    elif(end-start < 1000): 
        freqMajor = 100
        freqMinor = 20
    elif(end-start < 5000): 
        freqMajor = 1000
        freqMinor = 200
    elif(end-start < 12000): 
        freqMajor = 2000
        freqMinor = 500
    else: 
        freqMajor = 5000
        freqMinor = 500
    return [freqMajor, freqMinor]

def plotElasticConstants(convexHull1, pp):
    elasticConstants = convexHull1[["modelID","C11_FCC","C12_FCC","C44_FCC"]]
    plot1 = elasticConstants.plot.bar(x="modelID",rot=0)
    fig1 = plot1.get_figure()
    fig1.suptitle("Errors on elastic constants", fontsize=12)
    ax = fig1.add_subplot(1,1,1)
    ax.set_ylabel("% error")
    
    pp.savefig(fig1,orientation='landscape')
    plt.close()

def plotSurfaceEnergies(convexHull1, pp):
    surfNames = ["modelID"]
    for colNamei in convexHull1.columns:
        if("Esurf_" in colNamei):
            surfNames.append(colNamei)
    surfaceEnergy = convexHull1[surfNames]
    surfaceEnergy = surfaceEnergy.rename(columns = lambda x: x.split("_")[1] if x in surfNames[1::] else x)
    fig = plt.figure()
    for indexrow in surfaceEnergy.iterrows():
        errors = indexrow[1][surfaceEnergy.columns[1::]]
        ax = errors.plot(marker='o', markersize=4,label=str(int(indexrow[1]["modelID"])))
        ax.set_ylabel("% error")    
        ax.set_xticks(np.arange(len(surfaceEnergy.columns[1::])))
        ax.set_xticklabels(surfaceEnergy.columns[1::],rotation=90)
        ax.legend()
        fig.add_axes(ax)
    fig.suptitle("Errors on surface energies", fontsize=12)
    pp.savefig(fig,orientation='landscape')
    plt.close()

def plotProperty(propertyName, subPlotNum,fig,fileNameFactor):
    fsaxeslabels=12
    fserrors=fsaxeslabels
    ticklabelsize=fsaxeslabels
    markersize1=4
    linewidth1=1
    
    y1x1 = getXY(fileNameFactor,propertyName,"Training")
    y1 = y1x1[1]
    x1 = y1x1[0]
    
    yx = getXY(fileNameFactor,propertyName,"Validation")
    y = yx[1]
    x = yx[0]
    
    if(propertyName == 'Energy' or propertyName == 'Force' ):
        x = np.multiply(x,1000.0)
        y = np.multiply(y,1000.0)
        x1 = np.multiply(x1,1000.0)
        y1 = np.multiply(y1,1000.0)
        
    xx1 = np.append(x, x1)
    
    mae = mean_absolute_error(x,y)  
    mae1 = mean_absolute_error(x1,y1)  
    
    ax1 = fig.add_subplot(3,2,subPlotNum)
    
    ax1.plot(x1 , y1,'s', markersize=markersize1, color='orange', label="Training")
    ax1.plot(x , y,'o', markersize=markersize1, color='blue', label="Validation")
    ax1.legend()
    ax1.plot(sorted(xx1,key=float), sorted(xx1,key=float),'--', linewidth=linewidth1,color='black')
    
    if propertyName == "Energy":
        units = r' ($meV / atom$)'
    elif propertyName == "Force":
        units = r' ($meV / \AA$)'
    elif propertyName == "Stress":
        units = r' ($GPa$)'
    
    
    ax1.set_xlabel("Target " +propertyName+units,fontsize=fsaxeslabels)            
    ax1.set_ylabel('Predicted '+propertyName+units,fontsize=fsaxeslabels)
    
    ax1.set_title("MAE: "+('%.3g' % Decimal(mae1))+", "+('%.3g' % Decimal(mae))+units, fontsize=fserrors)
    #         ax1.set_title("MAE: "+('{0:0.2g}'.format(mae1))+", "+('{0:0.2g}'.format(mae))+units, fontsize=fserrors)
    
#     ax1.minorticks_on()
    ax1.tick_params(axis='both', which="both", labelsize=ticklabelsize, direction="in")
    
#     # Make a plot with major ticks that are multiples of 20 and minor ticks that
#     # are multiples of 5.  Label major ticks with '%d' formatting but don't label
#     # minor ticks.
#     start,end = ax1.get_xlim()
#     majorMinor = getTickFrequency(end,start)
#     freqMajor = majorMinor[0]
#     freqMinor = majorMinor[1]
#     
#     start,end = ax1.get_ylim()
#     majorMinor = getTickFrequency(end,start)
#     yfreqMajor = majorMinor[0]
#     yfreqMinor = majorMinor[1]
#                 
#     ax1.xaxis.set_major_locator(MultipleLocator(freqMajor))
#     #         ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))
#     # For the minor ticks, use no labels; default NullFormatter.
#     ax1.xaxis.set_minor_locator(MultipleLocator(freqMinor))
#     
#     ax1.yaxis.set_major_locator(MultipleLocator(yfreqMajor))
#     #         ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))
#     # For the minor ticks, use no labels; default NullFormatter.
#     ax1.yaxis.set_minor_locator(MultipleLocator(yfreqMinor))
            
            
def createReport(pathArchivedName,runName):
    """
    Create a PDF document with plots of predicted vs target values, and, if available, elastic constants, and surface energies
    
    Args:
        pathToResultsDir (str): fullPath of the directory that contains the results
        runName (str): name of the experiment
    
    """

    dirSnapshot = pathArchivedName
                
    pp = PdfPages(dirSnapshot+"/"+runName+"_convexHull.pdf")
    
    convexHull1 = pd.read_csv(dirSnapshot+"/"+runName+"_convexHull.csv")
#     if("max_abs_percent_error_elastic_constants" in convexHull1):
#         convexHull1 = convexHull1[convexHull1["max_abs_percent_error_elastic_constants"] < 80]
#     if("mean_abs_percent_error_surface_energies" in convexHull1):
#         convexHull1 = convexHull1[convexHull1["mean_abs_percent_error_surface_energies"] < 80]
    if("max_abs_percent_error_elastic_constants" in convexHull1):
        plotElasticConstants(convexHull1, pp)
    if("mean_abs_percent_error_surface_energies" in convexHull1):
        plotSurfaceEnergies(convexHull1,pp)

    for indexrow in convexHull1.iterrows():
            
        fig = plt.figure()
        fig.set_size_inches([8.5,11])
        fssuptitle=11
        
        fig.suptitle("ID: "+str(indexrow[1]["modelID"])+", "+ indexrow[1]["expression"], fontsize=fssuptitle)
    
        fileNameFactor = dirSnapshot+"\\"+str(indexrow[1]["modelID"])
        
        with open(fileNameFactor+"errorMetricsTraining.data") as f:
            lines = f.readlines()
        propertiesList = []
        [propertiesList.append(linei.split()[2]) for linei in lines]
            
        subPlotNum = 0
        
        for propertyName in propertiesList:
            subPlotNum += 1
            plotProperty(propertyName, subPlotNum, fig, fileNameFactor)
                    
        fig.tight_layout(rect=[0, 0, 1, 0.95])#rect[left,bottom,right,up]
        pp.savefig(fig,orientation='landscape')
        plt.close()
    
    pp.close()
    
def isOperator(some_string):
    if(some_string in operators_list):
        return True
    return False
def numSiteSums(str_expression):
    str_expression = str_expression.replace("^", "**")
    str_expression = str_expression.replace("[", "(")
    str_expression = str_expression.replace("]", ")")
    expression = parse_expr(str_expression)
    str_expression = str(expression).replace("**", "^")
    return str_expression.count("T") + str_expression.count("F")

def fixMinusConstantPower(str_expression):
    str_expression = str_expression.replace("**", "^")
    
    new_str_expression = []
    list_str_expression = list(str_expression)
    i = 0
    while(i < len(list_str_expression)):
        new_str_expression.append(list_str_expression[i])
        if(list_str_expression[i].isdigit() and i > 0):
            if(list_str_expression[i-1] == "-"):
                indexMinus = i-1
                #Go to the end of the number
                while(list_str_expression[i].isdigit() or list_str_expression[i] == "."):
                    i += 1
                    new_str_expression.append(list_str_expression[i])
                    if(i >= len(list_str_expression)):
                        break
                if(list_str_expression[i] == "^" and (list_str_expression[i+1] == "r" or list_str_expression[i+1] == "(")):
                    new_str_expression[indexMinus] = "-1.0*"
        i += 1
    return_str_expression = ""
    for s in new_str_expression:
        return_str_expression += s
    return_str_expression = return_str_expression.replace("^", "**")
    return return_str_expression

def numNodes(str_expression):
    str_expression = str_expression.replace("^", "**")
    str_expression = str_expression.replace("[", "(")
    str_expression = str_expression.replace("]", ")")
    str_expression = str_expression.replace("E", "2.718281828459045235")
    str_expression = str_expression.replace("e", "*10.0**")
    str_expression = fixMinusConstantPower(str_expression)    
        
    expression = parse_expr(str_expression)
    str_expression = str(expression).replace("**", "^")
    str_expression = str_expression.replace("e", "*10.0^")

    
    list_str_expression = list(str_expression)
    i = 0
    countNum = 0
    countMinusAsNum = 0
    size = str_expression.count("+") + str_expression.count("-") + str_expression.count("*") + str_expression.count("/") + str_expression.count("^") + str_expression.count("r") + str_expression.count("T") + str_expression.count("F")
    
    while(i < len(list_str_expression)):
        if(list_str_expression[i].isdigit() and i > 0):
            if(list_str_expression[i-1] == "-"):
                if(i-1 == 0):
                    # means that the minus sign should not be counted as an operator
                    countMinusAsNum += 1
                elif(isOperator(list_str_expression[i-2]) or list_str_expression[i-2] == "("):
                    # means that the minus sign should not be counted as an operator
                    countMinusAsNum += 1
            countNum += 1
            while(list_str_expression[i].isdigit() or list_str_expression[i] == "."):
                i += 1
                if(i >= len(list_str_expression)):
                    break
        elif(list_str_expression[i] == "-" and not list_str_expression[i+1].isdigit()):
            # minus sign before the first element of the list (if it's not a number) should be represented as -1*x instead of -x
            if(i == 0 and (list_str_expression[i+1] in functions_list or list_str_expression[i+1] in descriptors_list)):
                countNum += 1
            #minus sign before a non-constant where i-1 == "(" should be represented as -1*x instead of -x
            elif(list_str_expression[i-1] == "("):
                countNum += 1 
            i += 1
        else:
            i += 1
    
    size += countNum - countMinusAsNum
    return size

def getNumNodesNumSums(fullParentDirName,fullSimplifScriptName):
    simplifExpressions = []
    simplifExpressionsLaTeX = []
    for i in [1,2]:
        subprocess.check_output(["wolframscript",fullSimplifScriptName, fullParentDirName , str(i)])
        filepath = fullParentDirName+"/simplifiedexpressions."+str(i)+".txt"
        filepath2 = fullParentDirName+"/simplifiedexpressionsLaTeX."+str(i)+".txt"
        with open(filepath) as f:
            lines = f.readlines()
        with open(filepath2) as f2:
            lines2 = f2.readlines()
        simplifExpressions_i = []
        [simplifExpressions_i.append([numNodes(x), numSiteSums(x)]) for x in lines]
        simplifExpressionsLaTex_i = {}
        for x in range(0,len(lines2)):
            simplifExpressionsLaTex_i[x] = lines2[x]
        simplifExpressions.append(simplifExpressions_i)
        simplifExpressionsLaTeX.append(simplifExpressionsLaTex_i)
        subprocess.check_output(["rm", filepath])
        subprocess.check_output(["rm", filepath2])

    content1, content2 = simplifExpressions[0],simplifExpressions[1]

    numNodesNumSums = []
    latexExpressions = []
    for i in range(0,len(content1)):
        keep1 = int(content1[i][1]) < int(content2[i][1])
        if(keep1):
            latexExpressions.append(simplifExpressionsLaTeX[0][i])
            numNodesNumSums.append(content1[i])
        else:
            latexExpressions.append(simplifExpressionsLaTeX[1][i])
            numNodesNumSums.append(content2[i])
    with open(fullParentDirName+"/LaTeX.txt","w") as f:       
        for expr1 in latexExpressions:
            f.write(expr1)
            
    return numNodesNumSums

def isDominated(point,points):
    for p in points:
        b0 = (abs(p[0]-point[0]) < 10**-12)
        b1 = (abs(p[1]-point[1]) < 10**-12)
        b2 = (abs(p[2]-point[2]) < 10**-12)
        equalpoints = b0 and b1 and b2
        if(equalpoints):
            continue
        if((p[0] < point[0] or b0) and (p[1] < point[1] or b1) and (p[2] < point[2] or b2)):
            return True
    return False
def getParetoFrontier(points):
    nondominated = []
    for point in points:
        if(not isDominated(point, points)):
            nondominated.append(point)
    return nondominated

def getMaxValue(points):
    maxvalue = 10**-300
    for p in points:
        if(p[0] > maxvalue):
            maxvalue = p[0]
        if(p[1] > maxvalue):
            maxvalue = p[1]
        if(p[2] > maxvalue):
            maxvalue = p[2]
    return maxvalue

def getPointIndex(point,points):
    index = 0
    for p in points:
        if(abs(point[0]-p[0])<10**-12 and abs(point[1]-p[1])<10**-12 and abs(point[2]-p[2])<10**-12):
            return index
        index += 1
    return None

def getFitness(directoryPath,trainOrValidate):
    """
    Get the fitness of the model from the error metrics printed by POET
    
    Args:
        directoryPath (str): fullPath of the directory that contains the data of the experiment
        trainOrValidate (str): "Training" or "Validation"
    
    Returns:
        fitness with weights {"Energy":0.5, "Force":0.4, "Stress":0.1}
    """

    fitness = []
    count = 1
    weightsProperties = {"Energy":0.5, "Force":0.4, "Stress":0.1}
    
    while(path.exists(directoryPath+"/"+str(count)+"errorMetrics"+trainOrValidate+".data")): #Training
        modelfitness = 0
        for propertyKey in weightsProperties.keys():
            propertyData = np.genfromtxt(directoryPath+"/"+str(count)+propertyKey+trainOrValidate+".data",delimiter=' ', names=["num","predicted","target"])
            modelfitness += weightsProperties.get(propertyKey)*mean_squared_error(propertyData["predicted"],propertyData["target"])/np.var(propertyData["target"])
        fitness.append(1000*modelfitness)
        count = count + 1
    return fitness

def write_3D_convex_hull(runName, parentDir1, simplifScript, showPlot=False):
    """
    Write the 3D convex hull of models using complexity, speed and fitness.

    Args:
        runName (str): name of the experiment
        parentDir1 (str): fullPath of the directory that contains the data of the experiment
        simplifScript (str): full path of the Mathematica script to simplify the models
        showPlot (bool): Whether 3D scatter plot is shown in the screen.
            Defaults to False.

    Returns:
        True if completes successfully
    """
    
    numNodesNumSums_1 = getNumNodesNumSums(parentDir1,simplifScript)

    df_nodes_sums = pd.DataFrame(numNodesNumSums_1)
    df_nodes_sums.columns = ["num_nodes","num_sums"]
        
    df1 = pd.read_csv(parentDir1+'errorsallmodels.Validation.data', delim_whitespace=True)
    col_data = {}
    newCols = []
    for col in df1:
        name,metric,units_metric,standard_deviation_or_target,units_standard_deviation_or_target = col.split(",")
        newCols.append(name)
        col_data[name] = {"metric":metric,"units_metric":units_metric,"standard_deviation_or_target":standard_deviation_or_target,"units_standard_deviation_or_target":units_standard_deviation_or_target}
    
    df1.columns = newCols
    
#     print("Assuming same order of models in snapshot and errorsallmodels.Validation.data or errorsallmodels.Training.data")
    
    # Generate a file "3dConvexHull.data" that contains 3 columns: numNodes, numSums and fitness
    importedpoints = pd.DataFrame()
    importedpoints["num_nodes"] = df_nodes_sums["num_nodes"]
    importedpoints["num_sums"] = df_nodes_sums["num_sums"]
    importedpoints["fitness"] = df1["fitness"]
    df1["num_nodes"] = df_nodes_sums["num_nodes"]
    df1["num_sums"] = df_nodes_sums["num_sums"]
    
    points = []
    for indexrow in importedpoints.iterrows():
        points.append([indexrow[1]['num_nodes'], indexrow[1]['num_sums'],indexrow[1]["fitness"]])
        
    maxvalue = getMaxValue(points)
    maxpoint = [maxvalue, maxvalue, maxvalue]
    points.append(maxpoint)
    
    try:
        hull = ConvexHull(points)
    except:
        df1.to_csv(parentDir1+"/"+runName+"_convexHull.csv")
        return True
    
    vertices = []
    for v in hull.vertices:
        vertices.append(points[v])
     
    x1, y1, z1 = [], [], []
    paretofrontier = getParetoFrontier(vertices)
    for p in paretofrontier:
        x1.append(float(p[0]))
        y1.append(float(p[1]))
        z1.append(float(p[2]))
     
    del points[getPointIndex(maxpoint, points)]
    x2, y2, z2 = [], [], [] 
    for v in range(0, len(points)):
        x2.append(float(points[v][0]))
        y2.append(float(points[v][1]))
        z2.append(float(points[v][2]))
    
    isInHullArray = []
    for indexrow in importedpoints.iterrows():
        p = [indexrow[1]['num_nodes'], indexrow[1]['num_sums'],indexrow[1]["fitness"]]
        isInHull = False
        for ph in paretofrontier:
            if(abs(p[2]-ph[2]) < 10**-12):
                isInHull = True
                break
        isInHullArray.append(isInHull)
        
    df1["isInHull"] = isInHullArray
    
    #add the LaTeX equations to the DataFrame
    with open(parentDir1+"/LaTeX.txt") as f1:
        lines1 = f1.readlines()
    simplifiedExpressions = []
    [simplifiedExpressions.append(x.rstrip().replace("T","\Sigma")) for x in lines1]
    df1["expression"] = simplifiedExpressions
    modelIDs = [i+1 for i in range(0,len(simplifiedExpressions))]
    df1.insert(loc=0, column="modelID", value=modelIDs)
        
    df1= df1[df1["isInHull"]]
    df1 = df1.dropna(axis=1, how='all')
    df1 = df1.dropna()

    df1.to_csv(parentDir1+"/"+runName+"_convexHull.csv",index=False)
    
    if(showPlot):
        fig = plt.figure()
        ax1 = Axes3D(fig)
        ax1.scatter(x1,y1,z1,s=1000, c='r', marker="o", label='first')
        ax1.scatter(x2,y2,z2,s=100, c='b', marker="^", label='second')
        plt.show()
    
    return True

def getNumInstances(inputFile):
    # get number of instances
    for line1 in inputFile:
        if(len(line1.split()) > 0 and line1.split()[0]=="num_instances"):
            numInstances = int(line1.split()[1].strip())
            if(numInstances < 2):
                print("Use num_instances >= 2")
                quit()
                
    return numInstances

def runPOET(poet_run_Dir,global_directory):
    """
    run POET
    
    Args:
        poet_run_Dir (str): the path to /yourPath/poet/poet_run/
        global_directory (str): the path to /yourPath/poet/poet_run/runs/yourRunName, where yourRunName is the name of your run
    
    """    
    with open(global_directory+"/inputPM.txt") as f:
        inputFile = f.readlines()
        
    numInstances = getNumInstances(inputFile)
        
    # Create the directories for running the instances of POET
    [os.mkdir(global_directory+"/"+str(i)) for i in range(1,numInstances+1)]

    # Create an inputPM.txt file for each of the instances of POET
    for i in range(1,numInstances+1):
        with open(global_directory+"/"+str(i)+"/inputPM.txt", 'w+') as f:
            for line1 in inputFile:
                line1 = line1.strip()
                if(len(line1.split())<2):continue
                if("data_directory" == line1.split()[0]):
                    f.write("data_directory "+poet_run_Dir+"/data/"+line1.split()[1]+" \n\n")
                else:
                    f.write(line1+" \n\n")
            f.write("global_directory "+global_directory+" \n\n")
            f.write("processorNum "+str(i)+" \n")
    
        # run each instance of POET
        # Xmx3700m means that the maximum Java heap memory size will be 3.7 GB (per instance). Setting a low maximum heap value compared to the amount of live data decrease performance by forcing frequent garbage collections.
        subprocess.Popen(["java", "-Xmx3700m","-jar", poet_run_Dir+"/poet.jar",global_directory+"/"+str(i)])
    
        # wait to minimize the chance of an instance having to wait a long time
        time.sleep(120)

def archiveRun(global_directory):
    """
    Move the files associated to the run in global_directory into a directory called global_directory/archived_run_number_n, where n is an integer that 
    corresponds to the latest archived_run_number_n directory in alphabetical order (using the natsort library) 
    and then copy the files that are needed for restarting the run into global_directory
    
    Args:
        global_directory (str): the path to /yourPath/poet/poet_run/runs/yourRunName/
    
    """

    dirsRunNum = natsort.natsorted(iglob(global_directory+"/archived_run_number_*"))
    
    latestRunNumber = int(os.path.basename(dirsRunNum[-1]).split("_")[-1]) if len(dirsRunNum)> 0 else -1
   
    # create the directory to archive the latest run
    os.mkdir(global_directory+"/"+"archived_run_number_"+str(latestRunNumber+1))
    
    #archived_run_number_n
    pathArchivedName = global_directory+"/"+"archived_run_number_"+str(latestRunNumber+1)
    
    for path_i in iglob(pathArchivedName+"/*.backup"):
        if(os.stat(path_i).st_size > 0):
            print("Check backup and original files. The original file may have lost data.")
            quit()
            
    # move all the files into dest
    for path_i in iglob(global_directory+"/*"):
        if("archived_run_number_" in path_i): continue
        shutil.move(path_i,pathArchivedName+"/"+os.path.basename(path_i))
        
    for name_i in ["run_restart_results.py"]:
        shutil.copy2(pathArchivedName+"/"+name_i,global_directory+"/"+name_i)
    
    
    return pathArchivedName

def restartRun(poet_run_Dir,global_directory, archivedName):
    # copy the inputPM.txt, the runPOET, trainingAndValidationIndices, and  globalTrainingFrontierMSE
    for name_i in ["inputPM.txt" ,"trainingAndValidationIndices",  "globalTrainingFrontierMSE"]:
        shutil.copy2(global_directory+"/"+archivedName+"/"+name_i,global_directory+"/"+name_i)
    runPOET(poet_run_Dir,global_directory)    
    
def writeResults(poet_run_Dir,pathArchivedName):
    """
    Write the convex hull to pathArchivedName/results in PDF format
    
    Args:
        poet_run_Dir (str): the path to /yourPath/poet/poet_run/
        pathArchivedName (str): the path to /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n, where yourRunName is the name of your run, 
                                and n is an integer representing the specific archived run for which the results will be written
    
    """
    # create the results directory
    resultsDir = pathArchivedName+"/results/"
    os.mkdir(resultsDir)
    #copy the globalTrainingFrontierMSE file
    shutil.copy2(pathArchivedName+"/globalTrainingFrontierMSE",resultsDir+"/snapshot")

    subprocess.call(["java", "-Xmx7800m", "-jar", poet_run_Dir+"/poet.jar","printReport",pathArchivedName,"MAE","predictedVsTarget"])
    
    simplifScript = poet_run_Dir+"/scripts/graphs_and_LaTeX/simplificationRules.wls"
    
    runName = str(os.path.basename(Path(pathArchivedName).resolve().parents[0]))

    write_3D_convex_hull(runName, pathArchivedName+"/results/", simplifScript)
    
    createReport(pathArchivedName+"/results/",runName)
    
def main():
    thisScript = Path(__file__).resolve()
    poet_run_Dir = str(thisScript.parents[2])
    global_directory = str(thisScript.parents[0])
    
    if(sys.argv[1] == "run"):
        runPOET(poet_run_Dir,global_directory)
    elif(sys.argv[1] == "archive"):
        pathArchivedName = archiveRun(global_directory)
        print("Archived "+global_directory+" to "+pathArchivedName)
    elif(sys.argv[1] == "restart"):
        print("Restarting run in "+global_directory +" from "+sys.argv[2])
        restartRun(poet_run_Dir, global_directory,sys.argv[2])
        
    elif(sys.argv[1] == "results"):
        print("Writing results to "+global_directory+"/"+sys.argv[2]+"/results/")
        writeResults(poet_run_Dir,global_directory+"/"+sys.argv[2])

if __name__ == '__main__': 
    main()
