### Indicate the cutoff distance for the potential models
cutoffDistance 5.0 Angstroms

### the inner radius for the smoothing function
rIn 3.0 Angstroms

### Indicate the name of the data directory in /yourPath/poet/poet_run/data/
data_directory EAM_Cu

### indicate the names of the directoires in /yourPath/poet/poet_run/data/data_directory/ 
### to include in the data, separated by spaces
datanames NVT_FCC_300K

### the number of training values
## Specify one per name in datanames, separated by spaces
numsDataName 100  

### the fraction of the number of structures in datanames used for training, the rest is used for validation. 
## Specify one per name in datanames, separated by spaces. 
## Must have some data for validation to view the results; not all the trainingFractions can be 1.0
trainingFractions 0.5 

### indicate the properties that you want to use for training, separated by spaces. For example: Energy, Force, or Stress
properties Energy 

### the weight of each property, in the same order, separated by spaces
weightsProperties 1.0

# total number of instances (Java Virtual Machines or applications or processes), greater than or equal to 2
num_instances 2

# the maximum number of operations for a Java Virtual Machine instance
# only one number, common to all instances (as opposed to one per instance)
# the minimum gA_maxOperations recommended is 1000
# you may want to test using gA_maxOperations greater values, for example 500,000
gA_maxOperations 5000

# Convergence threshold for the fitness on the mini-batch of one Java Virtual Machine instance
# note: convergenceThreshold is not the same as the fitness on all the training data, usually convergenceThreshold 
# is less than the fitness on all the training data.
convergenceThreshold 0.00000001

